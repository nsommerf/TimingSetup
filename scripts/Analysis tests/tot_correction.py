import numpy as np
import matplotlib.pyplot as plt

# Generate random data
x = np.random.randn(10)  # Variable to be binned
y = np.random.randn(10)  # Variable for which mean will be calculated
print(x)
print(y)

# Define bin edges for x variable
bin_edges = np.linspace(min(x), max(x), num=10)
print(bin_edges)

# Calculate the mean of y variable within each bin
bin_means = []
for i in range(len(bin_edges) - 1):
    mask = (x >= bin_edges[i]) & (x < bin_edges[i + 1])
    print(mask)
    print(y[mask])
    bin_mean = np.mean(y[mask])
    bin_means.append(bin_mean)

# Plot the results
plt.hist2d(x,y)
plt.show()
plt.bar(range(len(bin_means)), bin_means, width=0.8)
plt.xlabel('Bin')
plt.ylabel('Mean of y')
plt.title('2D Histogram with Mean')
plt.show()
