from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

directory = 'timing_resolution_measurement/'

'''filenames = [
    directory + 'measurement_21-06-2023_11-35-21.txt',
    directory + 'measurement_21-06-2023_11-16-35.txt',
    directory + 'measurement_21-06-2023_10-57-26.txt',
    directory + 'measurement_20-06-2023_15-03-30.txt',
    directory + 'measurement_13-06-2023_16-01-20.txt',
    directory + 'measurement_13-06-2023_15-56-22.txt',
    directory + 'measurement_13-06-2023_15-50-30.txt',
    directory + 'measurement_13-06-2023_15-45-00.txt',
    directory + 'measurement_13-06-2023_15-39-29.txt',
    directory + 'measurement_13-06-2023_15-31-23.txt',
    directory + 'measurement_13-06-2023_15-25-15.txt',
    directory + 'measurement_13-06-2023_15-20-12.txt',
    directory + 'measurement_14-06-2023_15-58-52.txt',
    directory + 'measurement_14-06-2023_15-54-29.txt',
    directory + 'measurement_14-06-2023_15-49-35.txt',
    directory + 'measurement_14-06-2023_15-44-59.txt',
    directory + 'measurement_14-06-2023_15-39-09.txt',
    directory + 'measurement_14-06-2023_15-34-19.txt',
    directory + 'measurement_14-06-2023_15-29-28.txt',
    directory + 'measurement_14-06-2023_15-24-19.txt',
    directory + 'measurement_14-06-2023_15-19-32.txt',
    directory + 'measurement_14-06-2023_15-14-54.txt',
    directory + 'measurement_13-06-2023_15-13-42.txt',
    directory + 'measurement_13-06-2023_15-08-34.txt',
    directory + 'measurement_13-06-2023_15-02-59.txt',
    directory + 'measurement_13-06-2023_14-57-33.txt',
    directory + 'measurement_13-06-2023_14-50-17.txt',
    directory + 'measurement_21-06-2023_09-35-25.txt',
    directory + 'measurement_21-06-2023_09-41-23.txt',
    directory + 'measurement_21-06-2023_09-46-54.txt',
    directory + 'measurement_21-06-2023_09-53-02.txt',
    directory + 'measurement_21-06-2023_09-58-55.txt',
    directory + 'measurement_21-06-2023_10-04-32.txt',
    directory + 'measurement_21-06-2023_10-10-40.txt',
    directory + 'measurement_21-06-2023_10-17-40.txt',
    directory + 'measurement_21-06-2023_10-23-54.txt',
    directory + 'measurement_21-06-2023_10-29-19.txt',
    directory + 'measurement_21-06-2023_10-35-11.txt',
    directory + 'measurement_21-06-2023_10-40-59.txt',
    directory + 'measurement_21-06-2023_10-47-15.txt'
]

titles = [
    '10k Measurements Swapped Layers',
    '10k Measurements',
    '5k Measurements',
    '2k Measurements',
    'Only Logic Board',
    '-180 mV Logic Threshold',
    '-160 mV Logic Threshold',
    '-140 mV Logic Threshold',
    '-120 mV Logic Threshold',
    '-100 mV Logic Threshold',
    '-80 mV Logic Threshold',
    '-60 mV Logic Threshold',
    '-60 mV Logic Threshold, 58.71 V SiPM Bias',
    '57.8 V SiPM Bias',
    '57.4 V SiPM Bias',
    '56.6 V SiPM Bias',
    '58.8 V SiPM Bias',
    '58.4 V SiPM Bias',
    '58 V SiPM Bias',
    '57.6 V SiPM Bias',
    '57.2 V SiPM Bias',
    '56.8 V SiPM Bias',
    '56.4 V SiPM Bias',
    '58.71 V SiPM Bias',
    '58.21 V SiPM Bias',
    '57.71 V SiPM Bias',
    '9 V Amplifier',
    '5 V Amplifier'
    '-110 mV Logic Threshold',
    '-115 mV Logic Threshold',
    '-120 mV Logic Threshold',
    '-125 mV Logic Threshold',
    '-130 mV Logic Threshold',
    '-135 mV Logic Threshold',
    '-140 mV Logic Threshold',
    '-145 mV Logic Threshold',
    '-150 mV Logic Threshold',
    '-155 mV Logic Threshold',
    '-160 mV Logic Threshold',
    '-165 mV Logic Threshold',
    '-170mV Logic Threshold'
]'''

filenames = [
    directory + 'measurement_28-06-2023_14-10-53.txt',
    directory + 'measurement_28-06-2023_13-54-57.txt',
    directory + 'measurement_28-06-2023_14-50-13.txt',
    directory + 'measurement_21-06-2023_11-35-21.txt',
    directory + 'measurement_21-06-2023_11-16-35.txt',
    directory + 'measurement_21-06-2023_11-05-33.txt',
    directory + 'measurement_21-06-2023_10-57-26.txt',
    directory + 'measurement_20-06-2023_15-03-30.txt',
    directory + 'measurement_13-06-2023_16-01-20.txt',
    directory + 'measurement_13-06-2023_15-56-22.txt',
    directory + 'measurement_13-06-2023_15-50-30.txt',
    directory + 'measurement_13-06-2023_15-45-00.txt',
    directory + 'measurement_13-06-2023_15-39-29.txt',
    directory + 'measurement_13-06-2023_15-31-23.txt',
    directory + 'measurement_13-06-2023_15-25-15.txt',
    directory + 'measurement_13-06-2023_15-20-12.txt',
    directory + 'measurement_14-06-2023_15-58-52.txt',
    directory + 'measurement_14-06-2023_15-54-29.txt',
    directory + 'measurement_14-06-2023_15-49-35.txt',
    directory + 'measurement_14-06-2023_15-44-59.txt',
    directory + 'measurement_14-06-2023_15-39-09.txt',
    directory + 'measurement_14-06-2023_15-34-19.txt',
    directory + 'measurement_14-06-2023_15-29-28.txt',
    directory + 'measurement_14-06-2023_15-24-19.txt',
    directory + 'measurement_14-06-2023_15-19-32.txt',
    directory + 'measurement_14-06-2023_15-14-54.txt',
    directory + 'measurement_13-06-2023_15-13-42.txt',
    directory + 'measurement_13-06-2023_15-08-34.txt',
    directory + 'measurement_13-06-2023_15-02-59.txt',
    directory + 'measurement_13-06-2023_14-57-33.txt',
    directory + 'measurement_13-06-2023_14-50-17.txt',
    directory + 'measurement_21-06-2023_09-35-25.txt',
    directory + 'measurement_21-06-2023_09-41-23.txt',
    directory + 'measurement_21-06-2023_09-46-54.txt',
    directory + 'measurement_21-06-2023_09-53-02.txt',
    directory + 'measurement_21-06-2023_09-58-55.txt',
    directory + 'measurement_21-06-2023_10-04-32.txt',
    directory + 'measurement_21-06-2023_10-10-40.txt',
    directory + 'measurement_21-06-2023_10-17-40.txt',
    directory + 'measurement_21-06-2023_10-23-54.txt',
    directory + 'measurement_21-06-2023_10-29-19.txt',
    directory + 'measurement_21-06-2023_10-35-11.txt',
    directory + 'measurement_21-06-2023_10-40-59.txt',
    directory + 'measurement_21-06-2023_10-47-15.txt'
]

titles = [
    'Colimator',
    'Material Between Layers',
    '10k Measurements Swapped Layers',
    '10k Measurements',
    '5k Measurements',
    '3k Measurements',
    '2k Measurements',
    'Only Logic Board',
    '180 mV Logic Threshold',
    '160 mV Logic Threshold',
    '140 mV Logic Threshold',
    '120 mV Logic Threshold',
    '100 mV Logic Threshold',
    '80 mV Logic Threshold',
    '60 mV Logic Threshold',
    '60 mV Logic Threshold, 58.71 V SiPM Bias',
    '57.8 V SiPM Bias',
    '57.4 V SiPM Bias',
    '56.6 V SiPM Bias',
    '58.8 V SiPM Bias',
    '58.4 V SiPM Bias',
    '58 V SiPM Bias',
    '57.6 V SiPM Bias',
    '57.2 V SiPM Bias',
    '56.8 V SiPM Bias',
    '56.4 V SiPM Bias',
    '58.71 V SiPM Bias',
    '58.21 V SiPM Bias',
    '57.71 V SiPM Bias',
    '9 V Amplifier',
    '5 V Amplifier',
    '-110 mV Logic Threshold',
    '-115 mV Logic Threshold',
    '-120 mV Logic Threshold',
    '-125 mV Logic Threshold',
    '-130 mV Logic Threshold',
    '-135 mV Logic Threshold',
    '-140 mV Logic Threshold',
    '-145 mV Logic Threshold',
    '-150 mV Logic Threshold',
    '-155 mV Logic Threshold',
    '-160 mV Logic Threshold',
    '-165 mV Logic Threshold',
    '-170mV Logic Threshold'
]


def gaussian_sum(x, mu1, sigma1, a1, mu2, sigma2, a2):
    # Generalized Gaussian distribution function
    return a1/(sigma1*np.sqrt(2*np.pi)) * np.exp((x-mu1)**2/(-2*sigma1**2))+a2/(sigma2*np.sqrt(2*np.pi)) * np.exp((x-mu2)**2/(-2*sigma2**2))

def generalized_gaussian(x, mu, sigma, eta):
    # Generalized Gaussian distribution function
    return eta/(sigma*np.sqrt(2*np.pi)) * np.exp((x-mu)**2/(-2*sigma**2))

def plot_generalized_gaussian(data, num_bins, initial_params, title, efficiency, n):
    # Bin the data
    counts, bin_edges = np.histogram(data, bins=num_bins)

    # Get the bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Fit generalized Gaussian distribution to the data
    params, pcov = curve_fit(generalized_gaussian, bin_centers, counts, p0=initial_params, maxfev=10000)
    fitted_data = generalized_gaussian(bin_centers, *params)
    deviation = counts - fitted_data
    err_counts = np.sqrt(counts)
    for i in range(len(err_counts)-1):
        if err_counts[i] == 0:
            err_counts[i] = 1
    residuals = deviation/err_counts
    reduced_chi_sq = (1.0 / (len(counts) - len(params))) * np.sum(((counts - fitted_data) / err_counts) ** 2)
    #print(reduced_chi_sq)
    spline = UnivariateSpline(bin_centers, fitted_data-np.max(fitted_data)/2, s=0)
    r1, r2 = spline.roots() # find the roots
    fwhm = r2-r1
    

    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    #plt.figure(figsize=(8, 6))
    xlim=20
    axs[0].hist(data, bins=num_bins, alpha=0.6, label='Data')
    axs[0].plot(np.linspace(-xlim,xlim,1000), generalized_gaussian(np.linspace(-xlim,xlim,1000), *params), 'r-', label='Gaussian Fit')
    axs[0].set_xlim(-xlim,xlim)
    axs[0].set_xlabel('Timediffrence [ns]')
    axs[0].set_ylabel('Counts')
    axs[0].set_title(title)
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    description = r"$A$ = "+f'{params[2]:.3e}' + "\n" \
    + r"$\mu$ = "+f'{params[0]:.3f}' + ' ns'+ "\n"\
    + r"$\sigma$ = "+f'{params[1]:.3f}' + ' ns'+ "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n" + r'FWHM =' + f'{fwhm:.3f}' + ' ns'
    axs[0].text(1, 5.5, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(-xlim,xlim)
    axs[1].set_xlabel('Timediffrence [ns]')
    axs[1].set_ylabel('Residuals')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    #plt.savefig('timing_analyse_measurements_plots/single_gauss/single_gauss_' + title.replace(' ', '').replace('.', '') +'.png', bbox_inches='tight')
    #plt.show()
    plt.close()

def plot_gaussian_sum(data, num_bins, initial_params, title, efficiency, n, bounds):
    # Bin the data
    counts, bin_edges = np.histogram(data, bins=num_bins)

    # Get the bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Fit generalized Gaussian distribution to the data
    params, pcov = curve_fit(gaussian_sum, bin_centers, counts, p0=initial_params, maxfev=10000, bounds=bounds)
    fitted_data = gaussian_sum(bin_centers, *params)
    gauss1 = generalized_gaussian(bin_centers, *params[:3])
    gauss2 = generalized_gaussian(bin_centers, *params[3:])
    deviation = counts - fitted_data
    err_counts = np.sqrt(counts)
    for i in range(len(err_counts)-1):
        if err_counts[i] == 0:
            err_counts[i] = 1
    residuals = deviation/err_counts
    reduced_chi_sq = np.sum(((counts - fitted_data) / err_counts) ** 2 /(len(counts) - len(params)))
    #print(reduced_chi_sq)
    spline = UnivariateSpline(bin_centers, fitted_data-np.max(fitted_data)/2, s=0)
    try:
        r1, r2 = spline.roots() # find the roots
        fwhm = r2-r1
    except:
        print("derp")

    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    xlim=20
    #plt.figure(figsize=(8, 6))
    axs[0].hist(data, bins=num_bins, alpha=0.6, label='Data')
    axs[0].plot(np.linspace(-xlim,xlim,1000), gaussian_sum(np.linspace(-xlim,xlim,1000), *params), 'r-', label='Generalized Gaussian Fit')
    axs[0].plot(np.linspace(-xlim,xlim,1000),generalized_gaussian(np.linspace(-xlim,xlim,1000), *params[:3]))
    axs[0].plot(np.linspace(-xlim,xlim,1000),generalized_gaussian(np.linspace(-xlim,xlim,1000), *params[3:]))
    axs[0].set_xlim(-xlim,xlim)
    axs[0].set_xlabel('Timediffrence [ns]')
    axs[0].set_ylabel('Counts')
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    axs[0].set_title(title)
    description = r"$A_1$ = "+f'{params[2]:.3e}' + "\n" + r"$A_2 = $"+f'{params[5]:.3e}' + "\n"\
    + r"$\mu_1$ = "+f'{params[0]:.3f}'+ 'ns' + "\n" + r"$\mu_2 = $"+f'{params[3]:.3f}'+ ' ns' + "\n"\
    + r"$\sigma_1$ = "+f'{params[1]:.3f}'+ 'ns' + "\n" + r"$\sigma_2 = $"+f'{params[4]:.3f}'+ ' ns' + "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n" + r'FWHM =' + f'{fwhm:.3f}' + ' ns'
    axs[0].text(1, 5.5, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(-xlim,xlim)
    axs[1].set_xlabel('Timediffrence [ns]')
    axs[1].set_ylabel('Residuals')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    resolutions.append(fwhm)
    #plt.savefig('timing_analyse_measurements_plots/gauss_sum/sum_gauss' + title.replace(' ', '').replace('.', '') + '.png', bbox_inches='tight')
    #plt.show()
    plt.close()

def plot_2d(first_diff,tot_12,tot_34, title):
    fig, ax = plt.subplots()
    first_diff_ns = np.array(first_diff)*10**9
    tot_34_ns = np.array(tot_34)*10**9
    xbinwidth=0.4
    num_binsx = int((np.max(first_diff_ns)-np.min(first_diff_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(first_diff_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax.hist2d(first_diff_ns, tot_34_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax.hist2d(first_diff_ns, tot_34_ns, bins=300)
    fig.colorbar(h[3], ax=ax)
    plt.title(title)
    plt.xlabel('Timedifference [ns]')
    if title != '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    else:
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    if title != 'Only Logic Board':
        plt.xlim(-20,20)
        plt.ylim(0,100)
    if title == 'Only Logic Board':
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    #plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '') + '_top' + '.png', bbox_inches='tight')
    #plt.show()
    plt.close()

    fig1, ax1 = plt.subplots()
    tot_12_ns = np.array(tot_12)*10**9
    xbinwidth=0.4
    num_binsx = int((np.max(first_diff_ns)-np.min(first_diff_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(first_diff_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax1.hist2d(first_diff_ns, tot_12_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax.hist2d(first_diff_ns, tot_34_ns, bins=300)
    fig1.colorbar(h[3], ax=ax1)
    plt.title(title)
    plt.xlabel('Timedifference [ns]')
    plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    if title != '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    else:
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    if title != 'Only Logic Board':
        plt.xlim(-20,20)
        plt.ylim(0,100)
    if title == 'Only Logic Board':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    #plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '')+ '_bottom' +'.png', bbox_inches='tight')
    #plt.show()
    plt.close()

    fig2, ax2 = plt.subplots()
    xbinwidth=2
    num_binsx = int((np.max(tot_12_ns)-np.min(tot_12_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(tot_34_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax2.hist2d(tot_12_ns, tot_34_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax.hist2d(tot_12_ns, tot_34_ns, bins=300)
    fig2.colorbar(h[3], ax=ax2)
    plt.title(title)
    plt.xlabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    if title == '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    plt.xlim(0,100)
    plt.ylim(0,100)
    #plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '')+ '_tot' +'.png', bbox_inches='tight')
    #plt.show()
    plt.close()

def read_data(filename):
    measurement_number = []
    first_intersection_12 = []
    second_intersection_12 = []
    tot_12 = []
    first_intersection_34 = []
    second_intersection_34 = []
    tot_34 = []
    chisquare_red = []
    first_diff = []
    first_diff_inv = []
    second_diff = []
    # Open the file in read mode
    with open(filename, 'r') as file:
        # Read the lines of the file
        lines = file.readlines()

    # Process the lines, excluding the first two lines as headers
    for line in lines[2:]:
        # Remove leading/trailing whitespace and split the line by comma
        values = line.strip().split(' ')

        # Extract the measurement number
        measurement_number.append(int(values[0]))
        first_intersection_12.append(float(values[1].strip('[]')))
        second_intersection_12.append(float(values[2].strip('[]')))
        tot_12.append(float(values[3].strip('[]')))
        first_intersection_34.append(float(values[4].strip('[]')))
        second_intersection_34.append(float(values[5].strip('[]')))
        tot_34.append(float(values[6].strip('[]')))
        #if np.abs(float(values[1].strip('[]'))-float(values[4].strip('[]'))) < 20e-9:
        if np.abs(float(values[3].strip('[]'))) > 0e-9:
            first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))
            first_diff_inv.append(float(values[4].strip('[]'))-float(values[1].strip('[]')))
        second_diff.append(float(values[2].strip('[]'))-float(values[5].strip('[]')))
    efficiency = len(measurement_number)/measurement_number[len(measurement_number)-1]
    n = str(measurement_number[len(measurement_number)-1])
    return np.array(first_diff), np.array(tot_12), np.array(tot_34), efficiency, n


def multiplots():
    #plot_gaussian_sum(first_diff*10**9, num_bins, initial_params_sum, titles[i], efficiency, n, bounds_sum)
    th = [-180, -160, -140, -120, -100, -80, -60]
    print(titles[8:15])
    print(resolutions[8:15])
    plt.scatter(th, resolutions[8:15])
    plt.xlabel('Logic Board Threshold [mV]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'logic_th_57_21_bias_9_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    bias = [57.8, 57.4, 56.6, 58.8, 58.4, 58, 57.6, 57.2, 56.8, 56.4]
    print(titles[16:26])
    print(resolutions[16:26])
    plt.scatter(bias, resolutions[16:26])
    plt.xlabel('SiPM Bias [V]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'sipm_bias_140_th_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    bias = [58.71, 58.21, 57.71]
    print(titles[26:29])
    print(resolutions[26:29])
    plt.scatter(bias, resolutions[26:29])
    plt.xlabel('SiPM Bias [V]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'sipm_bias_70_th_9_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    th = [-110, -115, -120, -125, -130, -135, -140, -145, -150, -155, -160, -165, -170]
    print(titles[31:44])
    print(resolutions[31:44])
    plt.scatter(th, resolutions[31:44])
    plt.xlabel('Logic Board Threshold [mV]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'logic_th_57_2_bias_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    counts = [10000, 5000, 3000, 2000]
    print(titles[3:7])
    print(resolutions[3:7])
    plt.scatter(counts, resolutions[3:7])
    plt.xlabel('Number of Measurements')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'counts_140_th_57_2_bias_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

resolutions = []
print(len(filenames))

for i in range(len(filenames)):
    first_diff, tot_12, tot_34, efficiency, n = read_data(filenames[i])  # Example data, normally distributed
    binwidth=0.4e-9
    num_bins = int((np.max(first_diff)-np.min(first_diff))/binwidth)  # Number of bins for the histogram
    initial_params= [0.4, 5, 80]  # Starting parameters for the fit
    bounds = [[-2e-9, -1e-9, 0], [2e-9, 7e-9, 1000]]
    initial_params_sum = [0.1, 1, 1000, 0.1, 2, 200]  # Starting parameters for the fit
    bounds_sum = [[-2, 0, 0, -2, 0, 0], [2, np.inf, np.inf, 2, np.inf, np.inf]]
    #bounds_sum = [[-np.inf, 0, 0, -np.inf, 0, 0], [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]]


    if titles[i] != 'Only Logic Board':
        #plot_generalized_gaussian(first_diff*10**9, num_bins, initial_params, titles[i], efficiency, n)
        plot_gaussian_sum(first_diff*10**9, num_bins, initial_params_sum, titles[i], efficiency, n, bounds_sum)
    else:
        resolutions.append(0)
    #plot_2d(first_diff, tot_12, tot_34, titles[i])
    #print(resolutions)
    print(filenames[i])
    print(titles[i])
    print(i)

multiplots()



