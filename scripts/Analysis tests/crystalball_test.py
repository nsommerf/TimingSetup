import numpy as np

from scipy.stats import crystalball

import matplotlib.pyplot as plt

fig, ax = plt.subplots(1, 1)
beta, m = 2, 3

mean, var, skew, kurt = crystalball.stats(beta, m, moments='mvsk')
x = np.linspace(crystalball.ppf(0.01, beta, m),

                crystalball.ppf(0.99, beta, m), 100)

ax.plot(x, crystalball.pdf(x, beta, m),

       'r-', lw=5, alpha=0.6, label='crystalball pdf')
rv = crystalball(beta, m)

ax.plot(x, rv.pdf(x), 'k-', lw=2, label='frozen pdf')
vals = crystalball.ppf([0.001, 0.5, 0.999], beta, m)

np.allclose([0.001, 0.5, 0.999], crystalball.cdf(vals, beta, m))
True
r = crystalball.rvs(beta, m, size=1000)
ax.hist(r, density=True, bins='auto', histtype='stepfilled', alpha=0.2)

ax.set_xlim([x[0], x[-1]])

ax.legend(loc='best', frameon=False)

plt.show()