import numpy as np
import matplotlib.pyplot as plt
from jax import numpy as jnp
from jax import jit
import jax
from iminuit import Minuit

jax.config.update("jax_enable_x64", True)

def fit(x):
    def gauss(x,mu, sgma, c=1):
        return c* jnp.exp(-(x-mu)**2/(2*sgma**2))/(sgma*jnp.sqrt(2*np.pi))

    def double_gauss(x, mu1, sgma1, mu2, sgma2, w):
        return w*gauss(x, mu1, sgma1) + (1-w)*gauss(x, mu2, sgma2)

    @jit
    def ll_fit1(mu, sgma, c):
        return -jnp.sum(jnp.log(gauss(x,mu, sgma, c)))
    @jit
    def ll_fit2(mu1, sgma1, mu2, sgma2, w):
        return -jnp.sum(jnp.log(double_gauss(x, mu1, sgma1, mu2, sgma2, w)))

    start1 = [-7e-10,0.5e-9,1]
    start2 = [-7e-10,7e-10,-2e-10,2e-9,5/8]

    def minimize(ll, start):
        g = jax.grad(ll, argnums=list(range(len(start))))
        m = Minuit(ll, *start, grad=g)
        m.errordef=Minuit.LIKELIHOOD
        #m.limits["sigma"] = (0, np.inf)
        m.migrad()  # find minimum
        m.hesse()   # compute uncertainties
        print(m)
        return m
    print("#"*20 +"  FIT 1  " + "#"*20)
    res1 = minimize(ll_fit1, start1)
    p1 = np.array([ float(p.value) for p in res1.params])
    ll1 = ll_fit1(*p1)

    print("#"*20 +"  FIT 2  " + "#"*20)
    res2 = minimize(ll_fit2, start2)
    p2 = np.array([ float(p.value) for p in res2.params])
    ll2 = ll_fit2(*p2)

    return ll1, ll2, p1, p2



#filename = 'timing_resolution_measurement/measurement_21-06-2023_11-35-21.txt'; title="10k measurements"
filename = 'timing_resolution_measurement/measurement_21-06-2023_11-16-35.txt'; title="5k measurements"
#filename = 'timing_resolution_measurement/measurement_21-06-2023_10-57-26.txt'; title="2k measurements"
#filename = 'timing_resolution_measurement/measurement_20-06-2023_15-03-30.txt'; title="Only Logic Board"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_16-01-20.txt'; title="180 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-56-22.txt'; title="160 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-50-30.txt'; title="140 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-45-00.txt'; title="120 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-39-29.txt'; title="100 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-31-23.txt'; title="80 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-25-15.txt'; title="60 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-20-12.txt'; title="60 mV logic, 58.71bias"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-58-52.txt'; title="bias 57.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-54-29.txt'; title="bias 57.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-49-35.txt'; title="bias 56.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-44-59.txt'; title="bias 58.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-39-09.txt'; title="bias 58.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-34-19.txt'; title="bias 58v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-29-28.txt'; title="bias 57.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-24-19.txt'; title="bias 57.2v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-19-32.txt'; title="bias 56.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-14-54.txt'; title="bias 56.4v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-13-42.txt'; title="bias 58.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-08-34.txt'; title="bias 58.21v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-02-59.txt'; title="bias 57.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-57-33.txt'; title="9v amp"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-50-17.txt'; title="5v amp"
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-32-44.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-24-49.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-15-17.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-41-34.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-23-27.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_09-25-03.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58_mod.txt'  # Replace with your actual file name
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32_mod.txt'


measurement_number = []
first_intersection_12 = []
second_intersection_12 = []
tot_12 = []
first_intersection_34 = []
second_intersection_34 = []
tot_34 = []
chisquare_red = []
first_diff = []
second_diff = []

# Open the file in read mode
with open(filename, 'r') as file:
    # Read the lines of the file
    lines = file.readlines()

# Process the lines, excluding the first two lines as headers
for line in lines[2:]:
    # Remove leading/trailing whitespace and split the line by comma
    values = line.strip().split(' ')

    # Extract the measurement number
    measurement_number.append(int(values[0]))
    first_intersection_12.append(float(values[1].strip('[]')))
    second_intersection_12.append(float(values[2].strip('[]')))
    tot_12.append(float(values[3].strip('[]')))
    first_intersection_34.append(float(values[4].strip('[]')))
    second_intersection_34.append(float(values[5].strip('[]')))
    tot_34.append(float(values[6].strip('[]')))
    #if np.abs(float(values[1].strip('[]'))-float(values[4].strip('[]'))) < 20e-9:
    if np.abs(float(values[3].strip('[]'))) > 0e-9:
        first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))
    second_diff.append(float(values[2].strip('[]'))-float(values[5].strip('[]')))



efficiency = len(measurement_number)/measurement_number[len(measurement_number)-1]
#print(measurement_number)
#print(len(measurement_number))
#print(efficiency)
data = np.array(first_diff)  # Example data, normally distributed

ll1, ll2, res1, res2 = fit(data)

print("ll1: ", ll1) # Basis Modell (H0 Hypothese)
print("ll2: ", ll2) # Zwei Peak Modell (H1 Hypothese)
print("res1: ", res1)
print("res2: ", res2)


