from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
from scipy.stats import genextreme
import ast
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

def gaussian_sum(x, mu1, sigma1, a1, mu2, sigma2, a2):
    # Generalized Gaussian distribution function
    return a1/(sigma1*np.sqrt(2*np.pi)) * np.exp((x-mu1)**2/(-2*sigma1**2))+a2/(sigma2*np.sqrt(2*np.pi)) * np.exp((x-mu2)**2/(-2*sigma2**2))

def generalized_gaussian(x, mu, sigma, eta):
    # Generalized Gaussian distribution function
    return eta/(sigma*np.sqrt(2*np.pi)) * np.exp((x-mu)**2/(-2*sigma**2))

def plot_generalized_gaussian(data, num_bins, initial_params, bounds):
    # Bin the data
    counts, bin_edges = np.histogram(data, bins=num_bins)

    # Get the bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Fit generalized Gaussian distribution to the data
    params, pcov = curve_fit(gaussian_sum, bin_centers, counts, p0=initial_params, maxfev=10000)#, bounds=bounds
    fitted_data = gaussian_sum(bin_centers, *params)
    gauss1 = generalized_gaussian(bin_centers, *params[:3])
    gauss2 = generalized_gaussian(bin_centers, *params[3:])
    deviation = counts - fitted_data
    err_counts = np.sqrt(counts)
    for i in range(len(err_counts)-1):
        if err_counts[i] == 0:
            err_counts[i] = 1
    residuals = deviation/err_counts
    reduced_chi_sq = np.sum(((counts - fitted_data) / err_counts) ** 2 /(len(counts) - len(params)))
    #print(reduced_chi_sq)
    spline = UnivariateSpline(bin_centers, fitted_data-np.max(fitted_data)/2, s=0)
    r1, r2 = spline.roots() # find the roots
    fwhm = r2-r1
    #print(fwhm)

    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    #plt.figure(figsize=(8, 6))
    axs[0].hist(data, bins=num_bins, alpha=0.6, label='Data')
    axs[0].plot(bin_centers, fitted_data, 'r-', label='Generalized Gaussian Fit')
    axs[0].plot(bin_centers,gauss1)
    axs[0].plot(bin_centers,gauss2)
    axs[0].set_xlim(-0.75e-8,0.75e-8)
    axs[0].set_xlabel('Timediffrence [s]')
    axs[0].set_ylabel('Counts')
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    axs[0].set_title(title)
    description = r"$A_1$ = "+f'{params[2]:.3e}' + "\n" + r"$A_2 = $"+f'{params[5]:.3e}' + "\n"\
    + r"$\mu_1$ = "+f'{params[0]:.3e}' + "\n" + r"$\mu_2 = $"+f'{params[3]:.3e}' + "\n"\
    + r"$\sigma_1$ = "+f'{params[1]:.3e}' + "\n" + r"$\sigma_2 = $"+f'{params[4]:.3e}' + "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ str(measurement_number[len(measurement_number)-1])+ "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n" + r'FWHM =' + f'{fwhm:.3e}'
    axs[0].text(1, 5.5, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))


    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(-0.75e-8,0.75e-8)
    axs[1].set_xlabel('Timediffrence [s]')
    axs[1].set_ylabel('Residuals')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    #axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    plt.show()

#filename = 'timing_resolution_measurement/measurement_28-06-2023_14-50-13.txt'; title="10k measurements inverted layers"
filename = 'timing_resolution_measurement/measurement_21-06-2023_11-35-21.txt'; title="10k measurements"
#filename = 'timing_resolution_measurement/measurement_21-06-2023_11-16-35.txt'; title="5k measurements"
#filename = 'timing_resolution_measurement/measurement_21-06-2023_10-57-26.txt'; title="2k measurements"
#filename = 'timing_resolution_measurement/measurement_20-06-2023_15-03-30.txt'; title="Only Logic Board"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_16-01-20.txt'; title="180 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-56-22.txt'; title="160 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-50-30.txt'; title="140 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-45-00.txt'; title="120 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-39-29.txt'; title="100 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-31-23.txt'; title="80 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-25-15.txt'; title="60 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-20-12.txt'; title="60 mV logic, 58.71bias"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-58-52.txt'; title="bias 57.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-54-29.txt'; title="bias 57.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-49-35.txt'; title="bias 56.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-44-59.txt'; title="bias 58.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-39-09.txt'; title="bias 58.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-34-19.txt'; title="bias 58v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-29-28.txt'; title="bias 57.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-24-19.txt'; title="bias 57.2v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-19-32.txt'; title="bias 56.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-14-54.txt'; title="bias 56.4v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-13-42.txt'; title="bias 58.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-08-34.txt'; title="bias 58.21v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-02-59.txt'; title="bias 57.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-57-33.txt'; title="9v amp"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-50-17.txt'; title="5v amp"
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-32-44.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-24-49.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-15-17.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-41-34.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-23-27.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_09-25-03.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58_mod.txt'  # Replace with your actual file name
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32_mod.txt'


measurement_number = []
first_intersection_12 = []
second_intersection_12 = []
tot_12 = []
first_intersection_34 = []
second_intersection_34 = []
tot_34 = []
chisquare_red = []
first_diff = []
first_diff_inv = []
second_diff = []

# Open the file in read mode
with open(filename, 'r') as file:
    # Read the lines of the file
    lines = file.readlines()

# Process the lines, excluding the first two lines as headers
for line in lines[2:]:
    # Remove leading/trailing whitespace and split the line by comma
    values = line.strip().split(' ')

    # Extract the measurement number
    measurement_number.append(int(values[0]))
    first_intersection_12.append(float(values[1].strip('[]')))
    second_intersection_12.append(float(values[2].strip('[]')))
    tot_12.append(float(values[3].strip('[]')))
    first_intersection_34.append(float(values[4].strip('[]')))
    second_intersection_34.append(float(values[5].strip('[]')))
    tot_34.append(float(values[6].strip('[]')))
    #if np.abs(float(values[1].strip('[]'))-float(values[4].strip('[]'))) < 20e-9:
    if np.abs(float(values[3].strip('[]'))) > 0e-9:
        first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))
        first_diff_inv.append(float(values[4].strip('[]'))-float(values[1].strip('[]')))
    second_diff.append(float(values[2].strip('[]'))-float(values[5].strip('[]')))



'''fig, ax = plt.subplots()
first_diff_ns = np.array(first_diff)*10**9
tot_34_ns = np.array(tot_34)*10**9
h = ax.hist2d(first_diff_ns, tot_34_ns, bins=300)
fig.colorbar(h[3], ax=ax)
plt.xlabel('Timedifference [ns]')
plt.ylabel(r'ToT$_{34}$ [ns]')
plt.xlim(-20,20)
plt.ylim(0,100)
#plt.show()

fig1, ax1 = plt.subplots()
tot_12_ns = np.array(tot_12)*10**9
h = ax1.hist2d(first_diff_ns, tot_12_ns, bins=300)
fig1.colorbar(h[3], ax=ax1)
plt.xlabel('Timedifference [ns]')
plt.ylabel(r'ToT$_{12}$ [ns]')
plt.xlim(-20,20)
plt.ylim(0,100)
plt.show()'''



efficiency = len(measurement_number)/measurement_number[len(measurement_number)-1]
#print(measurement_number)
#print(len(measurement_number))
#print(efficiency)
data = np.array(first_diff)  # Example data, normally distributed
num_bins = 1000  # Number of bins for the histogram
initial_params = [4e-10, 5e-9, 8e-7, -4e-10, 5e-9, 2e-7]  # Starting parameters for the fit
bounds = [[-2e-9, -1e-9, 0, -2e-9, -1e-9, 0], [2e-9, 7e-9, 100, 2e-9, 7e-9, 100]]

plot_generalized_gaussian(data, num_bins, initial_params, bounds)

