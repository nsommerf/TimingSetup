import numpy as np
import matplotlib.pyplot as plt
'''
filenames = [
    'timing_resolution_measurement/measurement_14-06-2023_15-58-52.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-54-29.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-49-35.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-44-59.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-39-09.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-34-19.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-29-28.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-24-19.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-19-32.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-14-54.txt',
    'timing_resolution_measurement/measurement_13-06-2023_15-13-42.txt',
    'timing_resolution_measurement/measurement_13-06-2023_15-08-34.txt',
    'timing_resolution_measurement/measurement_13-06-2023_15-02-59.txt'
]
'''
#bias = [57.8,57.4,56.6,58.8,58.4,58,57.6,57.2,56.8,56.4,58.71,58.21,57.71]

'''
filenames = [
    'timing_resolution_measurement/measurement_14-06-2023_15-58-52.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-54-29.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-49-35.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-44-59.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-39-09.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-34-19.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-29-28.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-24-19.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-19-32.txt',
    'timing_resolution_measurement/measurement_14-06-2023_15-14-54.txt'
]
'''

#bias = [57.8,57.4,56.6,58.8,58.4,58,57.6,57.2,56.8,56.4]

#bias = range(len(filenames))
#filenames = [
#    'timing_resolution_measurement/measurement_13-06-2023_15-13-42.txt',
#    'timing_resolution_measurement/measurement_13-06-2023_15-08-34.txt',
#    'timing_resolution_measurement/measurement_13-06-2023_15-02-59.txt'
#]

#bias = [58.71,58.21,57.71]

'''
filenames = [
    "timing_resolution_measurement/measurement_21-06-2023_09-35-25.txt",
    "timing_resolution_measurement/measurement_21-06-2023_09-41-23.txt",
    "timing_resolution_measurement/measurement_21-06-2023_09-46-54.txt",
    "timing_resolution_measurement/measurement_21-06-2023_09-53-02.txt",
    "timing_resolution_measurement/measurement_21-06-2023_09-58-55.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-04-32.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-10-40.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-17-40.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-23-54.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-29-19.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-35-11.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-40-59.txt",
    "timing_resolution_measurement/measurement_21-06-2023_10-47-15.txt"
]

bias = [-110, -115, -120, -125, -130, -135, -140, -145, -150, -155, -160, -165, -170]

'''

bias=[1000,2000,3000,5000,10000]

filenames = [
    'timing_resolution_measurement/measurement_21-06-2023_10-10-40.txt',
    'timing_resolution_measurement/measurement_21-06-2023_10-57-26.txt',
    'timing_resolution_measurement/measurement_21-06-2023_11-05-33.txt',
    'timing_resolution_measurement/measurement_21-06-2023_11-16-35.txt',
    'timing_resolution_measurement/measurement_21-06-2023_11-35-21.txt'
]

resolutions = []



#filename = 'timing_resolution_measurement/measurement_13-06-2023_16-01-20.txt'; title="180 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-56-22.txt'; title="160 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-50-30.txt'; title="140 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-45-00.txt'; title="120 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-39-29.txt'; title="100 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-31-23.txt'; title="80 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-25-15.txt'; title="60 mV logic"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-20-12.txt'; title="60 mV logic, 58.71bias"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-58-52.txt'; title="bias 57.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-54-29.txt'; title="bias 57.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-49-35.txt'; title="bias 56.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-44-59.txt'; title="bias 58.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-39-09.txt'; title="bias 58.4v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-34-19.txt'; title="bias 58v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-29-28.txt'; title="bias 57.6v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-24-19.txt'; title="bias 57.2v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-19-32.txt'; title="bias 56.8v"
#filename = 'timing_resolution_measurement/measurement_14-06-2023_15-14-54.txt'; title="bias 56.4v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-13-42.txt'; title="bias 58.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-08-34.txt'; title="bias 58.21v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_15-02-59.txt'; title="bias 57.71v"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-57-33.txt'; title="9v amp"
#filename = 'timing_resolution_measurement/measurement_13-06-2023_14-50-17.txt'; title="5v amp"
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-32-44.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-24-49.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_15-15-17.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-41-34.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_14-23-27.txt'
#filename = 'timing_resolution_measurement/measurement_07-06-2023_09-25-03.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_13-56-58_mod.txt'  # Replace with your actual file name
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32.txt'
#filename = 'timing_resolution_measurement/measurement_06-06-2023_14-24-32_mod.txt'



for i in range(len(filenames)):
    measurement_number = []
    first_intersection_12 = []
    second_intersection_12 = []
    tot_12 = []
    first_intersection_34 = []
    second_intersection_34 = []
    tot_34 = []
    chisquare_red = []
    first_diff = []
    second_diff = []

    # Open the file in read mode
    with open(filenames[i], 'r') as file:
        # Read the lines of the file
        lines = file.readlines()

    # Process the lines, excluding the first two lines as headers
    for line in lines[2:]:
        # Remove leading/trailing whitespace and split the line by comma
        #print(line.strip().split(' '))
        values = line.strip().split(' ')
        #print(values)

        # Extract the measurement number
        measurement_number.append(int(values[0]))
        first_intersection_12.append(float(values[1].strip('[]')))
        second_intersection_12.append(float(values[2].strip('[]')))
        tot_12.append(float(values[3].strip('[]')))
        first_intersection_34.append(float(values[4].strip('[]')))
        second_intersection_34.append(float(values[5].strip('[]')))
        tot_34.append(float(values[6].strip('[]')))
        if np.abs(float(values[1].strip('[]'))-float(values[4].strip('[]'))) < 10e-9:
        #if np.abs(float(values[3].strip('[]'))) > 40e-9:
            first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))
        second_diff.append(float(values[2].strip('[]'))-float(values[5].strip('[]')))
    resolutions.append(np.std(first_diff)/np.sqrt(2))


#print(np.std(first_diff))
#print(np.std(second_diff))

# Create histogram
#plt.hist(first_diff, bins=60, edgecolor='black')

#efficiency = len(measurement_number)/measurement_number[len(measurement_number)-1]
#print(measurement_number)
#print(len(measurement_number))
#print(efficiency)
#description = "$\sigma$ = "+f'{np.std(first_diff):.3e}' + " s\n" + r"$\frac{1}{\sqrt{2}} \sigma$ = "+f'{np.std(first_diff)/np.sqrt(2):.3e}' + " s\n"+"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ str(measurement_number[len(measurement_number)-1]) 
#plt.text(0.9, 0.9, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

plt.scatter(bias, resolutions)
# Set labels and title
#plt.title("Bias")
#plt.xlabel('Bias [V]')
plt.title("Consistency")
plt.xlabel('Counts')
plt.ylabel('Time Resolution [s]')
#plt.title('Difference of First Switching')
plt.gca().ticklabel_format(axis='y', style='sci', scilimits=(0, 0))  # Set scientific notation for tick labels
#plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
#plt.savefig('timing_analyse_measurements_plots/'+filename.replace('timing_resolution_measurement/', '')+'.png', bbox_inches='tight')

# Display the histogram
plt.show()





