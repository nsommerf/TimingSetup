import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def GaussExp(x, mean, sgma, k, amplitude):
    result = np.empty(len(x))
    for i in range(len(x)):
        if (x[i]-mean)/sgma <= k:
            result[i] = amplitude*np.exp(-0.5*((x[i]-mean)/sgma)**2)
        else:
            result[i] = amplitude*np.exp(k**2/2-k*((x[i]-mean)/sgma))
    return result

x= np.linspace(-10,10,1000)
y = GaussExp(x, 0, 1, 1, 1)
plt.plot(x,y)
plt.show()