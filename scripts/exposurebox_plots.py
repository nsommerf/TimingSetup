import numpy as np
import matplotlib.pyplot as plt

x = np.array(range(8))

foil = np.array([22674, 24683, 23242, 24165, 23261, 23643, 23218, 22966])
ptfe = np.array([21028, 21190, 21235, 20495, 20949, 20839, 21540, 20640])
nopwm = np.array([20547, 20963, 20743, 20302, 21340, 22072, 22334, 21345])
foil_corners = np.array([19901, 22978, 20140, 20138, 20482, 20720, 22277, 20680])

plt.bar(x-0.3, foil, width=0.2, yerr = np.sqrt(foil)+np.std(nopwm), label='Foil')
plt.bar(x-0.1, ptfe, width=0.2, yerr = np.sqrt(ptfe)+np.std(nopwm),  label='PTFE')
plt.bar(x+0.1, nopwm, width=0.2, yerr = np.sqrt(nopwm)+np.std(nopwm), label='No LEDs')
plt.bar(x+0.3, foil_corners, width=0.2, yerr = np.sqrt(foil_corners)+np.std(nopwm), align='center', label='Foil with taped corners')
plt.axhline(np.mean(nopwm), color = 'black', label = 'Mean of No LEDS')

plt.xlabel('LED Position')
plt.ylabel('Counts')
#plt.title('Bar Chart of Different Datasets')
plt.xticks(x, ['0', '1', '2', '3', '4', '5', '6', '7'])  # Customizing the x-axis labels
plt.legend(loc = 'lower right')
#plt.ylim(top = 35000)
#print(np.std(nopwm))
plt.show()

ptfe1 = np.array([1987, 424037, 2502, 1902, 2283, 12245, 1854, 1885])
ptfe2 = np.array([1368, 170076, 1687, 1361, 1729, 6733, 1444, 1430])
ptfe3 = np.array([1845, 1959, 1764, 1714, 1770, 1633, 1800, 1682])

plt.bar(x-0.3, ptfe1, width=0.2, yerr = np.sqrt(ptfe1)+np.std(nopwm)/10, label='PTFE 1')
plt.bar(x-0.1, ptfe2, width=0.2, yerr = np.sqrt(ptfe2)+np.std(nopwm)/10, label='PTFE 2')
plt.bar(x+0.1, ptfe3, width=-0.2, yerr = np.sqrt(ptfe3)+np.std(nopwm)/10, label='PTFE 3')
plt.bar(x+0.3, nopwm/10, width=-0.2, yerr = np.sqrt(nopwm/10)+np.std(nopwm)/10, label='No LEDS scaled')
plt.axhline(np.mean(nopwm)/10, color = 'black', label = 'Mean of No LEDS scaled')

plt.xlabel('LED Position')
plt.ylabel('Counts')
#plt.title('Bar Chart of Different Datasets')
plt.xticks(x, ['0', '1', '2', '3', '4', '5', '6', '7'])  # Customizing the x-axis labels
plt.legend(loc = 'upper right')
#plt.ylim(top = 35000)
#print(np.std(nopwm))
plt.show()

plt.bar(x-0.3, ptfe1, width=0.2, yerr = np.sqrt(ptfe1)+np.std(nopwm)/10, label='PTFE 1')
plt.bar(x-0.1, ptfe2, width=0.2, yerr = np.sqrt(ptfe2)+np.std(nopwm)/10, label='PTFE 2')
plt.bar(x+0.1, ptfe3, width=-0.2, yerr = np.sqrt(ptfe3)+np.std(nopwm)/10, label='PTFE 3')
plt.bar(x+0.3, nopwm/10, width=-0.2, yerr = np.sqrt(nopwm/10)+np.std(nopwm)/10, label='No LEDS scaled')
plt.axhline(np.mean(nopwm)/10, color = 'black', label = 'Mean of No LEDS scaled')

plt.xlabel('LED Position')
plt.ylabel('Counts')
#plt.title('Bar Chart of Different Datasets')
plt.xticks(x, ['0', '1', '2', '3', '4', '5', '6', '7'])  # Customizing the x-axis labels
plt.legend(loc = 'upper left')
plt.ylim(top = 13000)
#print(np.std(nopwm))
plt.show()