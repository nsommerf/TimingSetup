# Importing Libraries
import serial
import time
import sys
import pyvisa
import datetime
#import cv2
#import pytesseract
#pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

#initalizing testparameters
pins = ['0','4','5','12','13','14','15','16','alle']
test_duration = 600
warmup_time = 30
nom=len(pins)

#starting serial com with arduino
#arduino = serial.Serial(port='COM4', baudrate=9600, timeout=.1)#use for laptop
arduino = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, timeout=.1)#use on lab desktop

#start communication with oscilloscope
try:
	rm = pyvisa.ResourceManager()
	device = rm.open_resource('TCPIP::131.220.218.112::INSTR',query_delay=0.25)
except:
    print('Failed to connect to Oscilloscope. Did the IP change?')
    sys.exit(0)

 
def toggle_led(position):
	if position == 'alle':
		toggle_all()
	else:
		arduino.write(bytes(position + '\n', 'utf-8'))
		time.sleep(0.05)

def toggle_all():
	toggle_led('5')
	toggle_led('4')
	toggle_led('15')
	toggle_led('13')
	toggle_led('12')
	toggle_led('14')
	toggle_led('16')
	toggle_led('0')
    
#def read_counter(position,filename_pos, filename):
    #img = cv2.imread(filename_pos+'.bmp')
    #crop_img = img[70:100,100:202]
    #cv2.imshow('img',img)
    #cv2.waitKey(0)
    #cv2.imshow('crop_img',crop_img)
    #cv2.waitKey(0)
    #text = pytesseract.image_to_string(crop_img)
    #print(text)
    #text = text.replace("\n", "")
    #print(text)
    #num = int(text)
    #f=open(filename+'.txt','a')
    #f.write(position+' '+text+'\n')
    #f.flush()
    #f.close()
		
def screen_grab(filename):
	device.timeout = 30000
	device.chunk_size = 20*1024*1024
	device.write("SCDP")
	IMAGE_DATA = device.read_raw()
	f=open(filename+'.bmp','wb')
	f.write(IMAGE_DATA)
	f.flush()
	f.close()

def clear_sweeps():
	device.write(":ACQuire:CSWeep")
	
def run_trigger():
	device.write(":TRIGger:RUN")
	
def stop_trigger():
	device.write(":TRIGger:STOP")

def measure_position(position, duration, filename_pos, filename):
	toggle_led(position)
	run_trigger()
	time.sleep(warmup_time)
	clear_sweeps()
	time.sleep(duration)
	screen_grab(filename_pos)
	#read_counter(position, filename_pos, filename)
	stop_trigger()
	toggle_led(position)


def full_measurement():
	finish = datetime.datetime.now() + datetime.timedelta(seconds=nom*test_duration+nom*warmup_time)
	print('Estimated finish time: ' + str(finish) + '.')
	for i in range(nom):
		print('Measuring led configuration ' + str(i+1) + ' of ' + str(nom) + '.')
		measure_position(pins[i], test_duration, 'data/100mv_foil_taped corners' + str(test_duration) + '_' + str(i), 'data/ptfe_' + str(test_duration))
#toggle_led(pins[4])
full_measurement()
