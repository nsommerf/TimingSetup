import serial.tools.list_ports

# Get a list of available serial ports
ports = serial.tools.list_ports.comports()

# Iterate over the ports and print their information
for port in ports:
    print(f"Device: {port.device}")
    print(f"  Description: {port.description}")
    print(f"  Manufacturer: {port.manufacturer}")
    print(f"  Product: {port.product}")
    print("")


