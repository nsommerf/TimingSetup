import numpy as np
import matplotlib.pyplot as plt

def sipm_signal(time, amplitude, rise_time, fall_time, time_walk_factor):
    """
    Generate a simulated SiPM signal.

    Parameters:
        time (array): Time array for the signal.
        amplitude (float): Signal amplitude.
        rise_time (float): Signal rise time (in ns).
        fall_time (float): Signal fall time (in ns).
        time_walk_factor (float): Time walk factor.

    Returns:
        array: SiPM signal array.
    """
    signal = np.zeros_like(time)
    #signal[time <= rise_time] = amplitude * (1-np.exp((-time[time <= rise_time] -rise_time) /rise_time))
    #signal[time > rise_time] = amplitude * np.exp((-time[time > rise_time]-rise_time) / fall_time)
    signal[time < rise_time] = amplitude * (1-np.exp(-time[time < rise_time] /(rise_time-time[time < rise_time])))
    signal[time >= rise_time] = amplitude * np.exp((rise_time - time[time >= rise_time]) / fall_time)
    return signal

# Simulation parameters
time_resolution = 0.1  # ns (time resolution of the simulation)
time_range = 100  # ns (simulation time range)
time_walk_factor = 5.0  # Time walk factor

# Create time array
time = np.arange(0, time_range, time_resolution)

# Generate SiPM signals of different amplitudes
amplitude1 = 100.0  # Amplitude of the first signal
amplitude2 = 250.0  # Amplitude of the second signal

# Generate SiPM signals
signal1 = sipm_signal(time, amplitude1, rise_time=10.0, fall_time=50.0, time_walk_factor=time_walk_factor)
signal2 = sipm_signal(time, amplitude2, rise_time=10.0, fall_time=50.0, time_walk_factor=time_walk_factor)

# Plot the SiPM signals
plt.figure(figsize=(10, 6))
plt.plot(time, signal1, label=f"Amplitude = {amplitude1}")
plt.plot(time, signal2, label=f"Amplitude = {amplitude2}")
plt.xlabel("Time (ns)")
plt.ylabel("Signal Amplitude")
plt.title("SiPM Signals with Different Amplitudes (Time Walk)")
plt.legend()
#plt.grid(True)
plt.show()
