import visa
import numpy as np
from scipy.optimize import curve_fit

# Connect to the oscilloscope over the network
rm = visa.ResourceManager()
scope = rm.open_resource('TCPIP::192.168.1.10::INSTR')

# Set up oscilloscope parameters
scope.write(':STOP')
scope.write(':WAV:SOUR CHAN1')
scope.write(':WAV:MODE NORM')
scope.write(':WAV:FORM BYTE')
scope.write(':WAV:POIN:MODE RAW')
scope.write(':WAV:POIN 10000')
scope.write(':CHAN1:SCAL 0.2')
scope.write(':TRIG:SOUR CHAN1')
scope.write(':TRIG:EDGE:SLOP POS')
scope.write(':TRIG:EDGE:LEV 1.0')

# Define function to fit to waveform data
def error_func(x, a, b, c, d, e):
    return a * np.exp(-(x - b)**2 / (2 * c**2)) + d * np.exp(-(x - e)**2 / (2 * c**2))

# Set up waveform measurements
n_measurements = 10
waveform_data = np.zeros((n_measurements, 4, 10000), dtype=np.float32)

# Trigger oscilloscope and retrieve waveform data after each trigger
for i in range(n_measurements):
    print(f'Measurement {i + 1}')
    scope.write(':RUN')
    scope.write(':TRIG')
    waveform_data[i, 0] = (np.frombuffer(scope.query_binary_values(':WAV:DATA?', 'B', False), dtype=np.uint8) - 127.5) * 0.2 / 25 + 0
    scope.write(':WAV:SOUR CHAN2')
    waveform_data[i, 1] = (np.frombuffer(scope.query_binary_values(':WAV:DATA?', 'B', False), dtype=np.uint8) - 127.5) * 0.2 / 25 + 0
    scope.write(':WAV:SOUR CHAN3')
    waveform_data[i, 2] = (np.frombuffer(scope.query_binary_values(':WAV:DATA?', 'B', False), dtype=np.uint8) - 127.5) * 0.2 / 25 + 0
    scope.write(':WAV:SOUR CHAN4')
    waveform_data[i, 3] = (np.frombuffer(scope.query_binary_values(':WAV:DATA?', 'B', False), dtype=np.uint8) - 127.5) * 0.2 / 25 + 0

# Fit error functions to waveform data and find time at which functions intersect
for i in range(n_measurements):
    print(f'Processing measurement {i + 1}')
    p0 = (1, 5000, 500, 1, 5000)
    for j in range(3):
        for k in range(j+1, 4):
            popt, pcov = curve_fit(error_func, np.arange(10000), waveform_data[i, j], p0=p0)
            t_jk = popt[1]
            p0 = popt
            print(f'Time at which channels {j+1} and {k+1} intersect: {t_jk}')
