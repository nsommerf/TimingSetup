import numpy as np
from jax import numpy as jnp
from jax import jit
import jax
from iminuit import Minuit
import matplotlib.pyplot as plt

jax.config.update("jax_enable_x64", True)

def fit(x):
    def gauss(x,mu, sgma):
        return jnp.exp(-(x-mu)**2/(2*sgma**2))/(sgma*jnp.sqrt(2*np.pi))

    def double_gauss(x, mu1, sgma1, mu2, sgma2, w):
        return w*gauss(x, mu1, sgma1) + (1-w)*gauss(x, mu2, sgma2)

    @jit
    def ll_fit1(mu, sgma):
        return -jnp.sum(jnp.log(gauss(x,mu, sgma)))
    @jit
    def ll_fit2(mu1, sgma1, mu2, sgma2, w):
        return -jnp.sum(jnp.log(double_gauss(x, mu1, sgma1, mu2, sgma2, w)))

    start1 = [1.0,2.0]
    start2 = [0,1.,2.,5.,0.5]

    def minimize(ll, start):
        g = jax.grad(ll, argnums=list(range(len(start))))
        m = Minuit(ll, *start, grad=g)
        m.errordef=Minuit.LIKELIHOOD
        # m.limits["sigma"] = (0, np.inf)
        m.migrad()  # find minimum
        m.hesse()   # compute uncertainties
        print(m)
        return m
    print("#"*20 +"  FIT 1  " + "#"*20)
    res1 = minimize(ll_fit1, start1)
    p1 = np.array([ float(p.value) for p in res1.params])
    ll1 = ll_fit1(*p1)

    print("#"*20 +"  FIT 2  " + "#"*20)
    res2 = minimize(ll_fit2, start2)
    p2 = np.array([ float(p.value) for p in res2.params])
    ll2 = ll_fit2(*p2)

    return ll1, ll2, p1, p2

data = np.append(np.random.normal(0,1,1000),np.random.normal(2,5,1000))
print(data)
print(type(data))
#print(data)
ll1, ll2, res1, res2 = fit(data)


def gauss1(x,mu, sgma):
    return jnp.exp(-(x-mu)**2/(2*sgma**2))/(sgma*jnp.sqrt(2*np.pi))
num_bins = 100
counts, bin_edges = np.histogram(data, bins=num_bins)
bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
plt.plot(bin_centers,gauss1(bin_centers, *res1))
plt.hist(data,num_bins)
plt.show()

print("ll1: ", ll1) # Basis Modell (H0 Hypothese)
print("ll2: ", ll2) # Zwei Peak Modell (H1 Hypothese)
print("res1: ", res1)
print("res2: ", res2)


# WILKS THEOREM
# https://en.wikipedia.org/wiki/Likelihood-ratio_test
# https://en.wikipedia.org/wiki/Wilks%27_theorem

d = 2*(ll2-ll1) 

DOF = len(res2) - len(res1)
print("Delta DOF: ", DOF)
print("d: ", d)

from scipy.stats import chi2
p = chi2.sf(d, DOF)
print("p: ", p)