# Importing Libraries
import serial
import time
arduino = serial.Serial(port='COM4', baudrate=9600, timeout=.1)
def write_read(x):
    arduino.write(bytes(x + '\n', 'utf-8'))
    #arduino.write(x + '\n')
    time.sleep(0.05)

while True:
	num = input("Which pin do you want to toggle? Enter a number: ") # Taking input from user
	if num == 'alle':
		print('Toggling all pins')
		write_read('5')
		write_read('4')
		write_read('15')
		write_read('13')
		write_read('12')
		write_read('14')
		write_read('16')
		write_read('0')
		#write_read('2')
	else:
		#print('else')
		write_read(num)