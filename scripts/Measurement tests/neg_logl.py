import numpy as np
from scipy.optimize import curve_fit

# Define your log-likelihood function
def log_likelihood(params, x, y):
    # Extract the parameters
    # Here, we assume a simple linear model: y = mx + c
    m, c = params

    # Calculate the predicted y values
    y_pred = m * x + c

    # Calculate the negative log-likelihood
    # Assuming a Gaussian likelihood function
    neg_log_likelihood = -np.sum((y - y_pred) ** 2 / (2 * sigma ** 2))

    return neg_log_likelihood

# Generate some example data
x = np.linspace(0, 10, 100)
m_true = 2.5
c_true = 1.0
sigma = 0.1
y_true = m_true * x + c_true
y_data = np.random.normal(y_true, sigma)

# Perform the fit
initial_params = [1.0, 0.0]  # Initial guess for the parameters
params_fit, pcov = curve_fit(log_likelihood, x, y_data, p0=initial_params)

# Extract the fitted parameters
m_fit, c_fit = params_fit

# Print the fitted parameters
print("Fitted slope (m):", m_fit)
print("Fitted intercept (c):", c_fit)
