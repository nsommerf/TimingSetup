import numpy as np
from scipy.optimize import minimize
from scipy.stats import norm
import matplotlib.pyplot as plt

# Define the sum of two Gaussians as the model function
def gaussian_sum(params, x):
    mu1, sigma1, mu2, sigma2 = params
    gaussian1 = norm.pdf(x, mu1, sigma1)
    gaussian2 = norm.pdf(x, mu2, sigma2)
    return gaussian1 + gaussian2

# Define the negative log-likelihood function
def neg_log_likelihood(params, x, data):
    y_pred = gaussian_sum(params, x)
    log_prob = np.log(y_pred).sum()
    neg_log_likelihood = -log_prob
    return neg_log_likelihood

# Initial parameter values and bounds
initial_params = [0.5, 0.5, 2.0, 0.5]
bounds = [(None, None), (0, None), (None, None), (0, None)]

# Minimize the negative log-likelihood
result = minimize(neg_log_likelihood, initial_params, args=(x_true, data), bounds=bounds)

# Extract the optimized parameters
mu1_fit, sigma1_fit, mu2_fit, sigma2_fit = result.x


# Generate some example data
filename = '../timing_resolution_measurement/measurement_21-06-2023_11-16-35.txt'; title="5k measurements"
measurement_number = []
first_intersection_12 = []
second_intersection_12 = []
tot_12 = []
first_intersection_34 = []
second_intersection_34 = []
tot_34 = []
chisquare_red = []
first_diff = []
second_diff = []

# Open the file in read mode
with open(filename, 'r') as file:
    # Read the lines of the file
    lines = file.readlines()

# Process the lines, excluding the first two lines as headers
for line in lines[2:]:
    # Remove leading/trailing whitespace and split the line by comma
    values = line.strip().split(' ')
    first_intersection_12.append(float(values[1].strip('[]')))
    if np.abs(float(values[3].strip('[]'))) > 0e-9:
        first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))
data = np.array(first_diff)

# Define the initial parameter guess
initial_params = [0, 1]

# Define the objective function
objective = lambda params: log_likelihood(params, data)

# Minimize the negative log-likelihood function
result = minimize(objective, initial_params)

# Extract the estimated parameters
estimated_mu, estimated_sigma = result.x

# Plot the data and fitted Gaussian distribution
plt.hist(data, bins=200, density=True, alpha=0.5, label='Data')
x = np.linspace(data.min(), data.max(), 1000)
y_estimated = 1 / (np.sqrt(2 * np.pi * estimated_sigma**2)) * np.exp(-(x - estimated_mu)**2 / (2 * estimated_sigma**2))
plt.plot(x, y_estimated, 'g-', label='Fitted Gaussian')
plt.xlabel('x')
plt.ylabel('Probability Density')
plt.title('Maximum Likelihood Estimation')
plt.legend()
plt.show()

# Print the results
print("\nEstimated Parameters:")
print(f"   mu    = {estimated_mu}")
print(f"   sigma = {estimated_sigma}")
