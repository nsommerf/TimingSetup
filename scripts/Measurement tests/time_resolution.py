import pyvisa
import numpy as np
from scipy.optimize import curve_fit
import matplotlib as plt

# Function to retrieve waveform data from all four channels
def get_waveforms(scope):
    waveforms = []
    for i in range(1, 5):
        print(f"Getting waveform of channel {i}")
        scope.write(f':WAV:SOUR C{i}')  # Set channel to retrieve data from
        #scope.write(':RUN')  # Start acquisition
        waveform_data = scope.query_binary_values(':WAV:DATA?', datatype='B')
        waveform_volts_per_div = float(scope.query(f':CHAN{i}:SCAL?'))
        waveform_offset = float(scope.query(f':CHAN{i}:OFFS?'))
        waveform_time_per_div = float(scope.query(':TIM:SCAL?'))
        waveform_time_offset = float(scope.query(':TIM:DEL?'))
        waveform = ((np.array(waveform_data) - 127.5) * waveform_volts_per_div / 25) + waveform_offset
        waveform_time = np.arange(len(waveform)) * waveform_time_per_div + waveform_time_offset
        waveforms.append((waveform_time, waveform))
    return waveforms

# Function to fit error functions to waveforms and find times where functions meet
def fit_and_find_intersection(waveforms):
    intersections = []
    for i in range(2):
        xdata1, ydata1 = waveforms[i]
        xdata2, ydata2 = waveforms[i+2]
        popt1, pcov1 = curve_fit(error_func, xdata1, ydata1, p0=(1, np.mean(xdata1), 1, np.mean(ydata1)))
        popt2, pcov2 = curve_fit(error_func, xdata2, ydata2, p0=(1, np.mean(xdata2), 1, np.mean(ydata2)))
        intersection = np.roots(popt1 - popt2)
        intersections.append(intersection[0])
    return intersections

# Set up the oscilloscope
rm = pyvisa.ResourceManager()
scope = rm.open_resource('TCPIP::131.220.218.137::inst0::INSTR')
#scope.write(':STOP')  # Stop acquisition
scope.write(':WAV:FORM BYTE')  # Set waveform format to byte
scope.write(':WAV:POINTS:MODE RAW')  # Set data points mode to raw
scope.write(':WAV:POINTS 10000')  # Set number of data points to retrieve
scope.write(':WAV:PRE:ENC BIN')  # Set data encoding to binary

# Define error function to fit to waveforms
def error_func(x, a, b, c, d):
    return a * np.exp(-((x-b)/c)**2) + d

# Perform multiple measurements and compute standard deviation of intersection times
num_measurements = 10
intersection_times = []
for i in range(num_measurements):
    print(f"Measurement {i+1}")
    waveforms = get_waveforms(scope)
    print(waveforms)
    #intersections = fit_and_find_intersection(waveforms)
    #intersection_times.append(intersections)
    #print(f'Time where channels 1 and 3 meet: {intersections[0]:.3f} seconds')
    #print(f'Time where channels 2 and 4 meet: {intersections[1]:.3f} seconds')

#intersection_times = np.array(intersection_times)
#mean_times = np.mean(intersection_times, axis=0)
#std_times = np.std(intersection_times, axis=0)

#print(f'Mean time where channels 1 and 3 meet: {mean_times[0]:.3f} ± {std_times[0]:.3f} seconds')
#print(f'Mean time where channels 2 and 4 meet: {mean_times[1]:.3f} ± {std_times[1]:.3f} seconds')

# Close connection to the oscilloscope
scope.close()

