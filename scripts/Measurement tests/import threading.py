import threading
import time

# Shared variable to control the loop
exit_loop = False

# Function to run the loop
def measurement_loop():
    global exit_loop
    while True:
        # Perform your measurement here
        print("Taking measurement...")
        time.sleep(2)

        # Check if the exit flag is set
        if exit_loop:
            print("Exiting the loop...")
            break

# Start the loop in a separate thread
loop_thread = threading.Thread(target=measurement_loop)
loop_thread.start()

# Main program
while True:
    command = input("Enter a command ('exit' to stop the loop): ")

    if command == "exit":
        # Set the exit flag to True
        exit_loop = True
        break

# Wait for the loop thread to finish
loop_thread.join()
print("Program finished.")
