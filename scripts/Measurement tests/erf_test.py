import numpy as np
import matplotlib.pyplot as plt
import scipy.special

# Generate data for plotting
x = np.linspace(-3, 3, 1000)
y = 2*scipy.special.erf((x-1)*1)+1

# Create the plot
fig, ax = plt.subplots()
ax.plot(x, y)

# Set the axis labels and title
ax.set_xlabel('x')
ax.set_ylabel('erf(x)')
ax.set_title('Error Function')

# Display the plot
plt.show()
