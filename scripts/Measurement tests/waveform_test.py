import pyvisa
import pylab as pl
import matplotlib.pyplot as plt
import struct
import math
import gc
import time
import numpy as np
import scipy

# Define the error functions
def errorfunc(x, a, b, c, d):
    x = np.array(x)
    return a*np.array(scipy.special.erf((x-b)*c))+d

# Define the difference of the error functions
def diff_error(x, a, b, c, d, e, f, g, h):
    x = np.array(x)
    return errorfunc(x, a, b, c, d) + errorfunc(x, e, f, g, h)

# Define the objective function to minimize
def objective(x, params):
    return np.abs(diff_error(x, params))


"""Modify the following global variables according to the model"""
SDS_RSC = 'TCPIP::131.220.218.137::inst0::INSTR'
CHANNEL = ["C1","C2","C3","C4"]
HORI_NUM = 10 #default 10
vertical_divisions = 8
tdiv_enum = [200e-12, 500e-12, 1e-9,
2e-9, 5e-9, 10e-9, 20e-9, 50e-9, 100e-9, 200e-9, 500e-9, \
1e-6, 2e-6, 5e-6, 10e-6, 20e-6, 50e-6, 100e-6, 200e-6, 500e-6, \
1e-3, 2e-3, 5e-3, 10e-3, 20e-3, 50e-3, 100e-3, 200e-3, 500e-3, \
1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]#100e-12,

"""The following code realizes the process of waveform reconstruction with slice"""
#establishing connection to osci and setting length of eceived data
_rm = pyvisa.ResourceManager()
sds = _rm.open_resource(SDS_RSC)
sds.timeout = 30000 # default value is 2000(2s)
sds.chunk_size = 20 * 1024 * 1024 # default value is 20*1024(20k bytes)

#this function gets the settings of the osci contained in the preamble
def main_desc(recv):
    WAVE_ARRAY_1 = recv[0x3c:0x3f + 1]
    wave_array_count = recv[0x74:0x77 + 1]
    first_point = recv[0x84:0x87 + 1]
    sp = recv[0x88:0x8b + 1]
    v_scale = recv[0x9c:0x9f + 1]
    v_offset = recv[0xa0:0xa3 + 1]
    interval = recv[0xb0:0xb3 + 1]
    code_per_div = recv[0xa4:0Xa7 + 1]#
    #print(code_per_div)
    adc_bit = recv[0xac:0Xad + 1]
    delay = recv[0xb4:0xbb + 1]
    tdiv = recv[0x144:0x145 + 1]
    probe = recv[0x148:0x14b + 1]

    data_bytes = struct.unpack('i', WAVE_ARRAY_1)[0]
    point_num = struct.unpack('i', wave_array_count)[0]
    #print("get me out")
    #print(point_num)
    fp = struct.unpack('i', first_point)[0]
    sp = struct.unpack('i', sp)[0]
    interval = struct.unpack('f', interval)[0]
    #print(interval)
    delay = struct.unpack('d', delay)[0]
    tdiv_index = struct.unpack('h', tdiv)[0]
    probe = struct.unpack('f', probe)[0]
    vdiv = struct.unpack('f', v_scale)[0] * probe
    offset = struct.unpack('f', v_offset)[0] * probe
    code = struct.unpack('f', code_per_div)[0]
    #print("code")
    #print(code)
    adc_bit = struct.unpack('h', adc_bit)[0]
    tdiv = tdiv_enum[tdiv_index]
    #print("interval")

    #print(probe)
    #print(interval)
    return vdiv, offset, interval, delay, tdiv, code, adc_bit

def main_wf_data(number_ch):
    sds.write(":WAVeform:STARt 0")
    sds.write("WAV:SOUR {}".format(CHANNEL[number_ch]))
    sds.write("WAV:PREamble?")
    recv_all = sds.read_raw()
    recv = recv_all[recv_all.find(b'#') + 11:]
    #print(recv)
    #print(len(recv))
    vdiv, ofst, interval, trdl, tdiv, vcode_per, adc_bit = main_desc(recv)
    #print(vdiv, ofst, interval, trdl, tdiv,vcode_per,adc_bit)
    #print(len(recv_all))

    points = sds.query(":ACQuire:POINts?").strip()
    #print(points)
    points = float(sds.query(":ACQuire:POINts?").strip())
    #print(points)
    one_piece_num = float(sds.query(":WAVeform:MAXPoint?").strip())
    #print(one_piece_num)
    if points > one_piece_num:
        sds.write(":WAVeform:POINt {}".format(one_piece_num))
    if adc_bit > 8:
        sds.write(":WAVeform:WIDTh WORD")
    
    #points=100
    read_times = math.ceil(points / one_piece_num)
    #print(points)
    #print(one_piece_num)
    #print(read_times)
    #read_times=10
    recv_all = []
    for i in range(0, read_times):
        start = i * one_piece_num
        #start=0
        sds.write(":WAVeform:STARt {}".format(start))
        sds.write("WAV:DATA?")
        recv_rtn = sds.read_raw().rstrip()
        block_start = recv_rtn.find(b'#')
        data_digit = int(recv_rtn[block_start + 1:block_start + 2])
        data_start = block_start + 2 + data_digit
        recv = list(recv_rtn[data_start:])
        recv_all += recv
        #sds.write(":TRIG:RUN")#restarting for next aquisition

    convert_data = []
    if adc_bit > 8:
        for i in range(0, int(len(recv_all) / 2)):
            data = recv_all[2 * i + 1] * 256 + recv_all[2 * i]
            convert_data.append(data)
    else:
        convert_data = recv_all
    volt_value = []
    for data in convert_data:
        if data > pow(2, adc_bit - 1) - 1:
            data = data - pow(2, adc_bit)
        else:
            pass
        volt_value.append(data)
        #print(convert_data)
    del recv, recv_all, convert_data
    gc.collect()

    time_value = []
    for idx in range(0, len(volt_value)):
        #print("voltvalue vcode per vdiv ofset voltvalue")
        #print(volt_value[idx])
        #print(vcode_per)
        #print(vdiv)
        volt_value[idx] = volt_value[idx] / vcode_per * float(vdiv)*vertical_divisions/2 - float(ofst) #problem with original code: full measurement range is from -vcode_per to + vcode_per -> need to scale voltage per division with total number of divisions
        #print(ofst)
        #print(volt_value)
        #print(len(volt_value))
        time_data = - (float(tdiv) * HORI_NUM / 2) + idx * interval + float(trdl) #interval should be divided by 2? not anymore????????????? was maybe broken because of incorrect query for trigger status?

        #print(len(volt_value))
        #print(interval)
        #print(trdl)
        #print(time_data)
        time_value.append(time_data)
        #print(len(volt_value))
    return time_value, volt_value

    '''pl.figure(figsize=(7, 5))
    pl.errorbar(time_value, volt_value, markersize=1, fmt='.', label=u"Y-T")
    pl.legend()
    pl.grid()
    #pl.show()'''

'''# Define the error functions
def error1(x, a, b, c):
    return a * np.exp(-((x - b) / c)**2)

def error2(x, d, e, f):
    return d * np.exp(-((x - e) / f)**2)

# Define the difference of the error functions
def diff_error(x, params):
    a, b, c, d, e, f = params
    return error1(x, a, b, c) - error2(x, d, e, f)

# Define the objective function to minimize
def objective(x, params):
    return np.abs(diff_error(x, params))

# Define the four data sets
x1 = np.array([1, 2, 3, 4, 5])
y1 = np.array([2, 3, 2.5, 1.5, 1])
x2 = np.array([3, 4, 5, 6, 7])
y2 = np.array([1.5, 2, 2.5, 2, 1.5])
x3 = np.array([1, 2, 3, 4, 5])
y3 = np.array([1.5, 2, 3, 2.5, 1.5])
x4 = np.array([3, 4, 5, 6, 7])
y4 = np.array([1, 1.5, 2, 2.5, 2])

# Fit the first two data sets
params12 = minimize_scalar(lambda x: objective(x, [1, 3, 0.5, 1, 4, 0.5])).x

# Fit the second two data sets
params34 = minimize_scalar(lambda x: objective(x, [1, 3, 0.5, 1, 4, 0.5])).x

# Find the intersection of the first two fits
intersect12 = minimize_scalar(lambda x: diff_error(x, params12)).x

# Find the intersection of the second two fits
intersect34 = minimize_scalar(lambda x: diff_error(x, params34)).x

# Print the results
print(f"Intersection of the first two fits: {intersect12}")
print(f"Intersection of the second two fits: {intersect34}")'''

if __name__=='__main__':
    start_time = time.time()
    #sds.write(":TRIG:STOP") #required to stop trigger before aquisition
    sds.write("TRIG:MODE SING")
    num_channels = 4
    data = [] #data[channel][time or volt]
    fit_params = []
    err_fit_params = []
    wait_time = 0
    while True:
        time.sleep(0.001)
        wait_time+=1
        #trigger_stat = struct.unpack('f', sds.write("TRIG:STAT?"))[0]
        print("Waiting for trigger since {:.3f} second(s)".format(wait_time/1000))
        trigger_stat = sds.query('TRIG:STAT?')
        #print(sds.write("TRIG:MODE?"))
        #print(trigger_stat)
        if trigger_stat == "Stop\n":
            print("Trigger detected!")
            break
    for i in range(num_channels):
        time_data, volt_data = main_wf_data(i)
        filename=f'output_{i}.txt'#.format{i}
        with open(filename, 'w') as f:
            f.write('{} {}\n'.format(time_data,volt_data))
        amplitude = np.max(volt_data) - np.min(volt_data)
        delta_volt_value = np.empty(len(volt_data)-1)
        for j in range(len(volt_data)-1):
            delta_volt_value[j] = volt_data[j]-volt_data[j+1]
            #print(volt_data[j])
            #print(volt_data[j+1])
            #print(volt_data[j]-volt_data[j+1])
            #print(delta_volt_value)
        #print(delta_volt_value)
        jump1 = time_data[np.argmin(delta_volt_value)]
        jump2 = time_data[np.argmax(delta_volt_value)]
        #print(jump1)
        #print(jump2)
        if jump2 < jump1:
            temp = jump2
            jump2 = jump1
            jump1 = temp
        #print(jump1)
        #print(jump2)
        slew_rate = 10**(9)
        center = np.max(volt_data) - amplitude
        p0 = [-amplitude, jump1, slew_rate, center , amplitude, jump2, slew_rate, center]
        #p0 = [-0.055, -5*10**(-9), 10**(9), 0., 0.055, -1*10**(-9), 10**(9), 0.32]
        #bounds = ([-0.07,-1.,10**(8), 0.2,0.04,-1.,10**(8), 0.2],[-0.03,1.,10**(10), 0.4,0.07,1.,10**(9), 0.4])
        popt, pcov = scipy.optimize.curve_fit(diff_error, time_data, volt_data, p0 = p0, maxfev = 100000)#, bounds = bounds, p0 = [0.05, -1., 0.6, 0.05, -1., 0.6] a*np.array(scipy.special.erf((x-b)*c))+d
        fit_params.append(popt)
        #print(fit_params)
        err_fit_params.append(pcov)
        yerr = np.ones_like(volt_data)*0.005
        reduced_chi_sq = (1.0 / (len(volt_data) - len(popt))) * np.sum(((volt_data - diff_error(time_data, *popt)) / yerr) ** 2)
        #print(reduced_chi_sq)
        #data.append((time_data,volt_data))
        plt.scatter(time_data, volt_data, label='Data')
        plot_time = np.linspace(np.min(time_data),np.max(time_data), 1000)
        plt.plot(plot_time, diff_error(plot_time, *popt), label='Fit')
        #plt.plot(time_data, scipy.special.erf(time_data), label='Fit')
        #plt.plot(time_data, errorfunc(time_data, -0.055, 0.0, 1000000000, 0.588), label='Fit')
        #plt.plot(time_data, diff_error(time_data, 0.05, -1., 0.6, 0.05, -1., 0.6), label='Fit')
        #plt.legend()
        #if i == 1 or i ==3:
            #plt.show()
        #print(data)
    #print(data[0][0])
    #print(data[0][1])
    #print(data[1][0])
    #print(data[1][1])
    #sds.write(":TRIG:RUN")#restarting for next aquisition
    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Elapsed time: {:.3f} seconds".format(elapsed_time))