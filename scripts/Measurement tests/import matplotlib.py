import matplotlib.pyplot as plt
import numpy as np

x = np.arange(5)
y = np.exp(x)
z = np.sin(x)
fig2, (ax2, ax3) = plt.subplots(nrows=2, ncols=1) # two axes on figure
ax2.plot(x, z)
ax3.plot(x, -z)

plt.show()