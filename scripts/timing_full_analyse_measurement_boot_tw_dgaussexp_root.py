# use : source ~/ROOT/installed_root/bin/thisroot.sh
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import numpy as np
from scipy.integrate import quad
import time
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from tqdm import tqdm
import ROOT



NBOOTSTRAP = 100
directory = 'timing_resolution_measurement/'

filenames = [
    directory + 'measurement_28-06-2023_14-10-53.txt',
    directory + 'measurement_28-06-2023_13-54-57.txt',
    directory + 'measurement_28-06-2023_14-50-13.txt',
    directory + 'measurement_21-06-2023_11-35-21.txt',
    directory + 'measurement_21-06-2023_11-16-35.txt',
    directory + 'measurement_21-06-2023_11-05-33.txt',
    directory + 'measurement_21-06-2023_10-57-26.txt',
    directory + 'measurement_20-06-2023_15-03-30.txt',
    directory + 'measurement_13-06-2023_16-01-20.txt',
    directory + 'measurement_13-06-2023_15-56-22.txt',
    directory + 'measurement_13-06-2023_15-50-30.txt',
    directory + 'measurement_13-06-2023_15-45-00.txt',
    directory + 'measurement_13-06-2023_15-39-29.txt',
    directory + 'measurement_13-06-2023_15-31-23.txt',
    directory + 'measurement_13-06-2023_15-25-15.txt',
    directory + 'measurement_13-06-2023_15-20-12.txt',
    directory + 'measurement_14-06-2023_15-58-52.txt',
    directory + 'measurement_14-06-2023_15-54-29.txt',
    directory + 'measurement_14-06-2023_15-49-35.txt',
    directory + 'measurement_14-06-2023_15-44-59.txt',
    directory + 'measurement_14-06-2023_15-39-09.txt',
    directory + 'measurement_14-06-2023_15-34-19.txt',
    directory + 'measurement_14-06-2023_15-29-28.txt',
    directory + 'measurement_14-06-2023_15-24-19.txt',
    directory + 'measurement_14-06-2023_15-19-32.txt',
    directory + 'measurement_14-06-2023_15-14-54.txt',
    directory + 'measurement_13-06-2023_15-13-42.txt',
    directory + 'measurement_13-06-2023_15-08-34.txt',
    directory + 'measurement_13-06-2023_15-02-59.txt',
    directory + 'measurement_13-06-2023_14-57-33.txt',
    directory + 'measurement_13-06-2023_14-50-17.txt',
    directory + 'measurement_21-06-2023_09-35-25.txt',
    directory + 'measurement_21-06-2023_09-41-23.txt',
    directory + 'measurement_21-06-2023_09-46-54.txt',
    directory + 'measurement_21-06-2023_09-53-02.txt',
    directory + 'measurement_21-06-2023_09-58-55.txt',
    directory + 'measurement_21-06-2023_10-04-32.txt',
    directory + 'measurement_21-06-2023_10-10-40.txt',
    directory + 'measurement_21-06-2023_10-17-40.txt',
    directory + 'measurement_21-06-2023_10-23-54.txt',
    directory + 'measurement_21-06-2023_10-29-19.txt',
    directory + 'measurement_21-06-2023_10-35-11.txt',
    directory + 'measurement_21-06-2023_10-40-59.txt',
    directory + 'measurement_21-06-2023_10-47-15.txt',
    directory + 'measurement_12-07-2023_11-13-15.txt',
    directory + 'measurement_12-07-2023_11-18-35.txt',
    directory + 'measurement_12-07-2023_11-23-24.txt',
    directory + 'measurement_12-07-2023_11-28-57.txt',
    directory + 'measurement_12-07-2023_11-34-15.txt',
    directory + 'measurement_12-07-2023_11-40-02.txt',
    directory + 'measurement_12-07-2023_11-45-07.txt',
    directory + 'measurement_12-07-2023_11-54-54.txt',
    directory + 'measurement_12-07-2023_12-01-41.txt',
    directory + 'measurement_12-07-2023_12-09-08.txt',
    directory + 'measurement_07-08-2023_10-34-31.txt',
    directory + 'measurement_07-08-2023_13-34-59.txt',
    directory + 'measurement_07-08-2023_13-44-50.txt',
    directory + 'measurement_07-08-2023_14-11-42.txt',
    directory + 'measurement_07-08-2023_14-20-54.txt',
    directory + 'measurement_10-08-2023_10-15-35.txt',
    directory + 'measurement_10-08-2023_13-50-33.txt',
    directory + 'measurement_10-08-2023_14-45-36.txt',
    directory + 'measurement_10-08-2023_15-00-06.txt',
    directory + 'measurement_11-08-2023_15-51-00.txt',
    directory + 'measurement_14-08-2023_09-19-31.txt',
    directory + 'measurement_14-08-2023_11-48-07.txt',
    directory + 'measurement_14-08-2023_11-58-31.txt',
    directory + 'measurement_14-08-2023_12-06-44.txt',
    directory + 'measurement_14-08-2023_12-11-36.txt',
    directory + 'measurement_17-08-2023_09-07-15.txt',
    directory + 'measurement_17-08-2023_09-10-54.txt',
    directory + 'measurement_17-08-2023_09-18-00.txt',
    directory + 'measurement_17-08-2023_09-28-33.txt',
    directory + 'measurement_17-08-2023_09-42-08.txt',
    directory + 'measurement_17-08-2023_09-58-59.txt',
    directory + 'measurement_17-08-2023_10-21-33.txt',
    directory + 'measurement_17-08-2023_10-48-26.txt'
]

titles = [
    'Colimator',
    'Material Between Layers',
    '10k Measurements Swapped Layers',
    '10k Measurements',
    '5k Measurements',
    '3k Measurements',
    '2k Measurements',
    'Only Logic Board',
    '-180 mV Logic Threshold',
    '-160 mV Logic Threshold',
    '-140 mV Logic Threshold',
    '-120 mV Logic Threshold',
    '-100 mV Logic Threshold',
    '-80 mV Logic Threshold',
    '-60 mV Logic Threshold',
    '-60 mV Logic Threshold, 58.71 V SiPM Bias',
    '57.8 V SiPM Bias',
    '57.4 V SiPM Bias',
    '56.6 V SiPM Bias',
    '58.8 V SiPM Bias',
    '58.4 V SiPM Bias',
    '58 V SiPM Bias',
    '57.6 V SiPM Bias',
    '57.2 V SiPM Bias',
    '56.8 V SiPM Bias',
    '56.4 V SiPM Bias',
    '58.71 V SiPM Bias',
    '58.21 V SiPM Bias',
    '57.71 V SiPM Bias',
    '9 V Amplifier',
    '5 V Amplifier',
    '-110 mV Logic Threshold',
    '-115 mV Logic Threshold',
    '-120 mV Logic Threshold',
    '-125 mV Logic Threshold',
    '-130 mV Logic Threshold',
    '-135 mV Logic Threshold',
    '-140 mV Logic Threshold',
    '-145 mV Logic Threshold',
    '-150 mV Logic Threshold',
    '-155 mV Logic Threshold',
    '-160 mV Logic Threshold',
    '-165 mV Logic Threshold',
    '-170mV Logic Threshold',
    '6 V Amplifier',
    '7 V Amplifier',
    '8 V Amplifier',
    '10 V Amplifier',
    '11 V Amplifier',
    '58.71 V SiPM Bias',
    '59.21 V SiPM Bias',
    '-50 mV Logic Threshold',
    '-40 mV Logic Threshold',
    '-30 mV Logic Threshold',
    'High Statistics',
    'No Extension Cables',
    'Asymetric Extension Cables',
    'Single SiPM',
    'Single SiPM TH adjusted',
    'Faster Scintillator',
    'Logic Board Away from Trigger',
    'Logic Board Trigger on Falling Edge',
    'Logic Board Trigger on Rising Edge',
    'Logic Board High Intersection Resolution',
    'Logic Board Investigation',
    'Logic Board Again Trigger on Falling Edge',
    'Logic Board Swapped Outputs',
    'Logic Board Wires Separated',
    'Logic Board Wires Separated Again',
    '1k Measurements',
    '2k Measurements',
    '3k Measurements',
    '4k Measurements',
    '5k Measurements',
    '6.5k Measurements',
    '8k Measurements',
    '10k Measurements'
]


def gaussian_sum(x, mu1, sigma1, a1, mu2, sigma2, a2):
    return a1/(sigma1*np.sqrt(2*np.pi)) * np.exp((x-mu1)**2/(-2*sigma1**2))+a2/(sigma2*np.sqrt(2*np.pi)) * np.exp((x-mu2)**2/(-2*sigma2**2))

def generalized_gaussian(x, mu, sigma, eta):
    return eta/(sigma*np.sqrt(2*np.pi)) * np.exp((x-mu)**2/(-2*sigma**2))

''' # slow because of for loop with ifs
def DoubleGaussExp(x, mean, sgma, kl, kh, amplitude):
    result = np.empty(len(x))
    for i in range(len(x)):
        if ((x[i]-mean)/sgma <= kh) & ((x[i]-mean)/sgma > -kl):
            result[i] = amplitude*np.exp(-0.5*((x[i]-mean)/sgma)**2)
        if (x[i]-mean)/sgma > kh:
            result[i] = amplitude*np.exp(kh**2/2-kh*((x[i]-mean)/sgma))
        if (x[i]-mean)/sgma <= -kl:
            result[i] = amplitude*np.exp(kl**2/2+kl*((x[i]-mean)/sgma))
    return result
'''

def oldDoubleGaussExp(x, mean, sgma, kl, kh, amplitude):
    middle = amplitude*np.exp(-0.5*((x-mean)/sgma)**2)
    high = amplitude*np.exp(kh**2/2-kh*((x-mean)/sgma))
    low = amplitude*np.exp(kl**2/2+kl*((x-mean)/sgma))

    l_m = np.where((x-mean) <= -kl * sgma, low, middle)
    l_m_h = np.where((x-mean) > kh * sgma, high, l_m)

    return l_m_h

def DoubleGaussExp(x, mean, sgma, kl, kh):
    middle = np.exp(-0.5*((x-mean)/sgma)**2)
    high = np.exp(kh**2/2-kh*((x-mean)/sgma))
    low = np.exp(kl**2/2+kl*((x-mean)/sgma))

    l_m = np.where((x-mean) <= -kl * sgma, low, middle)
    l_m_h = np.where((x-mean) > kh * sgma, high, l_m)

    return l_m_h

def scalingCalc(x, amplitude, mean, sgma, kl, kh):
    return amplitude*DoubleGaussExp(x, mean, sgma, kl, kh)


def GaussExp(x, mean, sgma, k, amplitude):
    result = np.empty(len(x))
    for i in range(len(x)):
        if (x[i]-mean)/sgma <= k:
            result[i] = amplitude*np.exp(-0.5*((x[i]-mean)/sgma)**2)
        else:
            result[i] = amplitude*np.exp(k**2/2-k*((x[i]-mean)/sgma))
    return result


def exponential(x, a, b, c):
    return a*np.exp(b*x)+c

def plot_generalized_gaussian(data, num_bins, initial_params, title, efficiency, n, xliml, xlimr, filename):
    # Bin the data
    counts, bin_edges = np.histogram(data, bins=num_bins)

    # Get the bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Fit generalized Gaussian distribution to the data
    params, pcov = curve_fit(generalized_gaussian, bin_centers, counts, p0=initial_params, maxfev=10000)
    perr = np.sqrt(np.diag(pcov))
    #print(perr)
    fitted_data = generalized_gaussian(bin_centers, *params)
    deviation = counts - fitted_data
    err_counts = np.sqrt(counts)
    for i in range(len(err_counts)-1):
        if err_counts[i] == 0:
            err_counts[i] = 1
    residuals = deviation/err_counts
    reduced_chi_sq = (1.0 / (len(counts) - len(params))) * np.sum(((counts - fitted_data) / err_counts) ** 2)
    #print(reduced_chi_sq)
    spline = UnivariateSpline(bin_centers, fitted_data-np.max(fitted_data)/2, s=0)
    r1, r2 = spline.roots() # find the roots
    fwhm = r2-r1
    

    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    #plt.figure(figsize=(8, 6))
    axs[0].hist(data, bins=num_bins, alpha=0.6, label='Data')
    axs[0].plot(np.linspace(xliml, xlimr,1000), generalized_gaussian(np.linspace(xliml, xlimr,1000), *params), 'r-', label='Gaussian Fit')
    axs[0].set_xlim(xliml,xlimr)
    axs[0].set_xlabel('Timediffrence [ns]')
    axs[0].set_ylabel('Counts')
    axs[0].set_title(title)
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    description = r"$A$ = "+f'{params[2]:.3e}' + "\n" \
    + r"$\mu$ = "+f'{params[0]:.3f}' + ' ns'+ "\n"\
    + r"$\sigma$ = "+f'{params[1]:.3f}' + ' ns'+ "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n" + r'FWHM =' + f'{fwhm:.3f}' + ' ns'
    axs[0].text(1, 5.5, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(xliml, xlimr)
    axs[1].set_xlabel('Timediffrence [ns]')
    axs[1].set_ylabel('Pulls')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    #plt.savefig('timing_analyse_measurements_plots/single_gauss/single_gauss_' + title.replace(' ', '').replace('.', '') + filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') +'.png', bbox_inches='tight')
    plt.show()
    plt.close()

def plot(data, xliml, xlimr, num_bins, title):
    plt.hist(data, bins=num_bins, alpha=0.6, label='Data', edgecolor='black', range=(xliml, xlimr))
    #plt.xlim(xliml,xlimr)
    plt.xlabel('Timediffrence [ns]')
    plt.ylabel('Counts')
    plt.title(title)
    #plt.set_position([0.1, 0.35, 0.8, 0.55])
    plt.show()
    plt.close()

def plot_nolim(data, num_bins, title):
    plt.hist(data, bins=num_bins, alpha=0.6, label='Data', edgecolor='black')
    #plt.xlim(min(data), max(data))
    plt.xlabel('Timediffrence [ns]')
    plt.ylabel('Counts')
    plt.title(title)
    plt.show()
    plt.close()

def plot_tot(tot_12, tot_34, num_bins, title):
    counts, bin_edges = np.histogram(tot_12, bins=num_bins)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    plt.hist(tot_12, bins=num_bins, alpha=0.6, label='Data')
    #plt.set_xlim(xliml,xlimr)
    plt.xlabel(r'ToT_{Bottom} [ns]')
    plt.ylabel('Counts')
    plt.title(title)
    #plt.set_position([0.1, 0.35, 0.8, 0.55])
    plt.show()
    plt.close()

    counts, bin_edges = np.histogram(tot_34, bins=num_bins)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    plt.hist(tot_12, bins=num_bins, alpha=0.6, label='Data')
    #plt.set_xlim(xliml,xlimr)
    plt.xlabel(r'ToT_{Top} [ns]')
    plt.ylabel('Counts')
    plt.title(title)
    #plt.set_position([0.1, 0.35, 0.8, 0.55])
    plt.show()
    plt.close()

def plot_gaussian_sum(data, num_bins, initial_params, title, efficiency, n, bounds, xliml, xlimr, filename):
    # Bin the data
    counts, bin_edges = np.histogram(data, bins=num_bins)

    # Get the bin centers
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    # Fit generalized Gaussian distribution to the data
    params, pcov = curve_fit(gaussian_sum, bin_centers, counts, p0=initial_params, maxfev=10000, bounds=bounds)

    bootstrap_params = []
    try:
        for _ in tqdm(list(range(NBOOTSTRAP))):
            bootstrap_sample = np.random.choice(data, size=len(data), replace=True)
            c, be = np.histogram(bootstrap_sample, bins=num_bins)
            bc = (be[:-1] + be[1:]) / 2
            p, pcov = curve_fit(gaussian_sum, bc, c, p0=initial_params, maxfev=10000, bounds=bounds)
            bootstrap_params.append(p)
    except:
        print('Bootstrap failed!')
    
    bootstrap_params = np.array(bootstrap_params)

    
    perr = np.sqrt(np.diag(pcov))
    print(perr)
    def calc_fwhm(params):
        fitted_data = gaussian_sum(bin_centers, *params)
        gauss1 = generalized_gaussian(bin_centers, *params[:3])
        gauss2 = generalized_gaussian(bin_centers, *params[3:])
        deviation = counts - fitted_data
        err_counts = np.sqrt(counts)
        for i in range(len(err_counts)-1):
            if err_counts[i] == 0:
                err_counts[i] = 1
        residuals = deviation/err_counts
        reduced_chi_sq = np.sum(((counts - fitted_data) / err_counts) ** 2 /(len(counts) - len(params)))
        #print(reduced_chi_sq)
        spline = UnivariateSpline(bin_centers, fitted_data-np.max(fitted_data)/2, s=0)
        try:
            r1, r2 = spline.roots() # find the roots
            fwhm = abs(r2-r1)
        except:
            fwhm = -1
            r1 = r2 = 0
            print("derp")
        return fwhm, residuals, reduced_chi_sq, gauss1, gauss2, r1, r2
    fwhm, residuals, reduced_chi_sq, gauss1, gauss2, r1, r2 = calc_fwhm(params)

    bootstrap_fwhm = np.array([calc_fwhm(p)[0] for p in bootstrap_params])
    err_fwhm = np.std(bootstrap_fwhm[bootstrap_fwhm > 0])

    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    #plt.figure(figsize=(8, 6))
    axs[0].hist(data, bins=num_bins, alpha=0.6, label='Data')
    axs[0].plot(np.linspace(xliml, xlimr,1000), gaussian_sum(np.linspace(xliml, xlimr,1000), *params), 'r-', label='Generalized Gaussian Fit')
    axs[0].plot(np.linspace(xliml, xlimr,1000),generalized_gaussian(np.linspace(xliml, xlimr,1000), *params[:3]))
    axs[0].plot(np.linspace(xliml, xlimr,1000),generalized_gaussian(np.linspace(xliml, xlimr,1000), *params[3:]))
    axs[0].set_xlim(xliml,xlimr)
    axs[0].set_xlabel('Timediffrence [ns]')
    axs[0].set_ylabel('Counts')
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    axs[0].set_title(title)
    description = r"$A_1$ = "+f'{params[2]:.3e}' + "\n" + r"$A_2 = $"+f'{params[5]:.3e}' + "\n"\
    + r"$\mu_1$ = "+f'{params[0]:.3f}'+ 'ns' + "\n" + r"$\mu_2 = $"+f'{params[3]:.3f}'+ ' ns' + "\n"\
    + r"$\sigma_1$ = "+f'{params[1]:.3f}'+ 'ns' + "\n" + r"$\sigma_2 = $"+f'{params[4]:.3f}'+ ' ns' + "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n" + r'FWHM =' + f'{fwhm:.3f}' + r"$\pm$" + f'{err_fwhm:.3f}' + ' ns'
    axs[0].text(1, 5.5, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(xliml,xlimr)
    axs[1].set_xlabel('Timediffrence [ns]')
    axs[1].set_ylabel('Pulls')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    resolutions.append(fwhm)
    plt.savefig('timing_analyse_measurements_plots/gauss_sum/sum_gauss' + title.replace(' ', '').replace('.', '') + filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') + '.png', bbox_inches='tight')
    #plt.show()
    plt.close()

def time_walk_correction(x,y,bin_width,plotting):
    nbins = int(np.abs((max(y)-min(y))/bin_width))
    bin_edges = np.linspace(min(y), max(y), num=nbins)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    # Calculate the mean of y variable within each bin
    bin_means = []
    bin_stds = []
    empty_bins = []
    entries = []
    x_corrected = np.zeros_like(x)
    for i in range(len(bin_edges) - 1):
        mask = (y >= bin_edges[i]) & (y < bin_edges[i + 1])
        bin_mean = np.mean(x[mask])
        bin_std = np.std(x[mask])
        if (np.isnan(bin_mean) == False) & (len(x[mask])>50):
            bin_means.append(bin_mean)
            bin_stds.append(bin_std)
            #print(len(x[mask]))
        else:
            #print('Fott damit!')
            empty_bins.append(i)
        entries.append(len(x[mask]))
        #print('Gurke!')
    entries = np.delete(np.array(entries),empty_bins)
    bin_centers = np.delete(bin_centers,empty_bins)
    y_offset = np.mean(bin_means[int(len(bin_means)*3/4):])
    if np.mean(bin_means[:int(len(bin_means)/2)]) < np.mean(bin_means[int(len(bin_means)/2):]):#first half lower than second
        bounds = [[-10**5,-np.inf,-np.inf],[0,0,np.inf]]
        p0= [-70, -0.05, y_offset]
    else:
        bounds = [[0,-np.inf,-np.inf],[10**5,0,np.inf]]
        p0= [70, -0.05, y_offset]
    try:
        params, pcov = curve_fit(exponential,bin_centers, bin_means, maxfev=1000, p0 = p0, sigma=bin_stds, bounds = bounds)#sigma=1/entries**0.5
    except:
        print('Not enough events to do timewalk correction. Skipping to next dataset!')
        return
    if plotting:
        fig = plt.figure(figsize=(8, 6))
        gs = fig.add_gridspec(2, hspace=0.3)
        axs = gs.subplots(sharex = False)
        print(params)
        axs[0].plot(np.linspace(min(bin_centers),max(bin_centers),1000), exponential(np.linspace(min(bin_centers),max(bin_centers),1000), *params))
        axs[0].scatter(bin_centers, bin_means)
        axs[0].set_xlabel('ToT [ns]')
        axs[0].set_ylabel(r'Mean $\Delta t$ [ns]')
        axs[1].scatter(bin_centers, entries, alpha=0.6, label='Data')
        axs[1].set_xlabel('ToT [ns]')
        axs[1].set_ylabel('Counts in ToT bin')
        plt.show()
        #subprocess.run(["xdotool", "search", "--name", "Figure", "windowmove", "1000", "1000"])
    x_corrected = x - exponential(y, *params)
    return x_corrected

def time_walk_correction_fit(x,y,xbin_width,ybin_width):
    nybins = int(np.abs((max(y)-min(y))/ybin_width))
    bin_edges = np.linspace(min(y), max(y), num=nybins)
    # Calculate the mean of y variable within each bin
    bin_means = []
    x_corrected = np.zeros_like(x)
    for i in range(len(bin_edges) - 1):
        mask = (y >= bin_edges[i]) & (y < bin_edges[i + 1])
        x_binned = x[mask]
        y_binned = y[mask]
        #print(mask)
        #print(y[mask])
        bin_mean = np.mean(x[mask])
        params, pcov = curve_fit(generalized_gaussian, x_binned, y_binned, p0=[-0.6, 1, 1], maxfev=10000)
        bin_means.append(bin_mean)
        for j in range(len(x)):
            if mask[j]:
                x_corr = x[j] - bin_mean
                x_corrected[j] = x_corr
    return x_corrected



def plot_gaussexp(data, nbins, initial_params, title, efficiency, n, bounds, xliml, xlimr, filename):
    params_temp = []
    perr_temp = []

    x = ROOT.RooRealVar("x", "x", -10, 10)
    mean = ROOT.RooRealVar("mean", "mean of gaussian", -0.666, -2, 2)
    sigma = ROOT.RooRealVar("sigma", "width of gaussian", 0.81, 0.1, 2)
    k_l = ROOT.RooRealVar("k_l", "lower transition", 1.115, -100, 100)
    k_h = ROOT.RooRealVar("k_h", "upper transition", 0.872, -100, 100)
    root_data = ROOT.RooDataSet.from_numpy({"x": data}, [x])

    xframe = x.frame(Title="Fitted ExpGaussExp")
    root_data.plotOn(xframe)

    binwidth = xframe.getFitRangeBinW()

    ExpGaussExp_string = '(x-mean) <= -k_l * sigma ? exp(k_l**2/2+k_l*((x-mean)/sigma)) : ((x-mean) <= k_h * sigma ? exp(-0.5*((x-mean)/sigma)**2) : exp(k_h**2/2-k_h*((x-mean)/sigma)))'

    args = ROOT.RooArgList(x, mean, sigma, k_l, k_h)
    model = ROOT.RooGenericPdf('ExpGaussExp', 'Exp', ExpGaussExp_string , args)

    fit_results = model.fitTo(root_data, ROOT.RooFit.Range("fit"), ROOT.RooFit.Save())
    model.plotOn(xframe, ROOT.RooFit.Range("fit"), ROOT.RooFit.NormRange("fit"))
    model.paramOn(xframe)
    params_from_root = fit_results.floatParsFinal()

    # Create a RooRealVar for the integral
    integral_var = ROOT.RooRealVar("integral_var", "Integral", 0.0)

    # Create the integral
    integral = model.createIntegral(ROOT.RooArgSet(x), ROOT.RooFit.Range("fit"))

    # Set the value of the integral_var to the result of the integral
    integral_var.setVal(integral.getVal())

    for i in range(params_from_root.getSize()):
        param = params_from_root[i]
        params_temp.append(params_from_root[i].getVal())
        perr_temp.append(params_from_root[i].getError())
        print(f"Parameter {param.GetName()}: Value = {param.getVal()}, Error = {param.getError()}")
    
    params = [params_temp[2], params_temp[3], params_temp[1], params_temp[0]]
    perr = [perr_temp[2], perr_temp[3], perr_temp[1], perr_temp[0]]

    mean.setVal(params[0])
    sigma.setVal(params[1])
    k_l.setVal(params[2])
    k_h.setVal(params[3])


    #gauss.plotOn(xframe)
    # Draw RooFit plot on a canvas.
    #c = ROOT.TCanvas("rf409_NumPyPandasToRooFit", "rf409_NumPyPandasToRooFit", 800, 400)
    #xframe.Draw()
    #c.Show()

    nbins = int((xlimr-xliml)/binwidth)
    counts, bin_edges = np.histogram(data, bins=nbins, range=(xliml, xlimr))
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    amplitude = sum(binwidth*counts) / integral_var.getVal()

    
    #calculate scaling factor for matplotlib fit display
    #amplitude, pamplitude = curve_fit(lambda x, amplitude: scalingCalc(x, amplitude,params_temp[2], params_temp[3], params_temp[1], params_temp[0]), bin_centers, counts)#, p0=initial_params, maxfev=10000, bounds=bounds
    

    bootstrap_params = []
    bootstrap_fail_counter = 0
    for _ in tqdm(list(range(NBOOTSTRAP))):
        bootstrap_sample = np.random.choice(data, size=len(data), replace=True)
        boot_params_temp = []
        boot_root_data = ROOT.RooDataSet.from_numpy({"x": bootstrap_sample}, [x])
        
        try:
            boot_fit_results = model.fitTo(boot_root_data, ROOT.RooFit.Range("fit"), ROOT.RooFit.Save())
            boot_params_from_root = boot_fit_results.floatParsFinal()
            for i in range(boot_params_from_root.getSize()):
                #boot_param = boot_params_from_root[i]
                boot_params_temp.append(boot_params_from_root[i].getVal())
                #boot_perr_temp.append(boot_params_from_root[i].getError())
                #print(f"Parameter {param.GetName()}: Value = {param.getVal()}, Error = {param.getError()}")
    
            boot_params = [boot_params_temp[2], boot_params_temp[3], boot_params_temp[1], boot_params_temp[0]]
            bootstrap_params.append(boot_params)
            #print(p)
            #plt.plot(np.linspace(xliml, xlimr,1000), amplitude*DoubleGaussExp(np.linspace(xliml, xlimr,1000), *boot_params), 'r-', label='GaussExp Fit')
            #plt.show()
            #plot(bootstrap_sample, xliml, xlimr, nbins, 'bootstrap')
        except:
            bootstrap_fail_counter += 1
            print('Bootstrap iteration failed!')

    bootstrap_params = np.array(bootstrap_params)
    bootstrapping_fails.append(bootstrap_fail_counter)

    def calc_fwhm(params):
        fitted_data = amplitude*DoubleGaussExp(bin_centers, *params)
        deviation = counts - fitted_data
        err_counts = np.sqrt(counts)
        for i in range(len(err_counts)):
            if err_counts[i] <= 1:
                err_counts[i] = 1
        #print(err_counts)
        residuals = deviation/err_counts
        #reduced_chi_sq = np.sum(((counts - fitted_data) / err_counts) ** 2 /(len(counts) - len(params)))
        reduced_chi_sq = np.sum(residuals ** 2 /(len(counts) - len(params)))
        #print(reduced_chi_sq)
        fitted_data = DoubleGaussExp(np.linspace(xliml, xlimr,10000), *params)
        spline = UnivariateSpline(np.linspace(xliml, xlimr,10000), fitted_data-np.max(fitted_data)/2, s=0)
        try:
            r1, r2 = spline.roots() # find the roots
            fwhm = abs(r2-r1)
        except:
            fwhm = -1
            r1 = r2 = 0
            print("derp")
        return fwhm, residuals, reduced_chi_sq, r1, r2, err_counts
    fwhm, residuals, reduced_chi_sq, r1, r2, err_counts = calc_fwhm(params)

    bootstrap_fwhm = np.array([calc_fwhm(p)[0] for p in bootstrap_params])
    err_fwhm = np.std(bootstrap_fwhm[bootstrap_fwhm > 0])




    # Plot the histogram and the fitted data
    fig = plt.figure(figsize=(8, 6))
    gs = fig.add_gridspec(2, hspace=0.3)
    axs = gs.subplots(sharex = False)
    #plt.figure(figsize=(8, 6))
    axs[0].hist(data, bins=nbins, alpha=0, density = False)
    axs[0].errorbar(bin_centers, counts, yerr = err_counts,fmt = ' ', marker='.', color = 'black', label='Data', ecolor = 'blue', capsize = 3)
    axs[0].plot(np.linspace(xliml, xlimr,1000), amplitude*DoubleGaussExp(np.linspace(xliml, xlimr,1000), *params), 'r-', label='ExpGaussExp Fit')
    axs[0].set_ylim(0,1.1*(max(counts)+max(err_counts)))
    axs[0].set_xlim(xliml,xlimr)
    axs[0].set_xlabel('Timediffrence [ns]')
    axs[0].set_ylabel('Counts')
    axs[0].set_position([0.1, 0.35, 0.8, 0.55])
    axs[0].set_title(title)
    description = ""\
    + r"$\mu$ = "+f'{params[0]:.3f}'+ r"$\pm$" +f'{perr[0]:.3f}' + 'ns'+ "\n"\
    + r"$\sigma$ = "+f'{params[1]:.3f}'+ r"$\pm$" + f'{perr[1]:.3f}' + 'ns'+ "\n"\
    + r"$k_L$ = "+f'{params[2]:.3f}' + r"$\pm$" + f'{perr[2]:.3f}' + "\n"\
    + r"$k_H$ = "+f'{params[3]:.3f}' + r"$\pm$" + f'{perr[3]:.3f}' + "\n"\
    + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
    + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n"\
    + r'FWHM =' + f'{fwhm:.3f}' + r"$\pm$" + f'{err_fwhm:.3f}' + ' ns'\
    #+ r"$A$ = "+f'{params[4]:.3f}'+ r"$\pm$" + f'{perr[4]:.3f}' + "\n"\
    axs[0].text(1.1, 5.3, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

    axs[1].scatter(bin_centers,residuals, marker='+')
    axs[1].set_xlim(xliml,xlimr)
    axs[1].set_xlabel('Timediffrence [ns]')
    axs[1].set_ylabel('Pulls')
    axs[1].set_position([0.1, 0.1, 0.8, 0.15])
    axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
    axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
    resolutions.append(fwhm)
    dres_stat.append(err_fwhm)
    dres_sys.append(fwhm-(params[1]*2.354820045))
    #plt.savefig('timing_analyse_measurements_plots/double_gaussexp/double_gaussexp_' + title.replace(' ', '').replace('.', '') + filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') + '.png', bbox_inches='tight')
    #print("Integral over the fit range:", normalization_factor)
    #print(amplitude)
    #plt.show()
    plt.close()

def plot_2d(first_diff,tot_12,tot_34, title, xliml, xlimr, filename):

    fig, ax = plt.subplots()
    first_diff_ns = np.array(first_diff)
    tot_34_ns = np.array(tot_34)
    xbinwidth=0.4
    num_binsx = int((np.max(first_diff_ns)-np.min(first_diff_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(first_diff_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax.hist2d(first_diff_ns, tot_34_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax.hist2d(first_diff_ns, tot_34_ns, bins=300)
    fig.colorbar(h[3], ax=ax)
    plt.title(title)
    plt.xlabel('Timedifference [ns]')
    if title != '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    else:
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    if title != 'Only Logic Board':
        plt.xlim(xliml,xlimr)
        plt.ylim(0,100)
    if title == 'Only Logic Board':
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '')+ '_top' + filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') + '.png', bbox_inches='tight')
    #plt.show()
    plt.close()

    fig1, ax1 = plt.subplots()
    tot_12_ns = np.array(tot_12)
    #first_diff_ns = time_walk_correction(first_diff_ns, tot_12_ns, 5)
    xbinwidth=0.4
    num_binsx = int((np.max(first_diff_ns)-np.min(first_diff_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(first_diff_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax1.hist2d(first_diff_ns, tot_12_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax1.hist2d(first_diff_ns, tot_34_ns, bins=300)
    fig1.colorbar(h[3], ax=ax1)
    plt.title(title)
    plt.xlabel('Timedifference [ns]')
    plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    if title != '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    else:
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    if title != 'Only Logic Board':
        plt.xlim(xliml,xlimr)
        plt.ylim(0,100)
    if title == 'Only Logic Board':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '')+ '_bottom' + filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') +'.png', bbox_inches='tight')
    #plt.show()
    plt.close()

    fig2, ax2 = plt.subplots()
    xbinwidth=2
    num_binsx = int((np.max(tot_12_ns)-np.min(tot_12_ns))/xbinwidth)
    ybinwidth=2
    num_binsy = int((np.max(tot_34_ns)-np.min(tot_34_ns))/ybinwidth)
    if title != 'Only Logic Board':
        h = ax2.hist2d(tot_12_ns, tot_34_ns, bins=[num_binsx,num_binsy])
    else:
        h = ax2.hist2d(tot_12_ns, tot_34_ns, bins=300)
    fig2.colorbar(h[3], ax=ax2)
    plt.title(title)
    plt.xlabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
    if title == '10k Measurements Swapped Layers':
        plt.ylabel(r'ToT$_{\mathrm{Top}}$ [ns]')
        plt.ylabel(r'ToT$_{\mathrm{Bottom}}$ [ns]')
    plt.xlim(0,100)
    plt.ylim(0,100)
    plt.savefig('timing_analyse_measurements_plots/2d_histo/2d_histo' + title.replace(' ', '').replace('.', '')+ '_tot'+ filename.replace('timing_resolution_measurement/measurement', '').replace('.txt', '') +'.png' , bbox_inches='tight')
    #plt.show()
    plt.close()

def multiplots():
    #plot_gaussian_sum(first_diff*10**9, num_bins, initial_params_sum, titles[i], efficiency, n, bounds_sum)
    th = [-180, -160, -140, -120, -100, -80, -60, -50, -40, -30]
    print(titles[8:15]+titles[51:54])
    print(np.append(resolutions[8:15],resolutions[51:54])*np.sqrt(2))
    print(np.append(dres_stat[8:15], dres_stat[51:54])*np.sqrt(2))
    print(np.append(dres_sys[8:15], dres_sys[51:54]))
    plt.errorbar(th, np.append(resolutions[8:15],resolutions[51:54]), yerr = [np.zeros_like(th),np.append(dres_stat[8:15]+dres_sys[8:15],dres_stat[51:54]+dres_sys[51:54])], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(th, np.append(resolutions[8:15],resolutions[51:54]), yerr = np.append(dres_stat[8:15],dres_stat[51:54]), fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('Logic Board Threshold [mV]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'logic_th_57_21_bias_9_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    bias = [57.8, 57.4, 56.6, 58.8, 58.4, 58, 57.6, 57.2, 56.8, 56.4]
    print(titles[16:26])
    print(resolutions[16:26]*np.sqrt(2))
    print(dres_stat[16:26]*np.sqrt(2))
    print(dres_sys[16:26])
    plt.errorbar(bias, resolutions[16:26], yerr = [np.zeros_like(bias),dres_stat[16:26]+dres_sys[16:26]], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(bias, resolutions[16:26], yerr = dres_stat[16:26], fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('SiPM Bias [V]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'sipm_bias_140_th_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    bias = [58.71, 58.21, 57.71, 58.71, 59.21]
    print(titles[26:29]+titles[49:51])
    print(np.append(resolutions[26:29], resolutions[49:51])*np.sqrt(2))
    print(np.append(dres_stat[26:29], dres_stat[49:51])*np.sqrt(2))
    print(np.append(dres_sys[26:29], dres_sys[49:51]))
    plt.errorbar(bias, np.append(resolutions[26:29], resolutions[49:51]), yerr = [np.zeros_like(bias),np.append(dres_stat[26:29]+dres_sys[26:29], dres_stat[49:51]+dres_sys[49:51])], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(bias, np.append(resolutions[26:29], resolutions[49:51]), yerr = np.append(dres_stat[26:29], dres_stat[49:51]), fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('SiPM Bias [V]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'sipm_bias_70_th_9_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    th = [-110, -115, -120, -125, -130, -135, -140, -145, -150, -155, -160, -165, -170]
    print(titles[31:44])
    print(resolutions[31:44]*np.sqrt(2))
    print(dres_stat[31:44]*np.sqrt(2))
    print(dres_sys[31:44])
    plt.errorbar(th, resolutions[31:44], yerr = [np.zeros_like(th),dres_stat[31:44] + dres_sys[31:44]], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(th, resolutions[31:44], yerr = dres_stat[31:44], fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('Logic Board Threshold [mV]')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'logic_th_57_2_bias_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    counts = [10000, 5000, 3000, 2000]
    print(titles[3:7])
    print(resolutions[3:7]*np.sqrt(2))
    print(dres_stat[3:7]*np.sqrt(2))
    print(dres_sys[3:7])
    plt.errorbar(counts, resolutions[3:7], yerr = [np.zeros_like(counts),dres_stat[3:7]+dres_sys[3:7]], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(counts, resolutions[3:7], yerr = dres_stat[3:7], fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('Number of Measurements')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'counts_140_th_57_2_bias_5_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    amp = [9,5,6,7,8,10,11]
    print(titles[29:31]+titles[44:49])
    print(np.append(resolutions[29:31],resolutions[44:49])*np.sqrt(2))
    print(np.append(dres_stat[29:31], dres_stat[44:49])*np.sqrt(2))
    print(np.append(dres_sys[29:31], dres_sys[44:49]))
    plt.errorbar(amp, np.append(resolutions[29:31],resolutions[44:49]), yerr = [np.zeros_like(amp),np.append(dres_stat[29:31]+dres_sys[29:31],dres_stat[44:49]+dres_sys[44:49])], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(amp, np.append(resolutions[29:31],resolutions[44:49]), yerr = np.append(dres_stat[29:31], dres_stat[44:49]), fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('Supply Voltage of Amplifier')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'amp_supply_57_21_bias_70_th'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

    counts = [1000, 2000, 3000, 4000, 5000, 6500, 8000, 10000]
    print(titles[69:77])
    print(resolutions[69:77]*np.sqrt(2))
    print(dres_stat[69:77]*np.sqrt(2))
    print(dres_sys[69:77])
    plt.errorbar(counts, resolutions[69:77], yerr = [np.zeros_like(counts),dres_stat[69:77]+dres_sys[69:77]], fmt='None', linestyle='', ecolor='blue', capsize=3)
    plt.errorbar(counts, resolutions[69:77], yerr = dres_stat[69:77], fmt='x', linestyle='', ecolor='red', color='black', capsize=3)
    plt.xlabel('Number of Measurements')
    plt.ylabel('Time Resolution [ns]')
    plt.savefig('timing_analyse_measurements_plots/multiplots/'+'counts_60_th_57_2_bias_9_amp'+'.png', bbox_inches='tight')
    plt.show()
    plt.close()

def read_data(filename):
    measurement_number = []
    first_intersection_12 = []
    second_intersection_12 = []
    tot_12 = []
    first_intersection_34 = []
    second_intersection_34 = []
    tot_34 = []
    chisquare_red = []
    first_diff = []
    first_diff_inv = []
    second_diff = []
    # Open the file in read mode
    with open(filename, 'r') as file:
        # Read the lines of the file
        lines = file.readlines()

    # Process the lines, excluding the first two lines as headers
    for line in lines[2:]:
        # Remove leading/trailing whitespace and split the line by comma
        values = line.strip().split(' ')

        # Extract the measurement number
        measurement_number.append(int(values[0]))
        first_intersection_12.append(float(values[1].strip('[]')))
        second_intersection_12.append(float(values[2].strip('[]')))
        tot_12.append(float(values[3].strip('[]')))
        first_intersection_34.append(float(values[4].strip('[]')))
        second_intersection_34.append(float(values[5].strip('[]')))
        tot_34.append(float(values[6].strip('[]')))
        #if np.abs(float(values[1].strip('[]'))-float(values[1 V SiPM Bias','-1104].strip('[]'))) < 20e-9:
        if np.abs(float(values[3].strip('[]'))) > 0e-9:
            first_diff.append(float(values[1].strip('[]'))-float(values[4].strip('[]')))#turn on 12 (usually bottom) - turn on 34 (usually top)
            first_diff_inv.append(float(values[4].strip('[]'))-float(values[1].strip('[]')))
        second_diff.append(float(values[2].strip('[]'))-float(values[5].strip('[]')))
    efficiency = len(measurement_number)/measurement_number[len(measurement_number)-1]
    n = str(measurement_number[len(measurement_number)-1])
    return np.array(first_diff), np.array(tot_12), np.array(tot_34), efficiency, n

start_time = time.time()

resolutions = []
dres_stat = []
dres_sys = []
#FWHMs = [] not in use
#dFWHMs = []

tot_12_for_comp=[]
tot_34_for_comp=[]


print('Total number of files to be analyzed: '+str(len(filenames)))
print('')

for i in range(len(filenames)):
    first_diff, tot_12, tot_34, efficiency, n = read_data(filenames[i])  # Example data, normally distributed
    first_diff= first_diff*10**9
    tot_12 = tot_12*10**9
    tot_34 = tot_34*10**9
    binwidth=0.5
    bootstrapping_fails = []

    if i < 0:
        resolutions.append(0)
        dres_stat.append(0)
        dres_sys.append(0)
        continue
    initial_params= [-0.4, 1, 80]  # Starting parameters for the fit
    bounds = [[-2e-9, -1e-9, 0], [2e-9, 7e-9, 1000]]
    initial_params_sum = [0.1, 1, 1000, 0.1, 2, 200]  # Starting parameters for the fit 
    bounds_sum = [[-2, 0, 0, -2, 0, 0], [2, np.inf, np.inf, 2, np.inf, np.inf]]
    #bounds_sum = [[-np.inf, 0, 0, -np.inf, 0, 0], [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]]
    bounds_gaussexp = [[-5, 0, 0, 0, 0], [5, np.inf, np.inf, np.inf, np.inf]]
    #bounds_gaussexp = [[-2, 0, -np.inf, -np.inf, -np.inf], [2, np.inf, np.inf, np.inf, np.inf]]
    xliml = -10
    xlimr = 10

    print('Number of current file: '+str(i+1))
    print('Title of current file: '+ str(titles[i]))
    if (i==2 or i==3):
        num_bins = 20
        #plot(tot_12, num_bins, titles[i])
        #plot(tot_34, num_bins, titles[i])
    if i >= 60 and i < 69:
        num_bins = 100
        plot_nolim(first_diff, num_bins, titles[i])
        resolutions.append(0)
        dres_stat.append(0)
        dres_sys.append(0)
        continue
    if (titles[i] != 'Only Logic Board'):
        #first_diff = time_walk_correction(first_diff, tot_12, 5, True)
        #first_diff = time_walk_correction(first_diff, tot_34, 2, True)
        num_bins = int((np.max(first_diff)-np.min(first_diff))/binwidth)  # Number of bins for the histogram
        #print(np.max(first_diff),np.min(first_diff),np.max(first_diff)-np.min(first_diff),(np.max(first_diff)-np.min(first_diff))/binwidth, num_bins)
        counts, bin_edges = np.histogram(first_diff, bins=num_bins)
        initial_params_gaussexp = [-0.6, 0.81, 1.1, 0.87, max(counts)]
        #plot_generalized_gaussian(first_diff, num_bins, initial_params, titles[i], efficiency, n, xliml, xlimr, filenames[i])
        #plot_gaussian_sum(first_diff, num_bins, initial_params_sum, titles[i], efficiency, n, bounds_sum, xliml, xlimr, filenames[i])
        #plot_gaussexp(first_diff, num_bins, initial_params_gaussexp, titles[i], efficiency, n, bounds_gaussexp, xliml, xlimr, filenames[i])
        #plot_2d(first_diff, tot_12, tot_34, titles[i], xliml, xlimr, filenames[i])
    else:
        num_bins = 100
        plot_nolim(first_diff, num_bins, titles[i])
        resolutions.append(0)
        dres_stat.append(0)
        dres_sys.append(0)
    print('')

resolutions = resolutions/np.sqrt(2)
dres_stat = dres_stat/np.sqrt(2)
#print(titles[29:31]+titles[44:49])
if len(resolutions) == len(dres_stat) == len(dres_sys) == len(titles) == len(filenames):
    print('Lengths of data arrays as expected.')
else:
    print('Caution missmatch of data array lengths! This will likely break Plots!')

#multiplots()

print('This is how often bootstrapping failed: '+str(bootstrapping_fails)+' of '+str(NBOOTSTRAP)+' failed.')

end_time = time.time()
elapsed_time = end_time - start_time
print("Total analysis duration: {:.3f} seconds".format(elapsed_time))
print("Enjoy your data!")