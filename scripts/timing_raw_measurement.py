import pyvisa
import pylab as pl
import matplotlib.pyplot as plt
import struct
import math
import warnings
import gc
import time
from datetime import datetime
import numpy as np
import scipy
import threading

tdiffs = []

# Define the error functions
def errorfunc(x, a, b, c, d):
    x = np.array(x)
    return a*np.array(scipy.special.erf((x-b)*c))+d

# Define the difference of the error functions
def diff_error(x, a, b, c, d, e, f, g, h):
    x = np.array(x)
    return errorfunc(x, a, b, c, d) + errorfunc(x, e, f, g, h)


"""Modify the following global variables according to the model"""
#SDS_RSC = 'TCPIP::131.220.218.137::inst0::INSTR'
SDS_RSC = 'TCPIP::131.220.218.128::inst0::INSTR'
CHANNEL = ["C1","C2","C3","C4"]
HORI_NUM = 10 #default 10
vertical_divisions = 8
tdiv_enum = [100e-12, 200e-12, 500e-12, 1e-9,
2e-9, 5e-9, 10e-9, 20e-9, 50e-9, 100e-9, 200e-9, 500e-9, \
1e-6, 2e-6, 5e-6, 10e-6, 20e-6, 50e-6, 100e-6, 200e-6, 500e-6, \
1e-3, 2e-3, 5e-3, 10e-3, 20e-3, 50e-3, 100e-3, 200e-3, 500e-3, \
1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]#remove 100e-12, for slow osci and add for fast

"""The following code realizes the process of waveform reconstruction with slice"""
#establishing connection to osci and setting length of eceived data
_rm = pyvisa.ResourceManager()
sds = _rm.open_resource(SDS_RSC)
sds.timeout = 30000 # default value is 2000(2s)
sds.chunk_size = 20 * 1024 * 1024 # default value is 20*1024(20k bytes)

#this function gets the settings of the osci contained in the preamble

def main_desc(recv):

#receiving raw data from osci
    WAVE_ARRAY_1 = recv[0x3c:0x3f + 1]
    wave_array_count = recv[0x74:0x77 + 1]
    first_point = recv[0x84:0x87 + 1]
    sp = recv[0x88:0x8b + 1]
    v_scale = recv[0x9c:0x9f + 1]
    v_offset = recv[0xa0:0xa3 + 1]
    interval = recv[0xb0:0xb3 + 1]
    code_per_div = recv[0xa4:0Xa7 + 1]#
    adc_bit = recv[0xac:0Xad + 1]
    delay = recv[0xb4:0xbb + 1]
    tdiv = recv[0x144:0x145 + 1]
    probe = recv[0x148:0x14b + 1]

#converting raw data into values
    data_bytes = struct.unpack('i', WAVE_ARRAY_1)[0]
    point_num = struct.unpack('i', wave_array_count)[0]
    fp = struct.unpack('i', first_point)[0]
    sp = struct.unpack('i', sp)[0]
    interval = struct.unpack('f', interval)[0]
    delay = struct.unpack('d', delay)[0]
    tdiv_index = struct.unpack('h', tdiv)[0]
    probe = struct.unpack('f', probe)[0]
    vdiv = struct.unpack('f', v_scale)[0] * probe
    offset = struct.unpack('f', v_offset)[0] * probe
    code = struct.unpack('f', code_per_div)[0]
    adc_bit = struct.unpack('h', adc_bit)[0]
    tdiv = tdiv_enum[tdiv_index]

    return vdiv, offset, interval, delay, tdiv, code, adc_bit

#this function gets waveforms

def main_wf_data(number_ch):

#setting up to read waveform data
    sds.write(":WAVeform:STARt 0")
    sds.write("WAV:SOUR {}".format(CHANNEL[number_ch]))
    sds.write("WAV:PREamble?")
    recv_all = sds.read_raw()
    recv = recv_all[recv_all.find(b'#') + 11:]
    vdiv, ofst, interval, trdl, tdiv, vcode_per, adc_bit = main_desc(recv)
    points = sds.query(":ACQuire:POINts?").strip()
    points = float(sds.query(":ACQuire:POINts?").strip())
    one_piece_num = float(sds.query(":WAVeform:MAXPoint?").strip())
    if points > one_piece_num:
        sds.write(":WAVeform:POINt {}".format(one_piece_num))
    if adc_bit > 8:
        sds.write(":WAVeform:WIDTh WORD")
    read_times = math.ceil(points / one_piece_num)
    recv_all = []

#reading raw waveform data for a single channel
    for i in range(0, read_times):
        start = i * one_piece_num
        sds.write(":WAVeform:STARt {}".format(start))
        sds.write("WAV:DATA?")
        recv_rtn = sds.read_raw().rstrip()
        block_start = recv_rtn.find(b'#')
        data_digit = int(recv_rtn[block_start + 1:block_start + 2])
        data_start = block_start + 2 + data_digit
        recv = list(recv_rtn[data_start:])
        recv_all += recv

    convert_data = []
    if adc_bit > 8:
        for i in range(0, int(len(recv_all) / 2)):
            data = recv_all[2 * i + 1] * 256 + recv_all[2 * i]
            convert_data.append(data)
    else:
        convert_data = recv_all
    volt_value = []
    for data in convert_data:
        if data > pow(2, adc_bit - 1) - 1:
            data = data - pow(2, adc_bit)
        else:
            pass
        volt_value.append(data)
    del recv, recv_all, convert_data
    gc.collect()

#converting raw data into values with units
    time_value = []
    for idx in range(0, len(volt_value)):
        #print(volt_value[idx])
        #print(vcode_per)
        #exprint(ofst)
        volt_value[idx] = volt_value[idx] / vcode_per * float(vdiv) - float(ofst) #add *vertical_divisions/2 for slow osci !!!!!!!!! problem with original code: full measurement range is from -vcode_per to + vcode_per -> need to scale voltage per division with total number of divisions
        time_data = -(float(tdiv) * HORI_NUM / 2) + idx * interval - float(trdl) #interval should be divided by 2? not anymore????????????? was maybe broken because of incorrect query for trigger status? trdl had wrong sign
        time_value.append(time_data)
    return time_value, volt_value, vcode_per, vdiv

#running the measurement: setting up output file, waiting for trigger of osci, extracting waveforms of all channels, fitting functions to data, determening intersections, time over threshold and writting them to output file
if __name__=='__main__':
    start_time = time.time()
    #warnings.filterwarnings("error")
    intersect_resolution = 100000 #OG 100k
    chi_barrier = 100000
    run_loop = True
    dt_object = datetime.fromtimestamp(start_time)
    filename = dt_object.strftime("timing_resolution_measurement/measurement_%d-%m-%Y_%H-%M-%S.txt")
    with open(filename, 'w') as f:
        f.write('intersection resolution = {}, maximum allowed chisquare_red = {}\nMeasurement number, 1st intersection 12, 2nd intersection 12, tot 12, 1st intersection 34, 2nd intersection 34, tot 34, chisquare_red \n'.format(intersect_resolution, chi_barrier))
    def measurement_loop():
        global run_loop
        global plotting
        num_channels = 4
        measurement_number = 0
        time_min = 0
        time_max = 0
        reduced_chi_sq = 0
        while run_loop:
        #while measurement_number<1000:
            fit_fail_flag = False
            wait_time = 0
            all_reduced_chi_sq = []
            measurement_number += 1
            fit_params = []
            err_fit_params = []
            print("\nThis is Measurement {}.".format(measurement_number))
            #time.sleep(3)
            sds.write("TRIG:MODE SING")
            while True:
                time.sleep(0.001)
                wait_time+=1
                trigger_stat = sds.query('TRIG:STAT?')
                if trigger_stat == "Stop\n":
                    print("Trigger detected! Waited {:.3f} second(s) for trigger.".format(wait_time/1000))
                    break
            for i in range(num_channels):
                time_data, volt_data, vcode_per, vdiv = main_wf_data(i)
                #print('len data time, volt')
                #print(time_data)
                #print(volt_data)
                time_min = time_data[0]
                time_max = time_data[len(time_data)-1]
                amplitude = (np.max(volt_data) - np.min(volt_data))/2
                delta_volt_value = np.empty(len(volt_data)-1)
                for j in range(len(volt_data)-1):
                    delta_volt_value[j] = volt_data[j]-volt_data[j+1]
                jump1 = time_data[np.argmin(delta_volt_value)]
                jump2 = time_data[np.argmax(delta_volt_value)]
                if jump2 < jump1:
                    temp = jump2
                    jump2 = jump1
                    jump1 = temp
                slew_rate = 10**(9)
                center = np.max(volt_data) - amplitude
                p0 = [-amplitude, jump1, slew_rate, center/2, amplitude, jump2, slew_rate, center/2]
                #p0 = [-0.055, -5*10**(-9), 10**(9), 0., 0.055, -1*10**(-9), 10**(9), 0.32]
                #bounds = ([-0.07,-1.,10**(8), 0.2,0.04,-1.,10**(8), 0.2],[-0.03,1.,10**(10), 0.4,0.07,1.,10**(9), 0.4])
                try:
                    popt, pcov = scipy.optimize.curve_fit(diff_error, time_data, volt_data, p0 = p0, maxfev = 100000)#, bounds = bounds, p0 = [0.05, -1., 0.6, 0.05, -1., 0.6] a*np.array(scipy.special.erf((x-b)*c))+d
                    fit_params.append(popt)
                    #print(fit_params)
                    #print(popt)
                    err_fit_params.append(pcov)
                    yerr = np.ones_like(volt_data)/vcode_per * float(vdiv)*vertical_divisions/2 
                    reduced_chi_sq = (1.0 / (len(volt_data) - len(popt))) * np.sum(((volt_data - diff_error(time_data, *popt)) / yerr) ** 2)
                    all_reduced_chi_sq.append(reduced_chi_sq)
                    if plotting:
                        plt.scatter(time_data, volt_data, label='Data '+str(i))
                        plot_time = np.linspace(np.min(time_data),np.max(time_data), 1000)
                        plt.plot(plot_time, diff_error(plot_time, *popt), label='Fit '+str(i))
                        plt.legend()
                        #print(popt)
                        if i ==3:#i == 1 or 
                            plt.grid(True)
                            plt.xlim(-1*10**(-9), 1*10**(-9))
                            plt.show()
                    if reduced_chi_sq > chi_barrier:
                        fit_fail_flag = True
                        print("Fit deviates to much from data. Reduced Chisquare: {}.".format(reduced_chi_sq))
                        break
                    if np.abs(popt[0]) < 0.001:
                        fit_fail_flag = True
                        print("Detected only a single pulse1.")
                        break
                    if np.abs(popt[4]) < 0.001:
                        fit_fail_flag = True
                        #print(np.abs(popt[0]))
                        #print(np.abs(popt[4]))
                        print("Detected only a single pulse2.")
                        break
                except:
                    fit_fail_flag = True
                    print("Fit did not converge or covariannce could not be estimated.")
                    break
                
            if fit_fail_flag:
                print("Skipping to next trigger.")
                continue
            try:
                #fit_params_len = len(fit_params[0])
                output = "{} ".format(measurement_number)
                #print(output)
                first_intersect =0
                second_intersect =0
                for i in [0,2]:
                    intersect_fraction = ((fit_params[i][5]+fit_params[i][1])/2-time_min)/(time_max - time_min)
                    #print(intersect_fraction)
                    time_space_first = np.linspace(time_min, (fit_params[i][1]+fit_params[i][5])/2, int(intersect_fraction*intersect_resolution))
                    time_space_second = np.linspace((fit_params[i][1]+fit_params[i][5])/2, time_max, int((1-intersect_fraction)*intersect_resolution))
                    intersect1 = time_space_first[np.abs(diff_error(time_space_first,*fit_params[i]) - diff_error(time_space_first,*fit_params[i+1])).argsort()[:1]]
                    intersect2 = time_space_second[np.abs(diff_error(time_space_second,*fit_params[i]) - diff_error(time_space_second,*fit_params[i+1])).argsort()[:1]]#+int(intersect_fraction*intersect_resolution)]
                    #print("{} {} {} ".format(intersect1, intersect2, intersect2-intersect1))
                    if i == 0:
                        first_intersect = intersect1
                    if i == 2:
                        second_intersect = intersect1
                    output += "{} {} {} ".format(intersect1, intersect2, intersect2-intersect1)
                with open(filename, 'a') as f:
                    f.write(output+"{}\n".format(all_reduced_chi_sq))
                #print(output+"{}\n".format(all_reduced_chi_sq))
                print("Measurement successful.\nFound the following intersections and their difference"+output)
                print(first_intersect-second_intersect)
                tdiffs.append(first_intersect-second_intersect)
            except:
                print("Could not determine intersections. \nSkipping to next trigger.")
                continue
    # Start the loop in a separate thread
    loop_thread = threading.Thread(target=measurement_loop)
    loop_thread.start()
    plotting = False
    while True:
        if plotting:
            plotting = input("Enter 'stop' to stop plotting.\n")
        if plotting == "stop":
            plotting=False
        command = input("\nEnter 'exit' to stop after the active measurement.\n")
        if command == "exit":
            print("Exiting after this measurment is complete.")
            # Set the exit flag to True
            run_loop = False
            break

# Wait for the loop thread to finish
    loop_thread.join()
    print("\nMeasurement finished.")
    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Total measurement duration: {:.3f} seconds".format(elapsed_time))
    print("Enjoy your data!")
    tdiffs = np.array(tdiffs)
    print(tdiffs)
    plt.hist(tdiffs, np.min([int(len(tdiffs)/10), 100]))
    plt.show()