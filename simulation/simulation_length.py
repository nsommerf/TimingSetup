import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
y_side = [189170, 189271, 188861, 189134, 189248, 189034]
y_err_side = np.sqrt(y_side)
x_side = [25, 30, 35, 40, 45, 50]
plt.errorbar(x_side, y_side, yerr=y_err_side, linewidth=1.0, label='side', color='red', fmt='o', markersize=8, capsize=2)
plt.axhline(y = np.mean(y_side), color = 'red', linestyle = '-', label='mean')
plt.xlabel('Length of Scintillator [mm]')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_length_side.png')
plt.show()
fig, ax = plt.subplots()
y_top = [35506, 35548, 35366, 35543, 35384, 35439]
y_err_top = np.sqrt(y_top)
x_top = [25, 30, 35, 40, 45, 50]
ax.errorbar(x_top, y_top, yerr=y_err_top, linewidth=1.0, label='top', color='blue', fmt='o', markersize=8, capsize=2)
plt.axhline(y = np.mean(y_top), color = 'blue', linestyle = '-', label='mean')
#ax.set(xlim=(0, 30))
plt.xlabel('Length of Scintillator [mm]')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_length_top.png')
plt.show()