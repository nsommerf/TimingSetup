import numpy as np
#from jax import numpy as np
#from jax import jit as jit
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.stats import beta
import random
from scipy.interpolate import UnivariateSpline

#energies in mev
#distances in cm

# Define the file paths
pvc_path = "pvc_dedx.txt"
air_path = "air_dedx.txt"
toluene_path = "toluene_dedx.txt"
copper_path = 'copper_dedx.txt'

materials = ['air', 'pvc', 'air', 'pvc', 'toluene', 'pvc', 'air', 'pvc', 'air', 'pvc']#, 'copper'
thicknesses = [1.3, 0.03, 2, 0.075, 0.15, 0.075, 0.2, 0.03, 2, 0.075]#, 0.008

# Initialize empty arrays to store data

def load_dedx(file_path, pos):
    colum = []
    # Read the file and populate the arrays
    with open(file_path, 'r') as file:
        lines = file.readlines()

    for line in lines:
        values = line.strip().split(' ')  # Split the line by spaces
        colum.append(float(values[pos]))  # Assuming the first column contains floating-point numbers

    # Convert the lists to NumPy arrays
    output = np.array(colum)

    return output

def electron_speed(momentum):
    energy = np.sqrt(np.square(momentum) + 0.511**2)
    speed = momentum/energy
    return speed

def electron_travel_time(momentum, dist):
    return dist/100/(const.c*electron_speed(momentum))*10**9

def make_counts(energy_range, spectrum, num_counts):
    counts = random.choices(energy_range, weights=spectrum, k = num_counts)
    return np.array(counts)

def energy_loss(counts, energy_loss):
    mask = counts > energy_loss
    return counts[mask]-energy_loss

def detection(energy, time, threshold):
    mask = energy > threshold
    return energy[mask], time[mask]

def theoretical_beta_spectrum(e_max, z, e_e, p_e):
    eta = z*const.alpha*(e_e+const.m_e)/p_e
    fermi = 2*const.pi*eta/(1-np.exp(-2*const.pi*eta))
    return fermi*p_e**2*(e_max-e_e)**2

def electron_momentum(e_e):
    return np.sqrt(e_e**2+const.m_e**2)

def sr90_spectrum(e_e):
    sr = theoretical_beta_spectrum(0.546, 38, e_e, np.sqrt(e_e**2+const.m_e**2))
    y = theoretical_beta_spectrum(2.28, 39, e_e, np.sqrt(e_e**2+const.m_e**2))
    result = np.where(e_e>0.546, y, sr*1163+y)
    return result

def bethe_bloch(e_e, material, thicknes):
    if material == 'pvc':
        density = 1.3
        result = density * thicknes * pvc_spline(e_e)
    elif material == 'air':
        density = 1.20479E-03
        result = density * thicknes * air_spline(e_e)
    elif material == 'toluene':
        density = 1.023
        result = density * thicknes * toluene_spline(e_e)
    elif material == 'copper':
        density = 8.96
        result = density * thicknes * toluene_spline(e_e)
    else:
        print('bethe_bloch failed! Aborting.')
        exit()
    return result

'''
def do_steps(e_e):
    step_size = 0.001
    energy = e_e
    time = 0
    for layer in range(len(materials)):
        num_steps = int(thicknesses[layer] / step_size)
        for i in range(num_steps):
            energy -= bethe_bloch(energy, materials[layer], step_size)
            if energy <= 0.001:
                time = np.inf
                continue
            if layer >= 4:
                time += electron_travel_time(energy, step_size)
    return time, energy
'''


def do_steps(e_e):
    step_size = 0.001
    energy = e_e.copy()
    time = np.zeros_like(e_e)
    for layer in range(len(materials)):
        num_steps = int(thicknesses[layer] / step_size)
        for i in range(num_steps):
            energy -= bethe_bloch(energy, materials[layer], step_size)
            if layer >= 4:
                time += electron_travel_time(energy, step_size)
    time[energy <= 0.001] = np.inf
    return time, energy




ekin = load_dedx(pvc_path,0)
dedx_pvc = load_dedx(pvc_path,1)
dedx_air = load_dedx(air_path,1)
dedx_toluene = load_dedx(toluene_path,1)
dedx_copper = load_dedx(copper_path,1)

pvc_spline = UnivariateSpline(ekin, dedx_pvc, s=0)
air_spline = UnivariateSpline(ekin, dedx_air, s=0)
toluene_spline = UnivariateSpline(ekin, dedx_toluene, s=0)
copper_spline = UnivariateSpline(ekin, dedx_copper, s=0)



'''
plt.loglog(ekin, dedx_pvc)
plt.loglog(ekin, dedx_air)
plt.loglog(ekin, dedx_toluene)
plt.show()
'''

num_counts = 100000
energy_range = np.linspace(0.001, 2.28, 100000)
#spectrum = beta.pdf(energy_range, 2.5, 10)+beta.pdf(energy_range/2.274, 3, 4)*0.4
counts = make_counts(energy_range, sr90_spectrum(energy_range), num_counts)
threshold = 0.1

time, energy = do_steps(counts)
mask = np.isfinite(time)
time = time[mask]
energy = energy[mask]
mask_det = energy >= threshold
#energy = energy[mask_det]
#time = time[mask_det]
#energy, time = detection(energy, time, threshold)



plt.figure(figsize=(8,6))
plt.hist([energy[mask_det],energy], 50, label = ['With Threshold','Without Threshold'], edgecolor='black', linewidth=0.5)
plt.title('Electron Energy Spectrum After Timing Layer')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts')
plt.axvline(np.mean(energy), label= 'Mean Energy With Detection', color = 'red')
plt.legend()
plt.show()

plt.figure(figsize=(8,6))
plt.hist([time[mask_det],time], 50, label = ['With Threshold','Without Threshold'], edgecolor='black', linewidth=0.5)
plt.title('Electron Travel Time')
plt.xlabel('Electron Travel Time [ns]')
plt.ylabel('Counts')
plt.axvline(np.mean(time[mask_det]), label= 'Mean Travel Time With Detection', color = 'red')
plt.legend()
plt.show()

plt.hist(counts, bins=50)
plt.title(r'Theoretical $\beta^-$-Spectrum of Sr90')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts')
plt.show()

plt.plot(energy_range, 0.025/(const.c*electron_speed(energy_range))*10**9, label= 'Travel Time of Electron')
plt.title('Electron Travel Time')
#plt.axvline(0.3, color = 'red')
plt.ylim(0, 1)
plt.xlim(0,2.28)
plt.axhline(0.025/const.c*10**9,linestyle = 'dashed', color= 'red', label = 'Minimum Travel Time')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Travel Time [ns]')
plt.legend()
plt.show()

plt.plot(energy_range, sr90_spectrum(energy_range))
plt.title(r'Theoretical $\beta^-$-Spectrum of Sr90')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Probability [a.u.]')
plt.show()

'''


plt.hist(energy_loss(counts, loss), bins=100)
plt.title(r'Expected $\beta^-$-Spectrum of Sr90 After First Layer')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts')
plt.show()

plt.plot(energy_range, dist/(const.c*electron_speed(energy_range))*10**9, label= 'Travel Time of Electron')
plt.title('Electron Travel Time')
#plt.axvline(0.3, color = 'red')
plt.ylim(0, 1)
plt.xlim(0,2.28)
plt.axhline(dist/const.c*10**9,linestyle = 'dashed', color= 'red', label = 'Minimum Travel Time')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Travel Time [ns]')
plt.legend()
plt.show()

names = []
for i in range(len(losses)):
    names.append(str(losses[i])+' MeV lost')

plt.hist(lost, bins=50, label= names, color=colors)
for i in range(len(means)):
    plt.axvline(means[i], color= colors[i], linestyle = 'dashed', label = 'Mean of '+str(losses[i]) + ' MeV lost')
#plt.vlines(means, 0, 60000, colors=[range(len(means))])#, label= losses
plt.title('Electron Travel Time')
plt.xlabel('Electron Travel Time [ns]')
plt.ylabel('Counts')
plt.legend()
#plt.ylim(0, 100)
#plt.xlim(0, 1)
plt.show()

'''

'''
plt.plot(energy_range, spectrum/max(spectrum))
plt.title('Energy Spectrum')
plt.axvline(0.3, color = 'red')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts [a.u.]')
plt.show()

plt.plot(energy_range, electron_speed(energy_range))
plt.title('Electron Speed')
plt.axvline(0.3, color = 'red')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Speed [c]')
plt.show()

plt.plot(energy_range, dist/(const.c*electron_speed(energy_range))*10**9)
plt.title('Electron Travel Time')
plt.axvline(0.3, color = 'red')
plt.ylim(0, 1)
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Travel Time [ns]')
plt.show()
'''