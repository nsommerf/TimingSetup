import random
import matplotlib.pyplot as plt
import numpy as np
import shapely
from shapely.geometry import LineString, Point
import time

start_time = time.time()

# not quite so Constant
#number of rays and max bounces
num_rays = 10
num_bounces = 100
#size of rectangle
rect = [0,35,0,1.5] #x1,x2,y1,y2
#size of incident area, eps is used to avoid starting on the edge(should be fine after first step should test)
#eps = 0.0000000001
eps = 0
xs_min = rect[1]-20 + eps
xs_max = rect[1] - eps
ys_min = rect[3] + eps
ys_max = rect[2] - eps
#different definitions of rectangle
a = (rect[0],rect[2])
b= (rect[0],rect[3])
c = (rect[1],rect[3])
d = (rect[1],rect[2])
left = LineString([a,b])
top = LineString([b,c])
right = LineString([c,d])
bottom = LineString([d,a])
border = [left, top, right, bottom]
left1 = (a,b)
top1 = (b,c)
right1 = (c,d)
bottom1 = (d,a)
simple_border = [(rect[0],rect[3]),(rect[1],rect[2]),(rect[0],0- rect[3]),(0-rect[1],rect[2])]
#refraction indices
n_out = 1
n_scinti = 1.58
crit_angle = np.arcsin(n_out/n_scinti)
#print('crit angle')
#print(np.rad2deg(crit_angle))
# #global variables used for crossfunction info(i know it should be avoided)
#counters to track final states
escaped_count = 0
to_many_bounces_count = 0
detected_count = 0
side_count = 0
top_count = 0
no_outcome = 0
angle = 0
hit_border=0
sipm_size = 1.3
det_top = [0,sipm_size,rect[3],rect[3]] #x1,x2,y1,y2
#det_side = [0,0,rect[3]-sipm_size,rect[3]] #x1,x2,y1,y2
det_side = [0,0,rect[3]-1.3,rect[3]] #x1,x2,y1,y2

plt.rcParams.update({'font.size': 18})

#arrays for histogramms
top_bounces = [0] * num_bounces
side_bounces = [0] * num_bounces

def angle_between(v1, v2):
	cosang = np.dot(v1, v2)
	sinang = np.linalg.norm(np.cross(v1, v2))
	#print(cosang)
	return np.arctan2(sinang, cosang)

#computes hit position
def intersects(start, angle):
	global skip_border
	x1, y1 = start
	slope = np.tan(angle)
	#print(slope)
	x2 = x1 + (rect[1]+rect[3])*np.cos(angle)
	y2 = y1 + (rect[1]+rect[3])*np.sin(angle)
	line1 = LineString([(x1,y1),(x2,y2)])
	hit = False
	for i in range(4):
		#print('\ntesting')
		#print(i)
		int_pt = line1.intersection(border[i])
		if skip_border != i:
			try:
				hit = int_pt.x, int_pt.y # remember to exclude corner cases
				global hit_border
				hit_border=i
				skip_border = i
				x2 = x_pos + (rect[1]+rect[3])*np.cos(angle)
				y2 = y_pos + (rect[1]+rect[3])*np.sin(angle)
				#plot_step(x_pos,x2,y_pos,y2)
				return hit
			except:
				no_outcome = 1
				#global no_outcome 
				#no_outcome += 1
				#print('no hit found\n')

#draws rectangle
def plot_box():
	x = np.linspace(rect[0],rect[0], 100)#x1,x2,y1,y2
	y = np.linspace(rect[2],rect[3], 100)
	ax.plot(x, y, linewidth=3.0, color="black", label='Scintillator')
	x = np.linspace(rect[0],rect[1], 100)#x1,x2,y1,y2
	y = np.linspace(rect[3],rect[3], 100)
	ax.plot(x, y, linewidth=3.0, color="black")
	x = np.linspace(rect[1],rect[1], 100)#x1,x2,y1,y2
	y = np.linspace(rect[2],rect[3], 100)
	ax.plot(x, y, linewidth=3.0, color="black")
	x = np.linspace(rect[0],rect[1], 100)#x1,x2,y1,y2
	y = np.linspace(rect[2],rect[2], 100)
	ax.plot(x, y, linewidth=3.0, color="black")
	x = np.linspace(det_top[0],det_top[1], 100)#x1,x2,y1,y2
	y = np.linspace(det_top[2],det_top[3], 100)
	ax.plot(x, y, linewidth=6.0, color="blue", label='SiPM')
	x = np.linspace(det_side[0],det_side[1], 100)#x1,x2,y1,y2
	y = np.linspace(det_side[2],det_side[3], 100)
	ax.plot(x, y, linewidth=6.0, color="blue")
	ax.set(xlim=(rect[0] - 0.1*rect[1], rect[1] + 0.1*rect[1]), ylim=(rect[2] - 0.1*rect[3], rect[3] + 0.1*rect[3]))
	#ax.set(xlim=(-50, 50), ylim=(-50, 50))

#draws light ray
def plot_step(x1,x2,y1,y2):#,color
	x = np.linspace(x1,x2, 100)#x1,x2,y1,y2
	y = np.linspace(y1,y2, 100)
	ax.plot(x, y, linewidth=3.0, color='red')

def plot_step_legend(x1,x2,y1,y2):#,color
	x = np.linspace(x1,x2, 100)#x1,x2,y1,y2
	y = np.linspace(y1,y2, 100)
	ax.plot(x, y, linewidth=3.0, label='Photon', color='red')

def detection(hit,side_flag,top_flag,j):
	global det_top
	global det_side
	if hit[0] >= det_top[0] and hit[0] <= det_top [1] and hit[1] >= det_top[2] and hit[1] <= det_top [3]:
		if top_flag ==False:
			global top_count
			global top_bounces
			top_bounces[j] +=1
			top_count += 1
	if hit[0] >= det_side[0] and hit[0] <= det_side [1] and hit[1] >= det_side[2] and hit[1] <= det_side [3]:
		if side_flag ==False:
			global side_count
			global side_bounces
			side_bounces[j] +=1
			side_count += 1

# Monte Carlo loop
for i in range(num_rays):
	skip_border = 5
	print((i+1)/num_rays*100)
	#start parameter
	x_pos = random.uniform(xs_min, xs_max)
	y_pos = random.uniform(ys_min, ys_max)
	angle = random.uniform(0, 2*np.pi)
	side_flag = False
	top_flag = False
	fig, ax = plt.subplots()
	plot_box()
	x2 = x_pos + (rect[1]+rect[3])*np.cos(angle)
	y2 = y_pos + (rect[1]+rect[3])*np.sin(angle)
	plot_step_legend(x_pos,x_pos,y_pos,y_pos)
	#print('Starting params: x_pos={}, y_pos={}, angle={}'.format(x_pos, y_pos, angle))
	for j in range(num_bounces):
		#print('this is', j)
		hit = intersects((x_pos,y_pos), angle)
		plot_step(x_pos,hit[0],y_pos,hit[1])
		#print(hit)
		detection(hit,side_flag,top_flag,j)
		skip_border=hit_border
		#print(hit_border)
		x2 = x_pos + (rect[1]+rect[3])*np.cos(angle)
		y2 = y_pos + (rect[1]+rect[3])*np.sin(angle)
		angle_check = angle_between((x2-x_pos,y2-y_pos),simple_border[hit_border])
		#print('check angle')
		#print(x_pos,y_pos)
		#print(x2,y2)
		#print('difference')
		#print((x2-x_pos,y2-y_pos),simple_border[hit_border])
		#print(np.rad2deg(angle_check))
		'''
		'''
		if angle_check  > crit_angle and angle_check  < np.pi-crit_angle:
			#print('escaped')
			escaped_count += 1
			break
		
		if hit_border == 0:
			if angle < np.pi:
				angle = angle - 2*angle_check
			if angle > np.pi:
				angle = angle + 2*(np.pi-angle_check)
		if hit_border == 1:
			if angle < 0.5*np.pi:
				angle = 2*np.pi - angle_check
			if angle > 0.5*np.pi:
				angle = 2*np.pi - angle_check
		if hit_border == 2:
			if angle < np.pi:
				angle = angle + 2*(np.pi-angle_check)
			if angle > np.pi:
				angle = angle - 2*angle_check
		if hit_border == 3:
			if angle < 1.5*np.pi:
				angle = angle - 2*angle_check
			if angle > 1.5*np.pi:
				angle = np.pi - angle_check
		#print('hit')
		#plot_step(x_pos,hit[0],y_pos,hit[1])
		try:
			x_pos = hit[0]
			y_pos = hit[1]
		except:
			no_outcome += 1
			break
		x2 = x_pos + (rect[1]+rect[3])*np.cos(angle)
		y2 = y_pos + (rect[1]+rect[3])*np.sin(angle)
		plot_step(x_pos,hit[0],y_pos,hit[1])
		#print('updated params: x_pos={}, y_pos={}, angle={}'.format(x_pos, y_pos, angle))
		if j+1 == num_bounces:
			to_many_bounces_count += 1
	ax.fill_between(np.linspace(xs_min, xs_max, 1000, endpoint=True), 0, rect[3], color='green', alpha=.2, label='Starting Area')
	#plt.rcParams["figure.figsize"] = [rect[1]/2, rect[3]/2]
	#plt.rcParams["figure.autolayout"] = True
	ax.set_xlabel('X Position [mm]')
	ax.set_ylabel('Y Position [mm]')
	ax.set_title('Python Raytracing')
	ax.legend(loc='upper right')
	plt.show()

end_time = time.time()
#fig, ax = plt.subplots()
#x = range(num_bounces)
#ax.scatter(x, top_bounces, linewidth=1.0, label='top', color='red')
#ax.scatter(x, side_bounces, linewidth=1.0, label='side', color='blue')
#ax.legend()
#ax.set(xlim=(0, 30))
#plt.xlabel('number of bounces')
#plt.ylabel('')
print("The runtime of the script is", end_time - start_time, "seconds")
print(str(escaped_count) + ' escaped')
#print(str(no_outcome) + ' no outcomes')
print(str(to_many_bounces_count) + ' to many bounces')
print(str(side_count) + ' detected on side')
print(str(top_count) + ' detected on top')
#plt.savefig('plots/'+'bounce_histo_'+str(num_rays)+'_'+str(sipm_size)+'_'+str(rect[1])+'_'+str(rect[3])+'.png')
#plt.show()
