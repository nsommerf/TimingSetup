import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
y_side = [218664, 218635, 218563, 217575, 218240, 217543, 217654, 218584, 218314, 217351]
y_err_side = np.sqrt(y_side)
x_side = range(len(y_side))
plt.errorbar(x_side, y_side, yerr=y_err_side, linewidth=1.0, label='side', color='red', fmt='o', markersize=8, capsize=2)
plt.axhline(y = np.mean(y_side), color = 'red', linestyle = '-', label='mean')
plt.xlabel('Simulation Run')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_consistency_side.png')
plt.show()
fig, ax = plt.subplots()
y_top = [53174, 53058, 53107, 53036, 53039, 52886, 52643, 53140, 53120, 52909]
y_err_top = np.sqrt(y_top)
x_top = range(len(y_top))
ax.errorbar(x_top, y_top, yerr=y_err_top, linewidth=1.0, label='top', color='blue', fmt='o', markersize=8, capsize=2)
plt.axhline(y = np.mean(y_top), color = 'blue', linestyle = '-', label='mean')
#ax.set(xlim=(0, 30))
plt.xlabel('Simulation Run')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_consistency_top.png')
plt.show()