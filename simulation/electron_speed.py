import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy.stats import beta
import random

#energies in mev
#distances in meters

dist = 0.027
m_e_mev = 0.5109989461
avogadro_number = 6.0249*10**3
avogadro_number =const.Avogadro


# Define the file path
file_path = "simulation/pvc_dedx.txt"

# Initialize empty arrays to store data
column1 = []
column2 = []

# Read the file and populate the arrays
with open(file_path, 'r') as file:
    lines = file.readlines()

for line in lines:
    values = line.strip().split(' ')  # Split the line by spaces
    column1.append(float(values[0]))  # Assuming the first column contains floating-point numbers
    column2.append(float(values[1]))  # Assuming the second column contains floating-point numbers
    print(column1)
    print(column2)

# Convert the lists to NumPy arrays
ekin = np.array(column1)
dedx = np.array(column2)

print(ekin)
print(dedx)
plt.loglog(ekin, dedx)
plt.xlim(0.01, 2.28)
plt.show()

# You can now use array1 and array2 as your NumPy arrays containing the data.


def electron_speed(momentum):
    energy = np.sqrt(momentum**2 + 0.511**2)
    speed = momentum/energy
    return speed

def electron_travel_time(momentum):
    return dist/(const.c*electron_speed(momentum))*10**9

def make_counts(energy_range, spectrum, num_counts):
    counts = random.choices(energy_range, weights=spectrum, k = num_counts)
    return np.array(counts)

def energy_loss(counts, energy_loss):
    mask = counts > energy_loss
    return counts[mask]-energy_loss

def detection(data, threshold):
    mask = data > threshold
    return data[mask]

def theoretical_beta_spectrum(e_max, z, e_e, p_e):
    eta = z*const.alpha*(e_e+const.m_e)/p_e
    fermi = 2*const.pi*eta/(1-np.exp(-2*const.pi*eta))
    return fermi*p_e**2*(e_max-e_e)**2

def electron_momentum(e_e):
    return np.sqrt(e_e**2+const.m_e**2)

def sr90_spectrum(e_e):
    sr = theoretical_beta_spectrum(0.546, 38, e_e, np.sqrt(e_e**2+const.m_e**2))
    y = theoretical_beta_spectrum(2.28, 39, e_e, np.sqrt(e_e**2+const.m_e**2))
    result = np.where(e_e>0.546, y, sr*1163+y)
    return result

def bethe_bloch(e_e, material, thicknes):
    if material == 'tape':
        i = 108.2*10**-6/m_e_mev
        z = (2*6+3+17)/6
        a = (2*12+3+35.45)/6
        density = 1.3
    elif material == 'air':
        i = 85.7*10**-6/m_e_mev
        z = (0.78*2*7+0.21*2*8+0.01*18)/5
        a = (0.78*2*14+0.21*2*16+0.01*40)/5
        density = 1.293*10**-3
    elif material == 'scinti':
        i = 64.7*10**-6/m_e_mev
        z = (9*6+10)/19
        a = (9*12+10)/19
        density = 1.023
    t = e_e/m_e_mev
    #print(i)
    preconst = 0.307075* z/a/np.power(electron_speed(e_e),2)#2*const.pi*avogadro_number*(2.81785*10**-13)**2*m_e_mev*
    #print(preconst)
    dedx = preconst*(np.log((np.power(t,2)*(t+2))/(2*np.power(i,2)))+(((np.power(t,2))/8-(2*t+1)*np.log(2))/(t+1)**2)+1-np.power(electron_speed(e_e),2))#
    #print(electron_speed(e_e))
    #print(np.log(t**2*(t+2)/(2*i**2)))
    #print(((t**2/8-(2*t+1)*np.log(2))/(t+1)**2))
    #print(dedx)
    result = dedx#*density*thicknes
    return result

#bethe_bloch(np.array([0.1, 0.5, 1, 1.5, 2]), 'tape', 1)
#print(bethe_bloch(0.5, 'tape', 1))

#print(const.physical_constants['classical electron radius'][0])
num_counts = 10000000
energy_range = np.linspace(0.1, 2.28, 10000)
spectrum = beta.pdf(energy_range, 2.5, 10)+beta.pdf(energy_range/2.274, 3, 4)*0.4
counts = make_counts(energy_range, sr90_spectrum(energy_range), num_counts)
loss = 0.8
threshold = 0.1
plt.plot(energy_range, bethe_bloch(energy_range, 'tape', 1))
#plt.ylim(0, 10)
plt.show()

lost = []
means = []
losses = [0.5, 0.8, 1, 1.5]
colors = ['blue', 'red', 'green', 'black']
j = 0

for i in losses:
    #lost.append(energy_loss(counts, i))
    lost.append(np.random.choice(electron_travel_time(detection(energy_loss(counts, i), threshold)), 1000000, replace=True))
    means.append(np.mean(lost[j]))
    j += 1

'''
plt.plot(energy_range, sr90_spectrum(energy_range))
plt.title(r'Theoretical $\beta^-$-Spectrum of Sr90')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Probability [a.u.]')
plt.show()

plt.hist(counts, bins=100)
plt.title(r'Theoretical $\beta^-$-Spectrum of Sr90')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts')
plt.show()

plt.hist(energy_loss(counts, loss), bins=100)
plt.title(r'Expected $\beta^-$-Spectrum of Sr90 After First Layer')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts')
plt.show()

plt.plot(energy_range, dist/(const.c*electron_speed(energy_range))*10**9, label= 'Travel Time of Electron')
plt.title('Electron Travel Time')
#plt.axvline(0.3, color = 'red')
plt.ylim(0, 1)
plt.xlim(0,2.28)
plt.axhline(dist/const.c*10**9,linestyle = 'dashed', color= 'red', label = 'Minimum Travel Time')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Travel Time [ns]')
plt.legend()
plt.show()

names = []
for i in range(len(losses)):
    names.append(str(losses[i])+' MeV lost')

plt.hist(lost, bins=50, label= names, color=colors)
for i in range(len(means)):
    plt.axvline(means[i], color= colors[i], linestyle = 'dashed', label = 'Mean of '+str(losses[i]) + ' MeV lost')
#plt.vlines(means, 0, 60000, colors=[range(len(means))])#, label= losses
plt.title('Electron Travel Time')
plt.xlabel('Electron Travel Time [ns]')
plt.ylabel('Counts')
plt.legend()
#plt.ylim(0, 100)
#plt.xlim(0, 1)
plt.show()

'''

'''
plt.plot(energy_range, spectrum/max(spectrum))
plt.title('Energy Spectrum')
plt.axvline(0.3, color = 'red')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Counts [a.u.]')
plt.show()

plt.plot(energy_range, electron_speed(energy_range))
plt.title('Electron Speed')
plt.axvline(0.3, color = 'red')
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Speed [c]')
plt.show()

plt.plot(energy_range, dist/(const.c*electron_speed(energy_range))*10**9)
plt.title('Electron Travel Time')
plt.axvline(0.3, color = 'red')
plt.ylim(0, 1)
plt.xlabel('Electron Energy [MeV]')
plt.ylabel('Electron Travel Time [ns]')
plt.show()
'''