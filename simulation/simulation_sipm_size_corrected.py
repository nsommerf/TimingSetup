import numpy as np
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
y_top = [35601*1.3/25, 81960*3/25, 162326*6/25]
y_err_top = np.sqrt(y_top)
x_top = [1.69 , 9 , 36]
ax.errorbar(x_top, y_top, yerr=y_err_top, linewidth=1.0, label='top', color='blue', fmt='o', markersize=8, capsize=2)
#plt.axhline(y = np.mean(y_top), color = 'blue', linestyle = '-', label='mean')
#ax.set(xlim=(0, 30))
plt.xlabel(r'Size of SiPM [mm$^2$]')
plt.ylabel('Corrected Number of Detected Photons')
ax.legend()
#plt.savefig('simulation/plots/simulation_sipm_size_corrected_top.png')
print(y_top)
plt.show()