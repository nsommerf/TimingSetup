import numpy as np
import matplotlib.pyplot as plt
fig, ax = plt.subplots()
y_top = [35601, 81960, 162326]
y_err_top = np.sqrt(y_top)
x_top = [1.69 , 9 , 36]
ax.errorbar(x_top, y_top, yerr=y_err_top, linewidth=1.0, label='top', color='blue', fmt='o', markersize=8, capsize=2)
#plt.axhline(y = np.mean(y_top), color = 'blue', linestyle = '-', label='mean')
#ax.set(xlim=(0, 30))
plt.xlabel(r'Size of SiPM [mm$^2$]')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_sipm_size_top.png')
plt.show()