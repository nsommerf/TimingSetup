import numpy as np
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
y_side = [217511, 189461, 142499, 113406]
y_err_side = np.sqrt(y_side)
x_side = [1,1.5,2,2.5]
plt.errorbar(x_side, y_side, yerr=y_err_side, linewidth=1.0, label='side', color='red', fmt='o', markersize=8, capsize=2)
#plt.axhline(y = np.mean(y_side), color = 'red', linestyle = '-', label='mean')
plt.xlabel('Thickness of Scintillator [mm]')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_thickness_side.png')
plt.show()
fig, ax = plt.subplots()
y_top = [52793, 35601, 26829, 21555]
y_err_top = np.sqrt(y_top)
x_top = [1,1.5,2,2.5]
ax.errorbar(x_top, y_top, yerr=y_err_top, linewidth=1.0, label='top', color='blue', fmt='o', markersize=8, capsize=2)
#plt.axhline(y = np.mean(y_top), color = 'blue', linestyle = '-', label='mean')
#ax.set(xlim=(0, 30))
plt.xlabel('Thickness of Scintillator [mm]')
plt.ylabel('Number of Detected Photons')
ax.legend()
plt.savefig('simulation/plots/simulation_thickness_top.png')
plt.show()