\RequirePackage[utf8]{inputenc}
\documentclass[american]{beamer}
\usetheme{Madrid}
\usecolortheme{whale}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
\RequirePackage[backend=biber, style=numeric]{biblatex}
\RequirePackage{graphicx}  
\RequirePackage{siunitx} 
\RequirePackage{svg} 
\RequirePackage{appendixnumberbeamer}
\RequirePackage[export]{adjustbox}
%\usepackage{svg}

\newenvironment<>{varblock}[2][.9\textwidth]{%
	\setlength{\textwidth}{#1}
	\begin{actionenv}#3%
		\def\insertblocktitle{#2}%
		\par%
		\usebeamertemplate{block begin}}
	{\par%
		\usebeamertemplate{block end}%
\end{actionenv}}

\beamertemplatenavigationsymbolsempty

\title[Timing Setup]{Development of a Setup to Measure the Timing Resolution of the upcoming Mighty Tracker}
\author[Niclas Sommerfeld]{\underline{Niclas Sommerfeld}, Can-Deniz Arslan, Klaas Padeken \\ Hannah Schmitz, Sebastian Neubert}
\institute[HISKP]{Helmholtz-Institut für Strahlen- und Kernphysik \\ DPG Frühjahrstagungen 2023 Dresden, T 44.3}
\date{March 21, 2023}

\titlegraphic{\begin{columns}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=l]{grafik/lhcb-logo-0121}
			\end{figure}
		\end{column}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=c]{grafik/hiskp_logo}
			\end{figure}
		\end{column}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=r]{grafik/UNI_Bonn_Logo_Standard_RZ_Office}
			\end{figure}
	\end{column}\end{columns}}


\begin{document}
	
\frame{\titlepage}



\begin{frame}
\frametitle{Why is the Timing Resolution Important?}

\begin{figure}[htbp]
	\centering
	\includegraphics{grafik/timing_window}
\end{figure}

\begin{itemize}	
	\item<1-> \SI{40}{MHz} bunchcrossing frequency $\Rightarrow$ \SI{25}{ns} between collisions
	\item<1->[$\rightarrow$] Timing resolution of $\approx$ \SI{3}{ns} required to contain event within $\pm4\sigma$
	\item<1->[$\rightarrow$] MightyPix is expected to achieve \SI{3}{ns}
	\item<2-> So far measurements of timing resolution only possible at testbeam
	\item<2->[$\rightarrow$] Develop setup for in-lab timing measurements
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Goals of the Timing Setup}
	\begin{figure}[htbp]
		\centering
		\includegraphics[height=4cm]{grafik/timing_overview}
	\end{figure}

	\begin{block}{Design Criteria}
		\begin{itemize}
			\item<1-> Good timing resolution $\Rightarrow$ SiPMs
			\item<2-> Versatile and modular
			\item<3-> Integrate seamlessly into existing FPGA readout
			\item<4-> Low enough material budget to be used in front of sensor
			\item<4->[$\rightarrow$] Facilitates simple sensor cooling from the back
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{SiPM Readout and Amplifier Circuit}
	\begin{minipage}{0.5\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\textwidth]{grafik/timing_amp}
			
		\end{figure}
	\end{minipage}
	\begin{minipage}{0.49\linewidth}
		\visible<2->{\includegraphics[width=\linewidth]{grafik/timing_amp_oszi_arrow}}
	\end{minipage}	
\newline
%\begin{center}
%\begin{minipage}{8cm}	
\begin{block}{Features of the Amplifier Circuit}
	\begin{itemize}
		\item Filters high voltage bias $\Rightarrow$ reduce noise
		\item Capacitive coupling of signal to amplifier $\Rightarrow$ isolates high voltage
		\item Amplification close to SiPM $\Rightarrow$ improves signal to noise ratio
		\item Fast rise and fall time $\Rightarrow$ enables high rates
	\end{itemize}
\end{block}
%\end{minipage}
%\end{center}	
\end{frame}


\begin{frame}
	\frametitle{SiPM Selection and Positioning}
	\begin{minipage}{0.28\textwidth}
			\centering
			\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
			\includegraphics<1,2,3,4,5>[width=\textwidth]{grafik/all_sipms}
			\includegraphics<6>[width=\textwidth]{grafik/all_sipms_box}
	\end{minipage}
	\begin{minipage}{0.68\textwidth}
		\visible<4->{\includegraphics[width=\textwidth]{grafik/scintillator_sim_example_thicc}}
	\end{minipage}	
	\begin{minipage}{0.46\textwidth}
			\begin{block}{SiPM Parameters}
				\begin{itemize}
					\item<1-> Mounting type: SMD, THT
					\item<2-> Active area: $1.3\times1.3~$\si{mm^2},  $3\times3~$\si{mm^2}, $6\times6~$\si{mm^2}
					\item<3-> Pixel pitch: \SI{25}{\micro m}, \SI{50}{\micro m}, \SI{75}{\micro m}
				\end{itemize}
			\end{block}
	\end{minipage}\hfil
	\begin{minipage}{0.46\textwidth}
		\begin{block}<4->{Simulation}
			\begin{itemize}
				\item<4-> random starting parameters
				\item<5-> simulates propagation and reflections
				\item<6->[$\rightarrow$]$3\times3~$\si{mm^2} with \SI{50}{\micro m} pixel pitch mounted with legs
			\end{itemize}
		\end{block}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Mechanical Structure}
	\begin{minipage}{0.48\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/SIPM_vogel_amp}
		\end{figure}
	\end{minipage}
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\linewidth]{grafik/SIPM_closed_medium_amp_mod}
	\end{minipage}	
		\begin{minipage}{0.48\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/SIPM_amp_mod}
		\end{figure}
	\end{minipage}\hfil
	\begin{minipage}{0.46\textwidth}
		\begin{block}{Requirements}
			\begin{itemize}
				\item<1-> 3D printed
				\item<2-> Mounts two amplifier boards
				\item<3-> Is light tight by itself
				\item<4-> Holds scintillator
				\item<5-> Little additional material budget
			\end{itemize}
		\end{block}
	\end{minipage}		
\end{frame}




\begin{frame}
	\frametitle{How to Handle Digitization and Signal Processing?}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\textwidth]{grafik/timing_logic}
		\end{figure}
	\begin{block}{Features of the Logic Board}
		\begin{itemize}
			\item<1-> Two inputs with over voltage protection
			\item<2-> Dual comparator with adjustable thresholds and hysteresis $\Rightarrow$ digitization of signals
			\item<3-> Output selection via jumper array $\Rightarrow$ reduce noise
			\item<4-> Differential AND $\Rightarrow$ coincidence of SiPMs
			\item<5-> Logic conversion from negative ECL to positive PECL logic $\Rightarrow$ interface with FPGA readout
		\end{itemize}
	\end{block}	
\end{frame}

\begin{frame}
	\frametitle{Summary and Outlook}
	\begin{minipage}{0.32\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/SIPM_vogel_amp}
		\end{figure}
	\end{minipage}
	\begin{minipage}{0.64\textwidth}
		\includegraphics[width=\linewidth]{grafik/timing_logic}
	\end{minipage}

\begin{minipage}{0.42\textwidth}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\linewidth]{grafik/timing_amp}
	\end{figure}
\end{minipage}
\begin{minipage}{0.54\textwidth}
	\begin{block}{Goals achieved so far}
		\begin{itemize}
			\item<1-> In lab timing measurement 
			\item<2-> Versatile and modular design
			\item<3-> Low material budget
		\end{itemize}
	\end{block}	
\visible<4->{
\begin{block}{To be done}
	\begin{itemize}
		\item<4-> Testing of circuits
		\item<5-> Integration into FPGA readout
		\item<6-> Analysis of setup performance
	\end{itemize}
\end{block}}
\end{minipage}	
\end{frame}

\appendix

\begin{frame}\center
	Thank you!\\
	\vspace*{1cm}
	Questions?
\end{frame}



\begin{frame}
	\frametitle{SiPM}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\textwidth]{grafik/mppc}
		\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Logic Board}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\textwidth]{grafik/logic_board_pcbnew}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{SiPM Amplifier Circuit}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\textwidth]{grafik/sipm_amp_ufl_pcbnew}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Planned LHC Luminosity Upgrade}
	\begin{minipage}{0.65\linewidth}
		%\centering
		\visible<1->{\includegraphics[width=\textwidth]{grafik/lhcb_luminosity}
			\href{https://cds.cern.ch/record/2776420/}{\color{blue}{ LHCB-TDR-023}}}
	\end{minipage}
	\begin{minipage}{0.34\linewidth}
		\begin{block}{Implications for LHCb}
			\begin{itemize}
				\item<1-> Luminosity increased to \SI{1.5e34}{cm^{-2}s^{-1}} after LS4
				\item<1-> [$\rightarrow$] Occupancy increased beyond design parameters
				\item<1-> [$\rightarrow$] Detector upgrades required
			\end{itemize}
		\end{block}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Downstream Tracker Upgrade}
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[height=4cm]{grafik/lhcb_detector_2}
		\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
		\includegraphics[height=4cm]{grafik/mt_schematic}
		\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}
	\end{minipage}
	\hfill	
	\begin{minipage}{0.43\textwidth}
		\begin{figure}[htbp]
			\centering
			\visible<1->{\includegraphics[width=\linewidth]{grafik/mt_occupancy}
				\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}}
		\end{figure}
	\end{minipage}\hfil
	\begin{minipage}{0.51\textwidth}
		\begin{block}{Migthy Tracker}
			\begin{itemize}
				\item<1-> Combination of scintillating fibres and pixel sensors
				\item<1-> Reduces occupancy below \SI{2}{\percent}
				\item<1-> [$\rightarrow$] Ensures good tracking performance
			\end{itemize}
		\end{block}
	\end{minipage}	
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_tab1}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_tab2}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_plot1}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[width=\textwidth]{grafik/sipm_plot2}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_plot3}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\begin{figure}[ht]
		\centering
		\includegraphics[height=8cm]{grafik/scinti1}
		\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
	\end{figure}
\end{frame}

\begin{frame}
		\includegraphics[width=\textwidth]{grafik/scinti2}
		\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/scinti3}
	\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
\end{frame}


%\begin{frame}
%	\frametitle{Sources}
%	\printbibliography
%\end{frame}
\end{document}
