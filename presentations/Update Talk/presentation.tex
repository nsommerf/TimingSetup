\RequirePackage[utf8]{inputenc}
\documentclass[american]{beamer}
\usetheme{Madrid}
\usecolortheme{whale}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}
\RequirePackage[backend=biber, style=numeric]{biblatex}
\RequirePackage{graphicx}  
\RequirePackage{siunitx} 
\RequirePackage{svg} 
\RequirePackage{appendixnumberbeamer}
\RequirePackage[export]{adjustbox}
%\usepackage{svg}

\newenvironment<>{varblock}[2][.9\textwidth]{%
	\setlength{\textwidth}{#1}
	\begin{actionenv}#3%
		\def\insertblocktitle{#2}%
		\par%
		\usebeamertemplate{block begin}}
	{\par%
		\usebeamertemplate{block end}%
\end{actionenv}}

\beamertemplatenavigationsymbolsempty

\title[Timing Setup]{Design and Commissioning of a Setup to Measure the Timing Resolution of Silicon Pixel Detectors}
\author[Niclas Sommerfeld]{Niclas Sommerfeld}
\institute[HISKP]{Helmholtz-Institut für Strahlen- und Kernphysik}
\date{July 4, 2023}

\titlegraphic{\begin{columns}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=l]{grafik/lhcb-logo-0121}
			\end{figure}
		\end{column}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=c]{grafik/hiskp_logo}
			\end{figure}
		\end{column}
		\begin{column}{0.33\textwidth}
			\begin{figure}
				\includegraphics[height=1.3cm,valign=r]{grafik/UNI_Bonn_Logo_Standard_RZ_Office}
			\end{figure}
	\end{column}\end{columns}}


\begin{document}
	
\frame{\titlepage}

\begin{frame}
	\frametitle{Planned LHC Luminosity Upgrade}
	\begin{minipage}{0.65\linewidth}
		%\centering
		\visible<1->{\includegraphics[width=\textwidth]{grafik/lhcb_luminosity}
			\href{https://cds.cern.ch/record/2776420/}{\color{blue}{ LHCB-TDR-023}}}
	\end{minipage}
	\begin{minipage}{0.34\linewidth}
		\begin{block}{Implications for LHCb}
			\begin{itemize}
				\item<1-> Luminosity increased to \SI{1.5e34}{cm^{-2}s^{-1}} after LS4
				\item<1-> [$\rightarrow$] Occupancy increased beyond design parameters
				\item<1-> [$\rightarrow$] Detector upgrades required
			\end{itemize}
		\end{block}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Downstream Tracker Upgrade}
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[height=4cm]{grafik/lhcb_detector_2}
		\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
		\includegraphics[height=4cm]{grafik/mt_schematic}
		\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}
	\end{minipage}
	\hfill	
	\begin{minipage}{0.43\textwidth}
		\begin{figure}[htbp]
			\centering
			\visible<1->{\includegraphics[width=\linewidth]{grafik/mt_occupancy}
				\href{https://cds.cern.ch/record/2776420/}{\color{blue}{LHCB-TDR-023}}}
		\end{figure}
	\end{minipage}\hfil
	\begin{minipage}{0.51\textwidth}
		\begin{block}{Migthy Tracker}
			\begin{itemize}
				\item<1-> Combination of scintillating fibres and pixel sensors
				\item<1-> Reduces occupancy below \SI{2}{\percent}
				\item<1-> [$\rightarrow$] Ensures good tracking performance
			\end{itemize}
		\end{block}
	\end{minipage}	
\end{frame}

\begin{frame}
\frametitle{Why is the Timing Resolution Important?}

\begin{figure}[htbp]
	\centering
	\includegraphics{grafik/timing_window}
\end{figure}

\begin{itemize}	
	\item \SI{40}{MHz} bunchcrossing frequency $\Rightarrow$ \SI{25}{ns} between collisions
	\item[$\rightarrow$] Timing resolution of $\approx$ \SI{3}{ns} required to contain event within $\pm4\sigma$
	%\item<1->[$\rightarrow$] MightyPix is expected to achieve \SI{3}{ns}
	\item So far measurements of timing resolution only possible at testbeam
	\item[$\rightarrow$] Develop setup for in-lab timing measurements
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{General Concept of the Timing Setup}
	\begin{figure}[htbp]
		\centering
		\includegraphics[height=3.5cm]{grafik/timing_overview}
	\end{figure}

	\begin{block}{Design Criteria}
		\begin{itemize}
			\item Good timing resolution $\Rightarrow$ silicon photomultipliers (SiPM) and fast plastic scintillator
			\item Versatile and modular
			\item Integrate seamlessly into existing FPGA readout (MARS)
			\item Low enough material budget to be used in front of sensor
			\item[$\rightarrow$] Facilitates simple sensor cooling from the back
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{SiPM Selection and Positioning}
	\begin{minipage}{0.28\textwidth}
		\centering
		\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
		\includegraphics[width=\textwidth]{grafik/all_sipms_box}
	\end{minipage}
	\begin{minipage}{0.68\textwidth}
		\visible{\includegraphics[width=\textwidth]{grafik/scintillator_sim_example_thicc}}
	\end{minipage}	
	\begin{minipage}{0.46\textwidth}
		\begin{block}{SiPM Parameters}
			\begin{itemize}
				\item Mounting type: SMD, THT
				\item Active area: $1.3\times1.3~$\si{mm^2},  $3\times3~$\si{mm^2}, $6\times6~$\si{mm^2}
				\item Pixel pitch: \SI{25}{\micro m}, \SI{50}{\micro m}, \SI{75}{\micro m}
			\end{itemize}
		\end{block}
	\end{minipage}\hfil
	\begin{minipage}{0.46\textwidth}
		\begin{block}{Simulation}
			\begin{itemize}
				\item Random starting parameters
				\item Simulates propagation and reflections
				\item[$\rightarrow$]$3\times3~$\si{mm^2} with \SI{50}{\micro m} pixel pitch mounted with legs
			\end{itemize}
		\end{block}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{SiPM Readout and Amplifier Circuit}
	\begin{minipage}{0.5\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\textwidth]{grafik/timing_amp}
			
		\end{figure}
	\end{minipage}
	\begin{minipage}{0.49\linewidth}
		\includegraphics[width=\linewidth]{grafik/amp_pcb_design}
		\includegraphics[width=\linewidth]{grafik/amp_pcb_noplug}
	\end{minipage}	
\newline
%\begin{center}
%\begin{minipage}{8cm}	
\begin{block}{Features of the Amplifier Circuit}
	\begin{itemize}
		\item Filters high voltage bias $\Rightarrow$ reduce noise
		\item Capacitive coupling of signal to amplifier $\Rightarrow$ isolates high voltage
		\item Amplification close to SiPM $\Rightarrow$ improves signal to noise ratio
		\item Fast rise and fall time $\Rightarrow$ enables high rates
		\item Positions SiPM
	\end{itemize}
\end{block}
%\end{minipage}
%\end{center}	
\end{frame}

\begin{frame}
	\frametitle{Amplifier PCB and Amplified SiPM Signal}
	\begin{minipage}{0.48\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/amp_pcb_plugs}
			\includegraphics[height=\linewidth, angle=90]{grafik/amp_pcb_backside}
			
			
		\end{figure}
	\end{minipage}
\hfil
	\begin{minipage}{0.48\linewidth}
		\includegraphics[width=\textwidth]{grafik/timing_amp_oszi_arrow}
	\end{minipage}	
	\newline
	%\begin{center}
	%\begin{minipage}{8cm}	
	\vspace*{0.5cm}
	\begin{block}{Implications of the Amplifier Circuit}
		\begin{itemize}
			\item Large ratio of cable to PCB $\Rightarrow$ need external strain relief
			\item SiPM obstructed by PCB $\Rightarrow$ mount SiPM first
			\item Slow response time relative to desired time resolution $\Rightarrow$ could be bottleneck
		\end{itemize}
	\end{block}
	%\end{minipage}
	%\end{center}	
\end{frame}


\begin{frame}
	\frametitle{How to Handle Digitization and Signal Processing?}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\textwidth]{grafik/timing_logic}
	\end{figure}
	\begin{block}{Features of the Logic Board}
		\begin{itemize}
			\item Two inputs with over voltage protection
			\item Dual comparator with adjustable thresholds and hysteresis $\Rightarrow$ digitization of signals
			\item Output selection via jumper array $\Rightarrow$ reduce noise
			\item Differential AND $\Rightarrow$ coincidence of SiPMs
			\item Logic conversion from PECL to LVDS $\Rightarrow$ interface with FPGA readout
		\end{itemize}
	\end{block}	
\end{frame}

\begin{frame}
	\frametitle{Logic Board}
	\begin{minipage}{0.48\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[height=3.5cm]{grafik/logic_pcb_design}
		\end{figure}
	\end{minipage}
\hfil
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=3.5cm, angle=90]{grafik/logic_pcb}
	\end{minipage}	
	\begin{minipage}{0.58\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[height=4cm]{grafik/logic_osci}
		\end{figure}
		\end{minipage}
\hfil
	\begin{minipage}{0.38\textwidth}
		\begin{block}{Signal Characteristics}
			\begin{itemize}
				\item Little Noise $\Rightarrow$ may be a reflection 
				\item Rise/fall time $\approx$\SI{0.4}{ns}
				\item \SI{1.2}{V} logic low and \SI{1.6}{V} logic high
				\item[$\Rightarrow$] may work with FPGA
			\end{itemize}
		\end{block}
	\end{minipage}
		
\end{frame}


\begin{frame}
	\frametitle{Mechanical Structure}
	\begin{minipage}{0.48\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[height=3.5cm]{grafik/mechanic_bare_scinti}
		\end{figure}
	\end{minipage}
\hfil
	\begin{minipage}{0.48\textwidth}
		\includegraphics[height=3.5cm]{grafik/mechanic_lid}
	\end{minipage}	
		\begin{minipage}{0.48\textwidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[height=4cm]{grafik/mechanic_lidless}
		\end{figure}
	\end{minipage}\hfil
	\begin{minipage}{0.48\textwidth}
		\begin{block}{Requirements}
			\begin{itemize}
				\item 3D printed
				\item Mounts two amplifier boards
				\item Is light tight by itself
				\item Holds scintillator
				\item Little additional material budget
			\end{itemize}
		\end{block}
	\end{minipage}		
\end{frame}



\begin{frame}
	\frametitle{Setup to Measure Timing Resolution}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=0.9\textwidth]{grafik/timing_resolution_measurement}
	\end{figure}
	
		\begin{itemize}
			\item Source above two timing layers
			\item Trigger on coincidence signal of lower layer
			\item Automated data taking with PC and Oscilloscope
		\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Performance of Logic Board}
\begin{minipage}{0.48\linewidth}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\linewidth]{grafik/timing_resolution_sketch_only_logic}	
	\end{figure}
\end{minipage}
\hfil
\begin{minipage}{0.48\linewidth}
	\includegraphics[width=\textwidth]{grafik/timing_resolution_only_logic}
\end{minipage}	
\newline
\begin{block}{Observations}
	\begin{itemize}
		\item Double peak $\Rightarrow$ may be related to sampling rate / artifact of quantized measurement or signal of function generator
		\item Timing resolution on the order of \SI{0.1}{ns} $\Rightarrow$ logic board should not bottleneck performance
	\end{itemize}
\end{block}
\end{frame}


\begin{frame}
	\frametitle{Shape of Timing Resolution}
	\begin{minipage}{0.48\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/2d_histo10kMeasurements_top}	
		\end{figure}
	\end{minipage}
	\hfil
	\begin{minipage}{0.48\linewidth}
		\includegraphics[width=\textwidth]{grafik/2d_histo10kMeasurements_bottom}
	\end{minipage}	
	\newline
	\begin{block}{Observations}
		\begin{itemize}
			\item Curvature of distribution $\Rightarrow$ follows expectation for time walk
			\item Lower ToT reached in bottom layer $\Rightarrow$ particles stopped in layer
			\item Difference in maximum ToT $\Rightarrow$ partly explained by different response of timing layers to deposited energy
		\end{itemize}
	\end{block}	
\end{frame}

\begin{frame}
	\frametitle{Determining a Single Value for the Time Resolution}
	\begin{minipage}{0.48\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/single_gauss_10kMeasurements}	
		\end{figure}
	\end{minipage}
	\hfil
	\begin{minipage}{0.48\linewidth}
		\includegraphics[width=\textwidth]{grafik/sum_gauss10kMeasurements}
	\end{minipage}	
	\newline
	\begin{block}{Considerations}
		\begin{itemize}
			\item Not a simple Gaussian
			\item Empirical fit function needed $\Rightarrow$ sum of two Gaussians describes data
			\item Timing resolution can be expressed as FWHM of distribution
			\item Timing resolution of a single layer: $\frac{1}{\sqrt{2}}\mathrm{FWHM}$
		\end{itemize}
	\end{block}	
\end{frame}

\begin{frame}
	\frametitle{Ongoing Work}
	\begin{minipage}{0.48\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/logic_threshold_overview}	
		\end{figure}
	\end{minipage}
	\hfil
	\begin{minipage}{0.48\linewidth}
		\includegraphics[width=0.9\textwidth]{grafik/sum_gauss5871VSiPMBias}
	\end{minipage}	
	\newline
	\begin{block}{Parameters to Optimize}
		\begin{itemize}
			\item Supply voltage of Amplifier $\Rightarrow$ balancing heating and fall time of signal
			\item SiPM bias $\Rightarrow$ trade off between gain and noise
			\item Threshold $\Rightarrow$ reducing time walk while avoiding noise floor
			\item Best so far: $\mathrm{FWHM} =$ \SI{1.074}{ns} $\hat{=} ~ \sigma_{single} =$ \SI{0.759}{ns}
		\end{itemize}
	\end{block}	
\end{frame}

\begin{frame}
	\frametitle{Future Topics to Explore}

\begin{minipage}{0.3\linewidth}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\linewidth]{grafik/mechanic_lidless}
	\end{figure}
\end{minipage}
\hfil
\begin{minipage}{0.3\linewidth}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\linewidth]{grafik/logic_pcb_cables}
	\end{figure}
\end{minipage}
\hfil
\begin{minipage}{0.3\linewidth}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\linewidth]{grafik/2d_histo10kMeasurements_bottom}
	\end{figure}
\end{minipage}


\begin{minipage}{0.46\linewidth}
	\begin{block}{Hardware Improvements}
		\begin{itemize}
			\item FPGA integration
			\item Streamline powering
			\item Increase active area
			\item Design faster amplifier
			\item Use faster scintillator
			\item Time walk correction
		\end{itemize}
	\end{block}

\end{minipage}
\hfil
\begin{minipage}{0.46\linewidth}
\begin{block}{Additional Measurements}
	\begin{itemize}
		\item Performance at different temperatures
		\item Noise immunity
		\item Testbeam
		\item Radiation hardness
	\end{itemize}
\end{block}
\end{minipage}

\end{frame}

\appendix

\begin{frame}\center
	Thank you!\\
	\vspace*{1cm}
	Questions?
\end{frame}



\begin{frame}
	\frametitle{SiPM}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\textwidth]{grafik/mppc}
		\end{figure}
\end{frame}



\begin{frame}
	\frametitle{SiPM Amplifier Circuit Schematic}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\textwidth]{grafik/amp_schematic}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Logic Board Schematic}
	\begin{figure}[htbp]
		\centering
		\includegraphics[width=\textwidth]{grafik/logic_schematic}
	\end{figure}
\end{frame}





\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_tab1}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_tab2}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_plot1}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[width=\textwidth]{grafik/sipm_plot2}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/sipm_plot3}
	\href{https://www.hamamatsu.com/content/dam/hamamatsu-photonics/sites/documents/99_SALES_LIBRARY/ssd/s13360_series_kapd1052e.pdf}{\color{blue}{Hamamatsu MPPC}}
\end{frame}

\begin{frame}
	\begin{figure}[ht]
		\centering
		\includegraphics[height=8cm]{grafik/scinti1}
		\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
	\end{figure}
\end{frame}

\begin{frame}
		\includegraphics[width=\textwidth]{grafik/scinti2}
		\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
\end{frame}

\begin{frame}
	\centering
	\includegraphics[height=8cm]{grafik/scinti3}
	\href{https://www.crystals.saint-gobain.com/radiation-detection-scintillators/plastic-scintillators/bc400-bc404-bc408-bc412-bc416}{\color{blue}{Saint Gobain}}
\end{frame}

\begin{frame}
	\frametitle{Further Uses of 2D Histogram}
	\begin{minipage}{0.48\linewidth}
		\begin{figure}[htbp]
			\centering
			\includegraphics[width=\linewidth]{grafik/2d_histo5VAmplifier_bottom}	
		\end{figure}
	\end{minipage}
	\hfil
	\begin{minipage}{0.48\linewidth}
		\includegraphics[width=\textwidth]{grafik/2d_histo9VAmplifier_bottom}
	\end{minipage}	
	\newline
	\begin{block}{Larger Supply Voltage of Amplifier}
		\begin{itemize}
			\item steeper and narrower distribution $\Rightarrow$ better timing resolution
		\end{itemize}
	\end{block}	
\end{frame}

%\begin{frame}
%	\frametitle{Sources}
%	\printbibliography
%\end{frame}
\end{document}
