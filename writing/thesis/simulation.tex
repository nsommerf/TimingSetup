\section{Deciding on a Configuration of SiPM and Scintillator}

In order to quickly evaluate the influence of different configurations of scintillator and SiPM combinations a raytracing algorithm was implemented. It is able compare sizes of SiPMs and scintillators, as well as placements of the SiPMs on the scintillators. The simulation works in two dimensions and only considers total internal reflection at the edges of the scintillator. 

\subsection{Scintillator}

The scintillator used is BC-408, which is a plastic scintillator intended for general purpose applications. The influence of the shape and size on the collection efficiency of photons will be investigated in the following sections. It has to be noted that the thickness of the scintillator is of special importance, since it dictates the amount of deposited energy for a given particle passing through it. While a thicker scintillator produces a larger signal it also means that a passing particle loses to much energy an gets either stuck in the scintillator or cannot induce a detectable signal in the chip behind it.

\subsection{Available SiPMs}

The range of SiPMs considered for the following simulation was narrowed down to SiPMs manufactured by Hamamatsu due to their good reputation and availability compared to SiPMs produced by other manufacturers. Narrowing it down further Hamamatsu's S13360 series was selected due to the wavelength where their photon detection efficiency peaks, being the closest match to the wavelength where the emission of the scintillator is maximum. The SiPMs are offered with three different sizes of active area: \qtyproduct{1.3 x 1.3}{mm}, \qtyproduct{3 x 3}{mm} and \qtyproduct{6 x 6}{mm}. All sizes are available as a surface mounted device, while the two larger sizes are also offered with through hole mounting. The through hole mounted SiPMs are always offered in a ceramic package. Every variant is available with three different pixel pitches: \SI{25}{\micro m}, \SI{50}{\micro m} and \SI{75}{\micro m}. The only factor influencing the cost of the SiPM is the size of the active area, where a larger area is more expensive.

\subsection{How the Raytracing Algorithm Works}

The scintillator is modeled as two dimensional rectangle. Each light ray is initialized with a random starting position in the X-Y plane and an angle. The starting parameters are kept within the right most \SI{20}{mm} of the rectangle. Using the starting position and angle a second position is computed which the defines the light ray. At this point a check is performed whether the ray hits a SiPM placed either on the small side of the scintillator or on the top. If one of the positions detects a photon, a flag is set to avoid scoring multiple hits at a position for a single ray. As a next step the intersection of the ray with the scintillator border is determined and a hit-flag is set to the corresponding border This hit-flag is used in the next intersection test to keep from determining the same border as being hit twice in a row. Now the angle at which the ray intersects the schintillators border is calculated and based on that angle the further propagation of the ray is decided on. If the angle is above the angle required for total internal reflection the further simulation of this particular ray is stopped and the number of escaped photons is incremented by one. If however the angle is below the maximum angle where total internal reflection occurs the new angle of the ray is determined using the initial angle and the border hit by the ray. The angle of total internal reflection is determined according to the following formula:

\begin{equation}\label{eq:tir}
	\theta_c = \arcsin\left(\frac{n_{air}}{n_{s}}\right) = \arcsin\left(\frac{1}{1.58}\right) = 39.27^{\circ}
\end{equation}

\noindent Where $\theta_c$ is the critical angle below which total internal reflection occurs, $n_{air}$ is the refractive index of air and $n_s$ is the refractive index of the used scintillator. At this point it is checked if the current number of bounces exceeds a maximum number of allowed bounces, if so a corresponing counter is incremented and the simulation of this photon is stopped. After that the next instance of the loop starts again checking for intersections with the borders of the scintillator. An example ray is shown in \ref{fig:simulation_example}.

\begin{figure}[htbp]\centering
	\includegraphics[width=\textwidth]{grafik/scintillator_sim_example}
	\caption{Simulation example showing the propagation and detection of a single ray. The photon (red) is randomly initialized inside the allowed starting area (translucent green). The borders of the scintillator with dimensions \qtyproduct{1.5 x 35}{mm} is shown in black. The areas covered by two possible SiPM options (\qtyproduct{1.3 x 1.3}{mm} on the side, \qtyproduct{3 x 3}{mm} on top) are shown in blue. In this example the SiPM on the side is hit after 9 bounces under an angle that would not facilitate any further reflections. It should be noted that the X- and Y-axis are scaled differently for clarity.}
	\label{fig:simulation_example}
\end{figure}

\subsection{Testing the Influence of Different Parameters on Collection Efficiency}

Several studies were performed to test influence of different parameters of the simulation on the collection efficiency. Tested parameters are different sizes of available SiPMs as well as different lengths and thicknesses of scintillator.

\subsubsection*{Consistency of Testing}

To asses the uncertainty of the simulation 9 runs with the same parameters were performed. The parameters used are \qtyproduct{1.3 x 1.3}{mm} SiPMs on both the side and top with a \SI{1}{mm} thick and \SI{35}{mm} long scintillator. Each run simulates \si{10^6} produced photons. The results of these test are shown in \ref{fig:simulation_consistency}. This shows that the individual numbers of detected photons are compatible with the mean value within two $\sigma$, while most are compatible within one $\sigma$. This shows that it is sufficient to simulate \si{10^6} photons each run.

\subsubsection*{Length of Scintillator}

The length of the scintillator varying from \SI{25}{mm} to \SI{50}{mm} in \SI{5}{mm} increments was simulated. The results of this variation are shown in \ref{fig:simulation_length}. The results for different lengths of scintillator are compatible with the mean of each distribution within one $\sigma$. This indicates that the length of the scintillator does not play a major role in the detection efficiency. This is to be expected since the number of detected without any bounces is small compared to the number of detected photons with at least one bounce. Furthermore due to the rectangular shape of the scintillator photons emitted under an angle suited to total internal reflection at least propagate in the scintillator until they hit an edge orthogonal to the first one they hit. This makes it likely for a photon to hit the SiPM, in a scintillator which is thin relative to its length. In practice this means that the additional length of the scintillator required to keep the SiPM and amplifier board from covering parts of the MightyPix in case of mounting the SiPm on top of the scintillator does not pose an issue.

\subsubsection*{Thickness of Scintillator}

Different thicknesses of scintillator ranging from \SI{1}{mm} to \SI{2.5}{mm} in steps of \SI{0.5}{mm} were simulated. The results are shown in \ref{fig:simulation_thickness}. This shows an increased efficiency of detection with decreasing thickness of scintillator. An explanation for that is the increased relative coverage of the SiPM. For the edge mounted case the relation is trivial. For top mounted case the distance of two neighboring hits on the same border for a given emission angle of a photon scales linearly with thickness of the scintillator. However this effect is counteracted by the number of photons generated by a particle passing the scintillator, which increases with the thickness of the scintillator.

\subsubsection*{Size of SiPM}

The different available sizes of SiPMs were simulated. This is only done for the mounting position on top of the scintillator, since the SiPMs larger than \qtyproduct{1.3 x 1.3}{mm} mounted on the side are not fully covered by the scintillator. This does not only waste sensitive area of the SiPM but also makes achieving light tightness harder. The results of simulations with different SiPM sizes is shown in \ref{fig:simulation_sipm_size}. The number of detected photons increases with increasing size of the SiPM. This effect is further compounded by the fact that the simulation only considers the size increase of the SiPM in one dimension, this means that to obtain a closer approximation of the actual number of photons detected by a given size of SiPM, one has to correct for the width of the SiPM as well. As a first order approximation one can use the ratio of width of the SiPM to width of the scintillator, which is assumed to be \SI{25}{mm}. This results in \ref{fig:simulation_sipm_size_corr}.



\begin{figure}[htbp]\centering
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_consistency_top}
	\end{minipage}
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_consistency_side}
	\end{minipage}	
	\caption{Results of 9 Simulation runs using the same parameters. Each run simulates \si{10^6} produced photons. The left plot in blue shows the number of photons reaching the SiPM mounted on top of the scintillator, while the right plot in red shows the number of photons reaching the SiPM mounted on the edge of the scintillator. Both SiPMs have an active area of \qtyproduct{1.3 x 1.3}{mm}. The scintillatior is  \SI{1}{mm} thick and \SI{35}{mm} long. The mean value for each distribution is shown. The error bars are determined as the square root of photons detected, which correspond to one $\sigma$.}
	\label{fig:simulation_consistency}
\end{figure}

\begin{figure}[htbp]\centering
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_length_top}
	\end{minipage}
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_length_side}
	\end{minipage}	
	\caption{Results of different lengths of scintillator. Each run simulates \si{10^6} produced photons. The left plot in blue shows the number of photons reaching the SiPM mounted on top of the scintillator, while the right plot in red shows the number of photons reaching the SiPM mounted on the edge of the scintillator. Both SiPMs have an active area of \qtyproduct{1.3 x 1.3}{mm}. The scintillator is  \SI{1.5}{mm} thick. The mean value for each distribution is shown. The error bars are determined as the square root of photons detected, which correspond to one $\sigma$.}
	\label{fig:simulation_length}
\end{figure}

\begin{figure}[htbp]\centering
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_thickness_top}
	\end{minipage}
	\begin{minipage}{0.48\textwidth}
		\includegraphics[width=\textwidth]{grafik/simulation_thickness_side}
	\end{minipage}	
	\caption{Results of different thicknesses of scintillator. Each run simulates \si{10^6} produced photons. The left plot in blue shows the number of photons reaching the SiPM mounted on top of the scintillator, while the right plot in red shows the number of photons reaching the SiPM mounted on the edge of the scintillator. Both SiPMs have an active area of \qtyproduct{1.3 x 1.3}{mm}. The scintillator is  \SI{1.5}{mm} thick and  \SI{35}{mm} long. The error bars are determined as the square root of photons detected, which correspond to one $\sigma$.}
	\label{fig:simulation_thickness}
\end{figure}

\begin{figure}[htbp]\centering
		\includegraphics[width=0.8\textwidth]{grafik/simulation_sipm_size_top}
	\caption{Results of different sizes of SiPM. Each run simulates \si{10^6} produced photons. The plot in shows the number of photons reaching the SiPM mounted on top of the scintillator. The scintillator is  \SI{35}{mm} long. The error bars are determined as the square root of photons detected, which correspond to one $\sigma$.}
	\label{fig:simulation_sipm_size}
\end{figure}

\begin{figure}[htbp]\centering
	\includegraphics[width=0.8\textwidth]{grafik/simulation_sipm_size_corrected_top}
	\caption{Results of different sizes of SiPM corrected for their widths. Each run simulates \si{10^6} produced photons. The plot in shows the number of photons reaching the SiPM mounted on top of the scintillator. The scintillator is  \SI{35}{mm} long. The error bars are determined as the square root of photons detected, which correspond to one $\sigma$.}
	\label{fig:simulation_sipm_size_corr}
\end{figure}

\subsection{Choice of SiPM and Scintillator}

With the knowledge about how the parameters influence the efficiency gained by the previous simulations, three final setups were simulated. All scenarios use a \SI{1.5}{mm} thick scintillator in order to keep the energy loss of particles traveling through the scintillator low. The first simulation tests a SiPM with an active area of \qtyproduct{1.3 x 1.3}{mm} mounted on the side of an \SI{25}{mm} long scintillator. The other two simulations test SiPMs with active areas of \qtyproduct{3 x 3}{mm} and \qtyproduct{6 x 6}{mm} mounted on top of an \SI{35}{mm} long scintillator, to compensate for some of the scintillator area being taken up by the SiPM and amplifier board. When correcting for the width of the SiPM relative to the width of the scintillator one gets the following results. The first setup with the smallest SiPM mounted on the side 9842 photons reach the SiPM, while for the medium sized SiPM mounted on top 9835 photons reach the SiPM and for the largest SiPM mounted on top 38985 photons reach the SiPM. This shows that a small SiPM mounted on the side performs similar to the medium sized SiPM mounted on top, while the large SiPM mounted on top detects roughly four times as many photons. Considering that the small and medium sized SiPMs cost almost the same amount and the largest SiPM only costs double that for a four times increase in efficiency, the largest SiPM offers the most captured photons per cost. However first laboratory tests with an old generation of SiPM similar in size to the small SiPM available showed a sufficiently large signal mounted on the side, thus the largest SiPM was disregarded. Finally the SiPM with an active area of \qtyproduct{3 x 3}{mm} mounted on top was chosen, because it is available with through hole mounting for ease of assembly. The pixel pitch was on one hand decided by the photon detection efficiency which is higher for larger pixels and on the other hand by the delivery times, which for the \SI{50}{\micro m} pixel pitch was 1.5 months while for the \SI{75}{\micro m} pixel pitch was 4 months. Since the photon detection efficiency for the \SI{50}{\micro m} pixels is only slightly worse than for the \SI{75}{\micro m} pixel pitch and the delivery time of 4 months would hinder the time frame of this thesis it was decided to order the SiPMs with \SI{50}{\micro m} pixel pitch, an active are of \qtyproduct{3 x 3}{mm}, with through hole mounting and mount them on top of the scintillator.

