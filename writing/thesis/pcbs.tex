\section{Custom Printed Circuit Boards}

This section details the design of the custom made printed circuit boards (PCBs) used to build the timing setup.

\subsection{Amplifier Board}

The design of the amplifier circuit is based on an existing design of which several populated PCBs and a schematic were available. A new PCB was designed based on the existing schematic and populated PCBs. It features a lowpass filter to reduce high frequency noise from the high voltage supply being introduced into the signal. It also capacitively couples the SiPM signal into the amplifier circuit to isolate it from the high voltage used to bias the SiPM. A simplified sketch of the amplifier board is shown in \ref{fig:timing_amp}, while the full circuit and PCB design are shown in \ref{fig:timing_amp_circuit} and \ref{fig:timing_amp_pcb}.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.6\textwidth]{grafik/timing_amp}
	\caption{Sketch of the amplifier board}
	\label{fig:timing_amp}
\end{figure}

\begin{figure}[htbp]\centering
	\includegraphics[width=\linewidth]{grafik/timing_amp_circuit}
	\caption{Circuit design of the amplifier board}
	\label{fig:timing_amp_circuit}
\end{figure}

\noindent The design is executed on a two layer PCB. Where the bottom layer is used as a ground plane. All components on the PCB where chosen as surface mount devices (SMD), because of their more compact form factor and better high frequency characteristics. SMD resistors and capacitors in the size 0402 are chosen where possible. This is not possible for the resistors on the high voltage path. They need to be at least of the size 0805 to achieve a maximum voltage rating above the bias voltage, required to operate the chosen SiPMs. U.FL connectors where chosen for the signal and supply voltage connections to the amplifier board. This is done because of their compact form factor and high frequency capabilities. Their maximum voltage rating is \SI{60}{V}. The low number of rated cycles of plugging and unplugging can be mitigated by using the SMA end of the U.FL to SMA cables used to connect the amplifier boards to the logic board to disconnect both boards.

A surface mounted right angle SMA connector was chosen to carry the high voltage to the amplifier board. It was decided against U.FL here because the operation voltage of the chosen SiPMs reaches up to \SI{60}{V}, meaning they may exceed the maximum voltage rating of the connectors. Surface mounting the plug does not expose high voltage terminals on the backside of the PCB or protrude from the PCB, making it safer to operate and more compact. The right angled connection was chosen to keep the PCB low profile and cables exiting the envelope of the PCB on one side.

Two plated through holes are placed on either end of the PCB, which are used to mount the PCB to the mechanical structure using M3X6 screws. The SiPM is positioned centered between the mounting holes and attached by soldering.

\subsection{Earlier Iterations of the Amplifier Board}

The first redesigned iteration of the amplifier board separated the ground domains of the high and low voltage parts of the amplifier. The reasoning behind this was that noise from the high voltage part has less impact on the amplifier part. However this caused shifting of the grounds of the high and low voltage side relative to each other, resulting in enough noise to render the amplifier useless. Thus the following designs returned to a common ground.

The circuit design on which the amplifier board is based, uses high frequency NPN and PNP transistors with frequency ratings of \SI{3}{GHz} and \SI{5}{GHz} which are no longer in production. A suitable substitute for the NPN transistor was found with a frequency rating of \SI{5}{GHz}. However the fastest PNP transistor available only has a frequency rating of \SI{600}{MHz}. To asses the impact of the lower frequency rating on the performance of the amplifier board two tests were conducted. First a \SI{5}{GHz} transistor was harvested of one of the existing amplifier boards and soldered onto a PCB produced with the new design. As the second test one of the slower and available transistors was soldered onto a PCB with the base design. Both tests showed a negligible difference in performance when it comes to the average rise time of a SiPM signal amplified by the amplifier boards. This means that the available \SI{600}{MHz} PNP transistor can be used as a substitute.

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\linewidth]{grafik/timing_amp_pcb}
		\caption{}
		\label{fig:timing_amp_pcb}
		
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[height=\linewidth, angle= 90]{grafik/amp_pcb_backside}
		\caption{}
		\label{fig:timing_amp_backside}
	\end{subfigure}
	
	\vspace{1cm}
	
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\linewidth]{grafik/amp_pcb_noplug}
		\caption{}
		\label{fig:timing_amp_noplug}
	\end{subfigure}
	\hfill
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\linewidth]{grafik/amp_pcb_plugs}
		\caption{}
		\label{fig:timing_amp_plugs}
	\end{subfigure}
	
	\caption{Design of Amplifier PCB (a), backside of amplifier PCB with SiPM temporarily placed (b), front side od amplifier PCB without plugs (c) and amplifier PCB with plugs and cables attached (d).}
	\label{fig:timing amp}
\end{figure}

\subsection{Logic Board}

The logic board is based on an existing design and manufactured as a four layer PCB. The top and bottom layers carry the signals, the layer below the top is used as a ground plane and the last layer is used as a power plane or as a ground plane where the backside carries a signal. The PCB has two inputs, one for each of the two amplifier boards present in a single timing layer. The inputs are connected to an integrated circuit (IC) which provides over voltage protection. The inputs are also connected to an IC which serves as a dual comparator. The dual comparator consists of two separate comparators in a single package. Each of the comparators compares its input signal to a threshold voltage set externally. When the signal voltage crosses the external threshold the differential output signal of the comparator changes its logic state. The threshold as well as the hysteresis of the output signal can be set for each comparator individually. After the dual comparator the signals are sent into the jumper array which can be used to send each of the comparator outputs directly to the logic conversion at the end of the logic board or the signals can be sent to the AND gate. The individual signals directed to the AND gate can also be inverted. The AND gate is used to build the coincidence of both comparator outputs. The last stage of the logic board is logic conversion which translates the logic levels used on the logic board to levels which MARS can understand. Sketch of the logic board is shown in \ref{fig:timing_logic}.

\begin{figure}[htbp]\centering
	\includegraphics[width=\textwidth]{grafik/timing_logic}
	\caption{Sketch of the logic board. Analog single ended inputs shown in red and differential digital signals shown in pairs of the same color with different gray levels.}
	\label{fig:timing_logic}
\end{figure}

\noindent The voltages required to generate the thresholds ranging from \SI{-2.5}{V} to \SI{+2.5}{V} are generated using a voltage reference and an operational amplifier. The thresholds themselves are the derived from that by connecting the voltages to either end of a trim potentiometer and using the center contact as the threshold connected to the negative input of the comparators. It was decided to use solder jumpers instead of regular jumpers with headers, because they act much less like an antenna picking up noise. 

The differential output of the comparators is high when its positive input is more positive than its negative input. The signal input of the logic board is connected to the positive input, while the threshold voltage is connected to the negative input. This means that when using the AND to build the coincidence of the inputs one must invert both comparator outputs when detecting negative signals. Otherwise for no signal the threshold is more negative than the input, resulting in the no signal state of the comparator being high and when a signal is detected dropping to low. Thus if not inverted the AND would be high for no signal and low if at least one of the SiPMs detects a signal.  
If the comparator outputs are routed directly to the individual outputs they can be inverted swapping the cables of the differential signal. Swapping the cables does not work for the AND output.

\subsection{First Production Run of Logic Boards}

The first design iteration omitted pull down resistors at the signal inputs of the logic board, which lead to a measurable voltage on the input of the logic board induced by the comparator. This voltage correlated with the set threshold, which meant that for certain ranges of threshold the input was constantly above threshold. Adding the pull down resistors solved this issue. 

Furthermore it used a version of the comparators that outputs ECL. By extension this meant that the AND was operated in ECL mode. However MARS can only interpret positive logic types, thus a logic converter which translates ECL to PECL was implemented. This logic converter appears to reach an operating temperature on the order of \SI{70}{\degreeCelsius}, additionally it turned out that the voltage levels of PECL are to high for MARS to be able to correctly interpret them. This lead to a major redesign of the logic board. The newest design switched to using a version of the comparator that outputs PECL and operates the AND in PECL mode. This removes the need for a dedicated logic converter and allows to use a simple resistor network to adjust the output voltage to an appropriate level.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.8\textwidth]{grafik/logic_pcb_design}
	\caption{PCB design of logic board}
	\label{fig:timing_logic_pcb_design}
\end{figure}

\begin{figure}[htbp]\centering
	\includegraphics[width=0.8\textwidth]{grafik/logic_pcb_cables}
	\caption{Logic board with cables attached}
	\label{fig:timing_logic_cables}
\end{figure}
