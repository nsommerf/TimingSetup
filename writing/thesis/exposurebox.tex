\section{Testing Different Scintillator Wrapping Methods}

A SiPM is a device capable of detecting a single photon, this means that each photon reaching the SiPM can produce a signal. Thus any non signal photon should be kept from reaching the SiPM to reduce the background. This can be done by wrapping both the SiPM and scintillator light tight. Furthermore a reflective wrapping around the scintillator increases the amount of light reaching the SiPM by reflecting light that would escape the scintillator back into it. This section explores the practicality and relative performance of wrapping either with reflective foil and black vinyl tape or PTFE tape and black vinyl tape.

\subsection{Observations about Wrapping}

One method of wrapping is to use PTFE tape as a reflective layer, which is usually used to seal threads in plumbing, the idea and execution are taken from~\cite{onsemi:wrapping}. The idea is that the white color of the tape increases the amount of light collected by the SiPM due to its reflectivity. The PTFE tape is wrapped around the scintillator in such a way that every point is covered by atleast three layers of tape. The PTFE tape does only adhere slightly to itself and not to the SiPM or scintillator. To permanently affix the PTFE tape black vinyl tape is used. This outer wrap may also improve the light tightness of the wrap. Wrapping the scintillator with multiple layers of tape significantly increases the size of the assembly. Furthermore wrapping the tape around the  corners and edges of the scintillator proves to be difficult to do in a neat way. This also leads to the wrap being thicker around the corners.

Another method of wrapping is to use a thin reflective foil and keep it in place by encasing it in a layer of black vinyl tape. The foil used here is a thin plastic foil with a thin layer of metal deposited on each side of the plastic. The foil wrapped around the scintillator like for a gift. The resulting wrap is thinner than the PTFE tape wrap. The thickness of the wrap is very similar in all areas of the scintillator compared to the PTFE wrap.

In summary wrapping with foil is simpler and yields a thinner wrap with a  more consistent thickness.

\subsection{Testing of Light Tightness}

In order to test the light tightness of a wrap the scintillator needs to be exposed to a controlled amount of light. This can be done by placing the scintillator and SiPM assembly in a light tight box and illuminating them with LEDs. To measure the amount of light reaching the inside of the scintillator one can determine the count rate of the SiPM with an oscilloscope. 

However for this to be a valid comparison criterion one must ensure equal operating conditions of the SiPM for both wrapping methods. The count rate will have three contributions the first one is stray light from the outside making its way into the box and assembly inside, this can be mitigated by improving the light tightness of the box as well as having its relative impact reduced by increasing the brightness of LEDs used to illuminate the assembly. The second source of counts is dark counts of the SiPM, they can be kept the same for multiple tests by performing the tests with the same SiPM and operating it at the same overvoltage. Furthermore by choosing a threshold larger than most dark count events can reach, the amount of dark counts contributing to the measurement can be greatly reduced. The third source of counts is radiation hitting the scintillator in the form of either from cosmic particles or radioactive decays of surrounding materials, however on average this contribution stays constant and should be small relative to the other contributions.

The 3d printed box used to test the scintillator and SiPM assembly is shown in \ref{fig:exposurebox_assembly}. It features a lip around the edge of the bottom part that interlocks with the lid cutting of any straight path for light from the outside to enter between top and bottom part. On the right is a channel for wires to pass through which is sealed up by stuffing it with foam. A total of seven leds are placed inside the box each centered on one side of the scintillator, except for the side where the channel and SiPM are, where two leds are placed on either side of the channel. Each of the leds is connected to the ground and a IO pin of a arduino with an ESP8266 microcontroller in order to be able to turn them on using a python program. Pulse width modulation (PWM) is used to adjust the output voltage to a level appropriate for the LEDs. The scintillator and SiPM assembly is positioned in the center and elevated from the bottom by two ribs which are sized to the same dimensions as the scintillator and positioned to center the the assembly in the box. The SiPM is connected to the amplifier via a long coaxial cable. An oscilloscope set to a fixed trigger value of \SI{100}{mV} is used to detect and count the output pulses of the amplifier. It counts the total number of peaks above the trigger threshold.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.6\textwidth, angle = 90]{grafik/exposurebox_assembly}
	\caption{Box for illuminating scintillator and SiPM assembly}
	\label{fig:exposurebox_assembly}
\end{figure}

\noindent A position is measured by turning on the corresponding led and letting it warm up for \SI{30}{s}. Then the counter of the oscilloscope is reset and after counting trigger pulses for ten minutes a screenshot is taken to save the counted value and the led is turned of. This is repeated once for every position. \ref{fig:exposurebox_results} shows the results wrapping with foil, PTFE, without PWM not turning on any LEDs and lastly a foil wrap with more vinyl tape around the corners of the scintillator. 

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/exposureboc_measurement_600s_100mv}
	\caption{Measurements of light tightness}
	\label{fig:exposurebox_results}
\end{figure}

\noindent The measurement without any PWM shows that estimating the error of the counts purely as the statistical variation determined by the square root of counts is not sufficient to explain the variation seen between the LED positions. Thus it was decided to calculate the error as the sum of the statistical variation and the standard deviation of the measurements without PWM. Within errors the measurement of the PTFE wrap is compatible with the measurement without PWM. The first foil measurement shows counts above the measurements without PWM beyond the errors, indicating insufficient light tightness. However adding a small piece of tape stretched over each edge of scintillator shows enough of an improvement to where measurements are compatible with the measurement without PWM.

\subsection{Additional Observations}

First tests of light tightness were conducted with shortened measurement duration of \SI{10}{s} warm up time and a measuring duration of \SI{60}{s}. The results of these tests are shown in \ref{fig:exposurebox_multi_ptfe} for multiple iterations of adding tape around areas where the counts imply a lack of light tightness. The measurements without PWM are scaled to match the measurement duration and shown for reference. The error are again determined by the sum of statistical uncertainty and the standard deviation of the measurements without PWM. 

\begin{figure}[htbp]\centering
	\includegraphics[width=0.8\textwidth]{grafik/exposureboc_measurement_60s_100mv}
	\includegraphics[width=0.8\textwidth]{grafik/exposureboc_measurement_60s_100mv_zoom}
	\caption{Measurements of improvements by adding additional tape. Bottom plot with scaled count numbers.}
\label{fig:exposurebox_multi_ptfe}
\end{figure}

\noindent This measurement highlights the importance of light tightness. Even though the scintillator and SiPM were thoroughly wrapped with PTFE tape and vinyl tape to the point where no PTFE tape was visible from the outside, adding some additional vinyl tape it was possible to clearly improve the light tightness. This also implies that a multi layered approach to light tightness should be considered.

Furthermore it should be mentioned that an attempt was made to with all LEDs powered at once. This however proved unsuccessful due to a large amount of noise  being introduced likely by the PWM of the LEDs, pointing out a design flaw in the cable routing. A better design would have been to pass the cables for the LEDs in from the opposite side of the cable connecting the SiPM to the amplifier. This points out the importance of a short distance between SiPM and amplifier in a noisy environment.


