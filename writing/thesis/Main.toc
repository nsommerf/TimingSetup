\babel@toc {american}{}\relax 
\contentsline {section}{\numberline {1}Theory}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Interactions of Electrons with Matter}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Silicon Photomultipliers}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Scintillators}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Time Walk}{5}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Electronics}{6}{subsection.1.5}%
\contentsline {section}{\numberline {2}LHCb Detector Now and in the Future}{9}{section.2}%
\contentsline {subsection}{\numberline {2.1}Current Detector}{9}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Upgrade II Proposal}{9}{subsection.2.2}%
\contentsline {section}{\numberline {3}Introduction to Timing Setup}{11}{section.3}%
\contentsline {subsection}{\numberline {3.1}Motivation}{11}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Concept}{11}{subsection.3.2}%
\contentsline {section}{\numberline {4}Custom Printed Circuit Boards}{12}{section.4}%
\contentsline {subsection}{\numberline {4.1}Amplifier Board}{12}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Earlier Iterations of the Amplifier Board}{13}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Logic Board}{14}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}First Production Run of Logic Boards}{15}{subsection.4.4}%
\contentsline {section}{\numberline {5}Deciding on a Configuration of SiPM and Scintillator}{17}{section.5}%
\contentsline {subsection}{\numberline {5.1}Scintillator}{17}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Available SiPMs}{17}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}How the Raytracing Algorithm Works}{17}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Testing the Influence of Different Parameters on Collection Efficiency}{18}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Choice of SiPM and Scintillator}{20}{subsection.5.5}%
\contentsline {section}{\numberline {6}Testing Different Scintillator Wrapping Methods}{25}{section.6}%
\contentsline {subsection}{\numberline {6.1}Observations about Wrapping}{25}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Testing of Light Tightness}{25}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Additional Observations}{27}{subsection.6.3}%
\contentsline {section}{\numberline {7}Mechanical Structures}{30}{section.7}%
\contentsline {subsection}{\numberline {7.1}Housing for Scintillator, SiPMs and Amplifier Boards}{30}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Source and Collimator Holder}{32}{subsection.7.2}%
\contentsline {section}{\numberline {8}Notes on Designs}{34}{section.8}%
\contentsline {section}{\numberline {9}Analysis of Setup Performance}{35}{section.9}%
\contentsline {subsection}{\numberline {9.1}Script for Data Tacking}{35}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Performance of Only the Logic Board}{36}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Data Analysis}{38}{subsection.9.3}%
\contentsline {subsection}{\numberline {9.4}Optimizing Parameters}{45}{subsection.9.4}%
\contentsline {subsection}{\numberline {9.5}Additional Measurements}{45}{subsection.9.5}%
\contentsline {subsection}{\numberline {9.6}Time Walk Correction}{45}{subsection.9.6}%
\contentsline {section}{\numberline {10}Conclusion and Outlook}{47}{section.10}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
