\section{Analysis of Setup Performance}

This section deals with the measurement of the timing resolution of the timing layer. The timing resolution is measured by stacking two timing layers on top of each other and placing a source on top. The timing resolution is estimated using the difference of the timestamps created by each of timing layers. The outputs of the timing layers are recorded using an oscilloscope. The oscilloscope is set up such that it measures the differential outputs of the coincidence signal of each timing layer. A sketch of this setup is shown in \cref{fig:timing_resolution_measurement_sketch}.

\begin{figure}[htbp]\centering
	\includegraphics[width=\textwidth]{grafik/timing_resolution_measurement}
	\caption{Sketch of setup used to measure the timing resolution of the timing layer}
	\label{fig:timing_resolution_measurement_sketch}
\end{figure}

\subsection{Script for Data Tacking}
\label{sec:script}

-show fit over full range

A python script is used to automate the data tacking. It uses serial commands to communicate with the oscilloscope via Ethernet. A measurement starts by setting the trigger of the oscilloscope to single mode, with a threshold between logic high and logic low. In this configuration the oscilloscope automatically stops the acquisition after a trigger is occurs. Meanwhile the program constantly checks if there was a trigger. After a trigger is detected it extracts the raw waveform data and converts it from raw values to measurements in actual units. This is done for each channel, one after another. Each measurement gets an incrementing measurement number. The difference of two generalized error function is fitted to the waveform of each channel. To determine whether or not the fits were successful three checks are performed. The first check catches errors and exceptions raised by \texttt{curve\_fit} which is used to perform the fits. The next check compares the $\chi^2_{red}$ of the fits to a set value. The last test only passes if the amplitude of the fitted function is large enough, this is necessary because the constant waveform present when there is no hit can also be described by setting the amplitude to zero. If the fit of any channel fails one or more of the checks the measurement is discarded, but the measurement number is incremented anyways. The turn on time for each of the timing layers is determined as the first intersection of the functions fitted to the differential output signal of the layer. Similarly the turn off is defined as the second intersection of differential output. The time over threshold (ToT) is defined as the time between turn on and turn off. The number of the measurement n, the turn on time, turn off time, ToT and $\chi^2_{red}$ for each of the fits are saved to a text file. The minimum time it takes for the script to take a data point is largely determined by the speed at which the oscilloscope can send the waveform data to the PC. The Oscilloscope used in this thesis enabled a data rate of up to five full measurement per second if the activity of the source and by extension  trigger rate is high enough.


\subsection{Performance of Only the Logic Board}

-show different measurements
-show closer look at function generator
-highlight sensitivety of measurement setup shift by turning cables/ moving cables
-show osci bug
-estimate maximum influence of external random stuff
-show clean peak -> minimum resolution (probably dominated by funcgen /osci)

Taking one step back from the setup shown in \cref{fig:timing_resolution_measurement_sketch} it is possible to take a look at the timing performance of only the logic board, by connecting a function generator to the inputs of each logic boards. A sketch of this measurement is shown in \cref{fig:timing_resolution_only_logic_sketch}.

\begin{figure}[htbp]\centering
	\includegraphics[width=\textwidth]{grafik/timing_resolution_sketch_only_logic}
	\caption{Sketch of setup used to measure the timing performance of the logic board}
	\label{fig:timing_resolution_only_logic_sketch}
\end{figure} 

\noindent The function generator produces a sine wave with a frequency of \SI{50}{MHz} and an  amplitude of \SI{4}{V} measured peak to peak centered around \SI{0}{V}, while the thresholds on the logic boards are set to \SI{-140}{mV}. The data is recorded by the script described in \cref{sec:script}. A histogram of the difference of the turn on times of the logic boards is shown in \cref{fig:timing_resolution_only_logic}.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/timing_resolution_only_logic}
	\caption{Measurement of the timing performance of the logic board}
	\label{fig:timing_resolution_only_logic}
\end{figure}

\noindent The histogram shows data collected from 9998 triggers. Furthermore the $\epsilon$ illustrates that every time the oscilloscope triggered both logic boards produced signals that were detected and passed all checks. Taking a look at the distribution of time differences reveals that two individual peaks are formed. This behavior is unexpected and therefore further investigated.

The first one is that this is simply an artifact produced by the sampling rate of the oscilloscope used to record the data, which is \SI{5}{GHz} corresponding to \SI{0.2}{ns} between to samples. This contradicts the fact that most counts are contained within a window of \SI{0.1}{ns} and that the distance between the peaks is about half of that. 
The second possible cause is that the Function generator uses pulse width modulation (PWM) to generate its output. The threshold of one logic board would then be such that with roughly equal probability it triggers on neighboring steps of the output. A PWM frequency of \SI{20}{GHz} would correspond to the \SI{0.05}{ns} time difference seen between the peaks.
To test the first of the previous theories one could use an oscilloscope with a faster sampling time to collect the data. Again using a faster oscilloscope one could look for any signs of PWM steps in the output of the function generator.

Although further investigations into this double peak structure would certainly be interesting, it was decided to not further pursue this route. This decision was taken, because most of the counts are at worst contained within \SI{0.1}{ns} which does not provide a bottleneck to the overall performance of the timing setup. However this phenomenon should be kept in mind in case of future improvements of the setup, which could reach resolutions where this effect could become relevant if it is not an artifact produced by the measurement alone.



\subsection{Data Analysis}\label{sec:data_analysis}

The data taken using two timing layers together as shown in \cref{fig:timing_resolution_measurement_sketch} with the script described in \cref{sec:script} is analyzed using a separate program. The analysis program starts by reading in the data stored in the text file of a measurement session. It the computes the difference between the turn on times of the top and bottom layer, by subtracting the turn on time of the top layer from the turn on time of the bottom layer. As a first attempt of understanding the measurement, the resulting time differences are plotted as a histogram and the errors of each bin are estimated as the square root of entries within them. Then a function is fitted to the data using the least squares method of \texttt{curve\_fit}. First a single Gaussian was fitted to the data shown in \cref{fig:fit_single_gauss}. The pulls determined as the fit value subtracted from data normalized to the error for each bin are shown below the plot. The pulls alongside the calculated $\chi^2_{red}$ display that a single Gaussian is not adequate do the describe the tails on either side of the Gaussian, with the tail to the right hand side being more pronounced than the tail to the left side.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/single_gauss_10kMeasurements_21-06-2023_11-35-21}
	\caption{Single Gaussian fitted to a measurement}
	\label{fig:fit_single_gauss}
\end{figure}

\noindent The tails are to some extend produced by time walk. This can be seen when plotting the time difference against the time over threshold (ToT) of each of the layers, this is shown in \cref{fig:2d_histo}. The shape of these distributions can be understood using the configuration of the timing layers and the interaction of low energy electron with low Z materials. The distributions curve in opposite directions for low ToTs. This can be explained by the way the time difference is computed, where the turn on time of the top layer is subtracted from the turn on time of the bottom layer. This means that time walk in the top layer leads to a tail on the left side of the core while time walk in the bottom layer leads to a tail on the right. Knowing that the ToT is proportional to the energy deposited in the scintillator reveals that the lower layer measures lower energies than the top layer. This is caused by particles that enter the bottom layer with low energy and are quickly stopped, depositing a small amount of energy. Particles that are stopped in the top layer are not visible, because the oscilloscope is set to trigger on the bottom layer. This means that particles that are stopped in the top layer and therefore cannot reach the bottom layer and are not measured. This results in a minimum for the energy deposited in the top layer, of the particles that are detected in the bottom layer. The plots show a difference in the maximum deposited energy for each layer. The naive expectation would be that the maximum energy in the bottom layer is lower than in the top layer, because all measured particles deposit a minimum of energy in the top layer and the available energy to be deposited in the bottom layer is limited. However this is not the case. There are two main reasons for this behavior. The first one is a different response to the same amount of energy being deposited in each layer. This can be verified by swapping the layers and taking the same measurement, which is shown in \cref{fig:2d_histo_swapped}. The measurement with swapped layers shows that the general shape of the ToT depends on the position of the layer. Furthermore this measurement shows a shift in the ToTs where layer A measures longer ToTs. This hints at a more efficient conversion of deposited energy into signal in layer A. Possible reasons for that include a more efficient coupling of light generated by the scintillator into the SiPMs, a more efficient conversion inside the SiPM and difference in the response of the logic board to its input. However if this were the main cause of the different response one would expect a layer to achieve the same maximum energy regardless of its position. This is where the second reason compounds to the difference. \cite{electron_bragg} suggests a Bragg peak like behavior for low energy electrons in low Z materials. This may explain the additional maximum energy seen in the bottom layer. The limited energy window seen by the top layer limits the time walk seen in the top layer and by extension the tail to the left side of the Gaussian core.

\begin{figure}[htbp]\centering
		\includegraphics[width=0.8\textwidth]{grafik/2d_histo10kMeasurements_top_21-06-2023_11-35-21}
		\includegraphics[width=0.8\textwidth]{grafik/2d_histo10kMeasurements_bottom_21-06-2023_11-35-21}
	\caption{Histogram of time difference versus time over threshold in top layer (top) and bottom layer (bottom) }
	\label{fig:2d_histo}
\end{figure}

\begin{figure}[htbp]\centering
	\includegraphics[width=0.8\textwidth]{grafik/tot_top}
	\includegraphics[width=0.8\textwidth]{grafik/tot_bottom}
	\caption{Histogram of ToT in top layer (top) and bottom layer (bottom), layer B is the default top layer and layer A is the default bottom layer}
	\label{fig:2d_histo_swapped}
\end{figure}

\noindent In an attempt to describe the tails created by time walk the sum of two Gaussians was fitted to the data, the results of which are displayed in \cref{fig:fit_double_gauss}. The Pulls and $\chi^2_{red}$ show that where there is a significant amount of data it can be adequately described. The fact that the data is not of Gaussian shape means that stating the timing resolution simply as a sigma is not sufficient. The full width at half maximum (FWHM) is a better measure for non Gaussian resolutions. Unlike sigma for a Gaussian fit the uncertainty of FWHM is not easily obtainable by analytical means. Thus bootstrapping is used to estimate the statistical uncertainty of the FWHM. Bootstrapping in this case means randomly picking values from the data until a new set of data equal in the number of entries is created.Then the FWHM of the new data set is computed and saved. This is repeated many times. The standard deviation of the FWHMs approaches the statistical error for for an infinite number of iterations. This analysis uses 1000 iterations. Testing with different numbers of iterations showed, that the standard deviation mostly stabilizes after 100 iterations.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/sum_gauss10kMeasurements_21-06-2023_11-35-21}
	\caption{Sum of two Gaussians fitted to a measurement}
	\label{fig:fit_double_gauss}
\end{figure}

\noindent The FWHM calculated previously is a measure for the timing resolution of two timing layers combined, but for the measurement of the timing resolution of a silicon chip the timing resolution of a single timing layer is relevant. In the case of Gaussian resolution one could simply add a factor of $\frac{1}{\sqrt{2}}$ to the sigma determined by the Gaussian fit to both layers and determine the timing resolution of a single layer that way. However it was already established that the timing resolution is not Gaussian in shape, thus a systematic error is introduced by simply assuming Gaussian error propagation. This systematic error is not easily estimated using the sum of two Gaussians fitted to the data. A function consisting of a Gaussian core with tails on either side offers the possibility to compare the FWHM computed from the sigma of the Gaussian to the FWHM determined from the full it function. A crystal ball function consists of a Gaussian core with tail on either side, but \cite{gaussexp} suggest using what they call GaussExp instead. According to \cite{gaussexp} GaussExp offers more stable fits and less parameters. The version of GaussExp with two tails, which is used to fit the data is shown in \cref{eq:gaussexp}. 

\begin{equation}\label{eq:gaussexp}
	\begin{split}
		f(x;\bar{x}, \sigma, k_L, k_H) &= A\cdot\mathrm{exp} \left(\frac{k_L^2}{2}+k_L\left(\frac{x-\mu}{\sigma}\right)\right),\quad \mathrm{for}\quad \frac{x-\mu}{\sigma}\leq -k_L\\
		&= A\cdot\mathrm{exp} \left(-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2\right),\quad \mathrm{for}\quad -k_L<\frac{x-\mu}{\sigma} \leq k_H\\
		&= A\cdot\mathrm{exp} \left(\frac{k_H^2}{2}-k_H\left(\frac{x-\mu}{\sigma}\right)\right),\quad \mathrm{for}\quad k_H<\frac{x-\mu}{\sigma}
	\end{split}
\end{equation}

\noindent Where $A$ is the maximum amplitude. $\mu$ and $\sigma$ are the mean and standard deviation of the Gaussian core. $k_L$ is the lower bound and $k_H$ is the upper bound of the Gaussian core. The fit of GaussExp to the data is shown in \cref{fig:fit_gaussexp}.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/double_gaussexp_10kMeasurements_21-06-2023_11-35-21}
	\caption{GaussExp fitted to a measurement}
	\label{fig:fit_gaussexp}
\end{figure}

\noindent The pulls and $\chi^2_{red}$ show that the data is well described by GaussExp with two tails, where most of the data is located. This approach separates the Gaussian core from the tails. This allows to calculate the FWHM of the Gaussian core using: $\mathrm{FWHM}_{core} = 2\sqrt{2\ln{2}}\sigma$. As a conservative estimate of the systematic uncertainty introduced by assuming Gaussian error propagation for calculating the timing resolution of a single layer, the difference $\mathrm{FWHM}_{core}$ and FWHM of the full function can be used. $\mathrm{FWHM}_{core}$ consistently underestimates underestimates FWHM of the full function. Taking a conservative approach the difference of $\mathrm{FWHM}_{core}$ and FWHM of the full function is linearly propagated to the timing resolution of a single layer, as the systematic uncertainty introduced by assuming Gaussian error propagation going from two layers to one. For the measurement shown in \cref{fig:fit_gaussexp} this results in a timing resolution of $\left(1.429 \pm 0.058 ~ ^{+0.113}_{-0} \right)$~\si{ns} for a single layer. 

The following measurements use GaussExp to describe their data, bootstrapping to estimate their statistical error and the difference between $\mathrm{FWHM}_{core}$ and FWHM of the full function to evaluate the systematical uncertainty introduced by calculating the timing resolution of a single layer from that of two.

\subsection{Optimizing Parameters}

There are several parameters available to tune when trying to achieve optimal performance of the timing setup. Tuneable parameters include the threshold set on the logic board, the bias voltage supplied to the SiPMs and the supply voltage that drives the amplifier. In order to find the optimal parameters to run the timing setup at, several scans over different parameters were performed using the analysis method described in \cref{sec:data_analysis}. The measured timing resolutions of a scan are then plotted together. Shown in  \cref{fig:multiplot_counts} is the timing resolution measured multiple times for the same settings with varying amounts of statistics.

\begin{figure}[htbp]\centering
	\includegraphics[width=0.9\textwidth]{grafik/counts_140_th_57_2_bias_5_amp}
	\caption{Timing resolution of a single layer, measured with a threshold of \SI{140}{mV} on the logic board, a SiPM bias of \SI{57.2}{V} and amplifier supply voltage of \SI{5}{V}. Systematic uncertainties are shown in blue closest to the data points and statistical errors are shown in red added to the systematic uncertainties.}
	\label{fig:multiplot_counts}
\end{figure}


\subsection{Additional Measurements} 



\subsection{Time Walk Correction}

-colimator
-extra material
-asymetric cable?
-high statistics of best params?
-single sipms?




stuff to analyze:
-stacked timing resolution
-timing resolution of logic board alone
-influence of dissimmilar sipm signal strength